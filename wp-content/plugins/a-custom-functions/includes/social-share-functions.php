<?php
// Social Media Share Buttons
add_shortcode( 'share_bar_content_facebook', 'share_bar_content_facebook' );
/**
* @param $atts
*
* @return string
*/
function share_bar_content_facebook( $atts ) {
global $wp;

return "www.facebook.com/sharer/sharer.php?u=" . home_url( add_query_arg( array(), $wp->request ) ) . "";
}

add_shortcode( 'share_bar_content_twitter', 'share_bar_content_twitter' );

/**
* @param $atts
*
* @return string
*/
function share_bar_content_twitter( $atts ) {
global $wp;

return "twitter.com/home?status=" . home_url( add_query_arg( array(), $wp->request ) ) . "";
}

add_shortcode( 'share_bar_content_gplus', 'share_bar_content_gplus' );

/**
* @param $atts
*
* @return string
*/
function share_bar_content_gplus( $atts ) {
global $wp;

return "plus.google.com/share?url=" . home_url( add_query_arg( array(), $wp->request ) ) . "";
}

add_shortcode( 'share_bar_content_email', 'share_bar_content_email' );

/**
* @param $atts
*
* @return string
*/
function share_bar_content_email( $atts ) {
global $wp;

return "#";
}
