<?php
/*
Plugin Name: OGK Custom Functions
Description: OGK Custom Functions , use this plugin for non theme specific functions
Version: 1.01
Author: Tim Graham | <a href="https://ogkcreative.com" target="_blank">OGK Creative</a>
*/

/* ================================================================= */

/**
 * Default ACF Site Options Add
 */
function my_acf_add_local_field_groups(){
	//include_once( 'includes/hero-acf-fields.php' );
	//include_once( 'includes/default-site-options.php' );
}
add_action('acf/init', 'my_acf_add_local_field_groups');

/* ================================================================= */

/**
 * ENQUEUE STYLES
 */



function theme_enqueue_styles() {
	$assets = get_template_directory_uri().'/assets';

	//Google Font Load
	wp_enqueue_style('google-font','https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700');
	// Font Awesome
	wp_enqueue_style( 'font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css' );

}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

/* ================================================================= */

/**
 * ENQUEUE SCRIPTS
 */
function theme_enqueue_scripts() {
	$assets = get_template_directory_uri().'/assets';
//	wp_register_script('jquery', $assets.'js/jquery-3.1.0.min.js', '', '', false);
//	wp_enqueue_script('jquery'); //jQuery
    wp_deregister_script('jquery');
//	//Owl Carousel
//	wp_register_script('owl-carousel', $assets.'/js/owl-carousel/owl.carousel.min.js', 'jquery', '', true);
//	wp_enqueue_script('owl-carousel');
//	// In View
//	wp_register_script('inView', $assets . '/js/inview/jquery.inview.js', 'jquery', '', true);
//	wp_enqueue_script('inView');
//	// lazysizes
//	wp_register_script('lazysizes', $assets.'/js/lazy-sizes/lazysizes-min.js', '', '', true);
//	wp_enqueue_script('lazysizes');
//	// Vide
//	wp_register_script('Vide', $assets.'/js/vide/jquery.vide.js', 'jquery', '', true);
//	wp_enqueue_script('Vide');
//	// Vide
//	wp_register_script('Lity', $assets.'/js/lity/lity.min.js', 'jquery', '', true);
//	wp_enqueue_script('Lity');
	
//	wp_register_script('scripts', $assets.'/js/scripts.js', '', (WP_DEBUG == TRUE)? time(): '', true);
//	wp_enqueue_script('scripts'); // main scripts file
}
add_action('wp_enqueue_scripts', 'theme_enqueue_scripts');

/* ================================================================= */

/**
 *  THEMED LOGIN PAGE
 */
function custom_login() { ?>
	<style type="text/css">
		#login h1 a, .login h1 a {
			background-image: url(<?php echo plugin_dir_url( __FILE__ ); ?>includes/images/logo.svg);
			height: 65px;
			width: 320px;
			background-size: 320px 65px;
			background-repeat: no-repeat;
			padding-bottom: 30px;
		}
		#login path {
			fill: #000;
		}
	</style>
<?php }

add_action( 'login_enqueue_scripts', 'custom_login' );

/* ================================================================= */
/**
 * @return string|void
 * Login Logo to redirect to homepage
 */
function my_login_logo_url() {
	return home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

/* ================================================================= */


add_theme_support( 'menus' ); // add menus
add_theme_support( 'post-thumbnails' ); // add featured iamges
add_post_type_support( 'page', 'excerpt' ); // add excerpts

/* ================================================================= */

/**
 * REGISTER MAIN MENU
 */
function register_my_menu() {
	register_nav_menus(
			array(
					'main-menu' => __('Main Menu'),
					'footer-menu' => __('Footer Menu')
			)
	);
}

add_action( 'init', 'register_my_menu' );

function add_menuclass($ulclass) {
	return preg_replace('/<a/', '<a class="menu-item"', $ulclass, -1);
}
add_filter('wp_nav_menu','add_menuclass');

/* ================================================================= */

/**
 * Allow shortcodes in menu items
 * @param $items
 * @param $args
 *
 * @return string
 */
function wp_nav_menu_items( $items, $args ) {
	$items = do_shortcode( $items );

	return $items;
}
add_filter( 'wp_nav_menu_items', 'wp_nav_menu_items', 10, 2 );

/* ================================================================= */

/**
 * add SVG to allowed file uploads
 *
 * @param $file_types
 *
 * @return array
 */
function add_file_types_to_uploads( $file_types ) {

	$new_filetypes        = array();
	$new_filetypes['svg'] = 'image/svg+xml';
	$file_types           = array_merge( $file_types, $new_filetypes );

	return $file_types;
}

add_action( 'upload_mimes', 'add_file_types_to_uploads' );

/* ================================================================= */

/**
 *  ACF
 */
function acf_form_head_include() {

	acf_form_head();

}
add_action( 'template_redirect', 'acf_form_head_include' );

/* ================================================================= */

/**
 * @param $api
 * Add ACF Google Maps API key
 * @return mixed
 */
function my_acf_google_map_api( $api ) {

	$api['key'] = 'AIzaSyAwJw4WnnUoNJKkW3mpDGq5ByOViieoJkI';

	return $api;

}
add_filter( 'acf/fields/google_map/api', 'my_acf_google_map_api' );

/* ================================================================= */

/**
 *   Create ACF options pages
 */
function after_load() {

	// ACF option pages
	if ( function_exists( 'acf_add_options_page' ) ) {

		acf_add_options_page( array(
			'page_title' => 'Site Settings',
			'menu_title' => 'Site Settings',
			'menu_slug'  => 'site-settings',
			'capability' => 'edit_posts',
			'redirect'   => false
		) );

	}

}
add_action( 'plugins_loaded', 'after_load' );

/* ================================================================= */

/**
 * Die and Dump function
 * Colin's little function for debugging
 * Borrowed from Laravel's DD Function --> https://laravel.com/docs/5.6/helpers#method-dd
 * Can be deleted on production
 */
if ( ! function_exists( 'dd' ) ) {
	function dd() {
		array_map( function ( $x ) {
			echo "<pre>";
			var_dump( $x );
			echo "</pre>";
		}, func_get_args() );
		die;
	}
}
?>