<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package _s
 */
?>

	</div><!-- #main -->

<?php if ( ! is_front_page() ) : ?>
	<div class="slideshow-wrap wrap">
		<ul class="slideshow">
			<?php $args = array( 'post_type' => 'slide', 'order' => 'ASC', 'posts_per_page' => '6', 'orderby' => 'menu_order' );
			$prevposts = new WP_Query( $args ); while ( $prevposts->have_posts() ) : $prevposts->the_post(); { ?>
			<li>
				<div class="slide-content">
					<h1><?php the_title(); ?></h1>
					<?php if ( 'slide_secondary_title' ) { ?><h2><?php the_field('slide_secondary_title'); ?></h2><?php } ?>
					<?php if ( 'slide_content' ) {
					$trimcontent = get_field('slide_content');
					$shortcontent = wp_trim_words( $trimcontent, $num_words = 25, $more = '… ' );
					?>
						<p><?php echo $shortcontent; ?></p>
					<?php } ?>
					<?php if ( 'slide_getting_started_url' ) { ?><a href="<?php the_field('slide_getting_started_url'); ?>" class="button"><?php _e('Get Started!'); ?></a><?php } ?>
				</div>
				<?php if ( 'slide_image' ) { ?><img src="<?php the_field('slide_image'); ?>"><?php } ?>				
			</li>
			<?php } endwhile; wp_reset_query(); ?>
		</ul><!-- .slideshow -->
	</div><!-- .slideshow-wrap -->
<?php endif; ?>

	<div class="secondary-content wrap">
		<div class="three-boxes">
			<a href="<?php bloginfo('url'); ?>/your-needs/" class="your-needs">
				<span class="gradient-hover"></span>
				<h1><?php _e('Your Needs &amp; Life Events'); ?><br>
				<span><?php _e('Gain a wealth'); ?><br><?php _e('of insight.'); ?></span></h1>
			</a>
			<a href="<?php bloginfo('url'); ?>/your-needs/401k-rollover-center/" class="learn-more">
				<span class="gradient-hover"></span>
				<h1><?php _e('Learn More About 401k Rollovers'); ?></h1>
			</a>
			<a href="<?php bloginfo('url'); ?>/your-needs/women-wealth/" class="women-wealth">
				<span class="gradient-hover"></span>
				<h1><?php _e('Women'); ?><br><?php _e('and Their Wealth'); ?></h1>
			</a>						
		</div><!-- .three-boxes -->

		<div class="news-boxes">
			<?php $args = array( 'post_type' => 'post', 'order' => 'DES', 'posts_per_page' => '1', 'orderby' => 'date', 'category_name' => 'news-room' );
			$newsroom = new WP_Query( $args ); while ( $newsroom->have_posts() ) : $newsroom->the_post(); { ?>			
				<div class="news-room">
					<span class="gradient-hover"></span>
					<span class="news-content">
						<h1><a href="<?php bloginfo('url'); ?>/resource-center/news-media/">News & Media</a></h1>
						<h2><?php echo mb_strimwidth(get_the_title(), 0, 19, '…'); ?> <span>- <?php the_time('F j, Y'); ?></span></h2>
						<p><?php echo mb_strimwidth(get_the_excerpt(), 0, 100, '…'); ?> <a href="<?php the_permalink(); ?>"><strong>Read More</strong></a></p>
					</span>
				</div>
			<?php } endwhile; wp_reset_query(); ?>							
			
			<?php $args = array( 'post_type' => 'post', 'order' => 'DES', 'posts_per_page' => '1', 'orderby' => 'date', 'cat' => '-5' );
			$blogposts = new WP_Query( $args ); while ( $blogposts->have_posts() ) : $blogposts->the_post(); { ?>			
			<div class="money-sense">
				<span class="gradient-hover"></span>
				<span class="news-content">
				<h1><a href="<?php bloginfo('url'); ?>/news-room/your-money-matters/">Money Sense Blog</a></h1>
					<h2><?php echo mb_strimwidth(get_the_title(), 0, 30, '…'); ?> <span>- <?php the_time('F j, Y'); ?></span></h2>
					<p><?php echo mb_strimwidth(get_the_excerpt(), 0, 160, '…'); ?> <a href="<?php the_permalink(); ?>"><strong>Read More</strong></a></p>
				</span>					
			</div>
			<?php } endwhile; wp_reset_query(); ?>			
		</div><!-- .news-boxes -->

	</div><!-- .secondary-content -->

<?php if ( is_front_page() ) : ?>
	<div class="slideshow-wrap wrap">
		<ul class="slideshow">
			<?php $args = array( 'post_type' => 'slide', 'order' => 'ASC', 'posts_per_page' => '6', 'orderby' => 'menu_order' );
			$prevposts = new WP_Query( $args ); while ( $prevposts->have_posts() ) : $prevposts->the_post(); { ?>
			<li>
				<div class="slide-content">
					<h1><?php the_title(); ?></h1>
					<?php if ( 'slide_secondary_title' ) { ?><h2><?php the_field('slide_secondary_title'); ?></h2><?php } ?>
					<?php if ( 'slide_content' ) {
					$trimcontent = get_field('slide_content');
					$shortcontent = wp_trim_words( $trimcontent, $num_words = 25, $more = '… ' );
					?>
						<p><?php echo $shortcontent; ?></p>
					<?php } ?>
					<?php if ( 'slide_getting_started_url' ) { ?><a href="<?php the_field('slide_getting_started_url'); ?>" class="button"><?php _e('Get Started!'); ?></a><?php } ?>
				</div>
				<?php if ( 'slide_image' ) { ?><img src="<?php the_field('slide_image'); ?>"><?php } ?>				
			</li>
			<?php } endwhile; wp_reset_query(); ?>
		</ul><!-- .slideshow -->
	</div><!-- .slideshow-wrap -->
<?php endif; ?>

	<div class="footer-links">

		<div class="footer-links-inner wrap">

			<div class="col-one">
				<div>
					<div class="footer-title">About Ulin &amp; Co</div>
					<?php wp_nav_menu( array( 'theme_location' => 'footer-about' ) ); ?>
				</div>
				<div>
					<div class="footer-title">Why Ulin &amp; Co</div>	
					<?php wp_nav_menu( array( 'theme_location' => 'footer-why' ) ); ?>
				</div>

				<div>
					<div class="footer-title">Retirement &amp; Planning</div>	
					<?php wp_nav_menu( array( 'theme_location' => 'footer-retirement' ) ); ?>
				</div>
			</div>

			<div class="col-two">
				<div>
					<div class="footer-title">Business Services</div>	
					<?php wp_nav_menu( array( 'theme_location' => 'footer-business' ) ); ?>
				</div>
				<div>
					<div class="footer-title">LPL Financial</div>	
					<?php wp_nav_menu( array( 'theme_location' => 'footer-lpl' ) ); ?>
				</div>
			</div>

			<div class="col-three">
				<div>
					<div class="footer-title">Wealth Management</div>	
					<?php wp_nav_menu( array( 'theme_location' => 'footer-wealth' ) ); ?>
				</div>
				<div>
					<div class="footer-title">News Room</div>	
					<?php wp_nav_menu( array( 'theme_location' => 'footer-newroom' ) ); ?>
				</div>
				<div>
					<div class="footer-title">Resource Center</div>	
					<?php wp_nav_menu( array( 'theme_location' => 'footer-resource' ) ); ?>
				</div>				
			</div>


			<div class="col-four">
				<div>
					<div class="footer-title">Your Needs</div>	
					<?php wp_nav_menu( array( 'theme_location' => 'footer-your' ) ); ?>
				</div>

				<div>
					<div class="footer-title">Financial Advisor</div>	
					<?php wp_nav_menu( array( 'theme_location' => 'footer-financial' ) ); ?>
				</div>
			</div>

			<p>Securities offered through LPL Financial  |  Member FINRA  |  SIPC  Investment Advice offered through Independent Financial Partners, a  Registered Investment Advisor and separate entity from LPL Financial. Independent Financial Partners and Ulin & Co. Wealth Management are separate entities from LPL Financial .</p>
			<p>The LPL Financial Registered Representatives associated with this site may only discuss and/or transact securities business with residents of the following states: Florida, New York, Connecticut<br> and Massachusetts</p>

		</div>

	</div>

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-footer-inner wrap">
			<nav id="site-navigation" class="footer-navigation" role="navigation">
				<?php wp_nav_menu( array( 'theme_location' => 'footer' ) ); ?>
			</nav><!-- #site-navigation -->
			<ul class="social">
				<li><a href="https://www.facebook.com/ulinwealth" class="facebook"><?php _e('Facebook'); ?></a></li>
				<li><a href="http://linkedin.com/in/jonulin" class="linkedin"><?php _e('LinkedIn'); ?></a></li>
				<li><a href="http://twitter.com/JonUlin" class="twitter"><?php _e('Twitter'); ?></a></li>
				<li><a href="http://www.youtube.com/user/bocaadvisor" class="youtube"><?php _e('YouTube'); ?></a></li>
			</ul><!-- .social -->
			<p class="copyright">© 2013 Ulin &amp; Co. Wealth Management. <a href="http://www.ogkcreative.com" target="_blank">Developed By OGK Creative</a>.</p>
		</div><!-- .site-info -->
		<div class="affiliates-logos">
			<div class="affiliates-logos-inner wrap">
				<ul>
                    <li><a href="http://lplfinancial.lpl.com"><img src="<?php echo get_template_directory_uri(); ?>/img/affiliates-lpl.jpg"></a></li>
                    <li><a href="http://www.ifpartners.com/clients/"><img src="<?php echo get_template_directory_uri(); ?>/img/affiliates-ifp.jpg"></a></li>
                    <li><a href="http://www.sipc.org"><img src="<?php echo get_template_directory_uri(); ?>/img/affiliates-sipc.jpg"></a></li>
					<li><a href="http://www.fpanet.org"><img src="<?php echo get_template_directory_uri(); ?>/img/affiliates-fpa.jpg"></a></li>					
					<li><a href="http://www.shrm.org/Pages/default.aspx"><img src="<?php echo get_template_directory_uri(); ?>/img/affiliates-shrm.jpg"></a></li>
					<li><a href="http://www.cfp.net"><img src="<?php echo get_template_directory_uri(); ?>/img/affiliates-cfp.jpg"></a></li>
					<li><a href="http://www.jdpower.com"><img src="<?php echo get_template_directory_uri(); ?>/img/affiliates-jdpower.jpg"></a></li>
<li><a href="https://brokercheck.finra.org/"><img src="<?php echo get_template_directory_uri(); ?>/img/fina.jpg"></a></li>
					
				</ul>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
