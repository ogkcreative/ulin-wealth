<?php

//*---------------------------Filter------------------------//
add_action('wp_ajax_nopriv_filter_by_ajax_post', 'filter_by_ajax_post');
add_action('wp_ajax_filter_by_ajax_post', 'filter_by_ajax_post');
function filter_by_ajax_post()
{
    $term_id    = $_POST['catid'];
    $page_count = '';
    $page_count = $_POST['page_count'];
    if ($term_id == 0) {
        $all_post       = new WP_Query(array(
            'post_type'      => 'post',
            'post_status'    => 'publish',
            'posts_per_page' => -1,
            'paged'          => $page_count
        ));
        $count_all_post = new WP_Query(array('post_type'   => 'post',
                                             'post_status' => 'publish'
        ));
    } else {
        $all_post       = new WP_Query(array(
            'post_type'      => 'post',
            'posts_per_page' => -1,
            'paged'          => $page_count,
            'tax_query'      => array(
                array(
                    'taxonomy' => 'category',
                    'field'    => 'term_id',
                    'terms'    => $term_id
                )
            )
        ));
        $count_all_post = new WP_Query(array(
            'post_type' => 'post',
            'tax_query' => array(
                array(
                    'taxonomy' => 'category',
                    'field'    => 'term_id',
                    'terms'    => $term_id
                )
            )
        ));
    }
    $filter_news_post = ceil($count_all_post->found_posts);
    $count_all_post   = ceil($count_all_post->found_posts / 4);
    ?>
    <?php if ( ! empty($all_post)) { ?>
    <input type="hidden" name="all_post" class="count_all_post" value="<?php echo $count_all_post; ?>">
    <input type="hidden" name="cat_id" class="cat_id_for_pagination" value="<?php echo $term_id; ?>">
    <?php if (wp_is_mobile()) {
        $desk_and_mob_class = 'news_post_list_mob';
    } else {
        $desk_and_mob_class = 'news_post_list_desk';
    } ?>
    <div class="replace_filter_html">
        <ul class="blog_posts news_post_list <?php echo $desk_and_mob_class; ?>">
            <?php while ($all_post->have_posts()) {
                $all_post->the_post();
                $category_id = get_the_category(get_the_ID());
                $cat_id      = $category_id[0]->cat_ID;
                $blogimage   = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full', false, ''); ?>
                <li>
                    <div class="post_thumb">
                        <a href="<?php the_permalink(); ?>"><img src="<?php echo $blogimage[0]; ?>" alt=""></a>
                    </div>
                    <div class="post_disc">
                        <h4>
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </h4>
                        <h3><?php echo get_the_date('F j, Y') ?> </h3>
                    </div>
                </li>
            <?php } ?>
        </ul>
        <?php if ($filter_news_post > 4) { ?>
            <div class="align-center">
                <a class="load-mored btn load_more btn-load-more scroll_load">Loading </a>
            </div>
        <?php } ?>
    </div>
    <?php wp_reset_query();
} ?>
    <?php
    wp_die();
}

//*---------------------------Filter------------------------//
add_action('wp_ajax_nopriv_filter_by_ajax_resource_post', 'filter_by_ajax_resource_post');
add_action('wp_ajax_filter_by_ajax_resource_post', 'filter_by_ajax_resource_post');
function filter_by_ajax_resource_post()
{
    $term_id    = $_POST['catid'];
    $page_count = '';
    $page_count = $_POST['page_count'];
    if ($term_id == 0) {
        $all_post       = new WP_Query(array(
            'post_type'      => 'resource',
            'post_status'    => 'publish',
            'posts_per_page' => -1,
            'paged'          => $page_count
        ));
        $count_all_post = new WP_Query(array('post_type'   => 'resource',
                                             'post_status' => 'publish'
        ));
    } else {
        $all_post       = new WP_Query(array(
            'post_type'      => 'resource',
            'posts_per_page' => -1,
            'paged'          => $page_count,
            'tax_query'      => array(
                array(
                    'taxonomy' => 'resource_cat',
                    'field'    => 'term_id',
                    'terms'    => $term_id
                )
            )
        ));
        $count_all_post = new WP_Query(array(
            'post_type' => 'resource',
            'tax_query' => array(
                array(
                    'taxonomy' => 'resource_cat',
                    'field'    => 'term_id',
                    'terms'    => $term_id
                )
            )
        ));
    }
    $filter_resource_post = ceil($count_all_post->found_posts);
    $count_all_post       = ceil($count_all_post->found_posts / 4);
    ?>
    <?php if ( ! empty($all_post)) { ?>
    <input type="hidden" name="all_post" class="count_all_post" value="<?php echo $count_all_post; ?>">
    <input type="hidden" name="cat_id" class="cat_id_for_pagination" value="<?php echo $term_id; ?>">
    <?php if (wp_is_mobile()) {
        $desk_and_mob_class = 'resource_post_list_mob';
    } else {
        $desk_and_mob_class = 'resource_post_list_desk';
    } ?>
    <div class="replace_filter_html">
        <ul class="blog_posts resource_post_list <?php echo $desk_and_mob_class; ?>">
            <?php while ($all_post->have_posts()) {
                $all_post->the_post();
                $category_id = get_the_category(get_the_ID());
                $cat_id      = $category_id[0]->cat_ID;
                $blogimage   = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full', false, ''); ?>
                <li>
                    <div class="post_thumb">
                        <a href="<?php the_permalink(); ?>"><img src="<?php echo $blogimage[0]; ?>" alt=""></a>
                    </div>
                    <div class="post_disc">
                        <h4>
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </h4>
                        <h3><?php echo get_the_date('F j, Y') ?> </h3>
                    </div>
                </li>
            <?php } ?>
        </ul>
        <?php if ($filter_resource_post > 4) { ?>
            <div class="align-center">
                <a class="resource_post load-mored btn load_more btn-load-more scroll_load">Loading </a>
            </div>
        <?php } ?>
    </div>
    <?php wp_reset_query();
} ?>
    <?php
    wp_die();
}

add_action('wp_ajax_nopriv_filter_by_ajax_content', 'filter_by_ajax_content');
add_action('wp_ajax_filter_by_ajax_content', 'filter_by_ajax_content');

function filter_by_ajax_content()
{ ?>

    <div class="loading_img" style="display:none;">
        <img src="<?php echo site_url(); ?>/wp-content/uploads/2018/08/loading.gif">
    </div>

    <div class="replace_filter_html">

        <?php if (have_rows('side_categories', 128)): ?>
            <ul class="sidenav_content">
                <?php $content_id = $_POST['con_id'];
                $k                = 1;
                while (have_rows('side_categories', 128)): the_row();
                    $headline       = get_sub_field('right_side_headline');
                    $paragraph_text = get_sub_field('right_side_paragraph_text'); ?>
                    <?php if ($k == $content_id) { ?>
                        <li>

                            <h2><?php echo $headline; ?></h2>

                            <?php echo $paragraph_text; ?>

                        </li>
                    <?php } ?>

                    <?php $k++; endwhile; ?>

            </ul>

        <?php endif; ?>

    </div>
    <?php die();
}

add_action('wp_ajax_nopriv_filter_by_ajax_finance_content_page', 'filter_by_ajax_finance_content_page');
add_action('wp_ajax_filter_by_ajax_finance_content_page', 'filter_by_ajax_finance_content_page');

function filter_by_ajax_finance_content_page()
{
    ?>

    <div class="loading_img" style="display:none;">
        <img src="<?php echo site_url(); ?>/wp-content/uploads/2018/08/loading.gif">
    </div>

    <div class="replace_filter_html">
        <?php $page_id = $_POST['page_id'];
        ?>
        <?php if (have_rows('side_categories', $page_id)): ?>
            <ul class="sidenav_content">
                <?php $content_id = $_POST['con_id']; $k = 1;
                while (have_rows('side_categories', $page_id)): the_row();
                    $headline       = get_sub_field('right_side_headline');
                    $paragraph_text = get_sub_field('right_side_paragraph_text');
                    $form_id =   get_sub_field('form_id');
                    ?>
                    <?php if ($k == $content_id) { ?>
                        <li>
                            <h2><?php echo $headline; ?></h2>

                            <p><?php echo $paragraph_text; ?></p>

                        </li>

                    <?php } $k++; endwhile; ?>
            </ul>

        <?php endif; ?>

    </div>
    <?php wp_die();
}


add_action('wp_ajax_nopriv_filter_by_ajax_finance_sub_content', 'filter_by_ajax_finance_sub_content');
add_action('wp_ajax_filter_by_ajax_finance_sub_content', 'filter_by_ajax_finance_sub_content');

function filter_by_ajax_finance_sub_content()
{ ?>

    <div class="loading_img" style="display:none;">
        <img src="<?php echo site_url(); ?>/wp-content/uploads/2018/08/loading.gif">
    </div>

    <div class="replace_filter_html">
        <?php $page_id = $_POST['page_id'];

        if (have_rows('side_categories', $page_id)):
            $l = 1;
            while (have_rows('side_categories', $page_id)): the_row(); ?>

                <?php if (have_rows('sub_categories', $page_id)): ?>
                    <ul class="sidenav_content">
                        <?php
                        $j = 1;
                        while (have_rows('sub_categories', $page_id)): the_row();
                            $sub_child_id   = $_POST['child_id'];
                            $sub_counter_id = $l.$j;
                            ?>
                            <?php if ($sub_child_id == $sub_counter_id) { ?>
                                <li>
                                    <h2><?php echo $sub_cate_headline = get_sub_field('sub_cate_headline'); ?></h2>
                                    <?php echo $sub_cat_content = get_sub_field('sub_cat_content'); ?>
                                </li>
                            <?php } ?>

                            <?php $j++; endwhile; ?>
                    </ul>
                <?php endif; ?>

                <?php $l++; endwhile; ?>
        <?php endif; ?>
    </div>
    <?php wp_die();
}

//*---------------------end-filter----------------------*//
?>
