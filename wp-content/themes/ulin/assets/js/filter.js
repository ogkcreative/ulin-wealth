window.gwrf;
jQuery(document).ready(function () {

	jQuery(".news_post ul.filter_category_menu > li > a").click(function (e) {
		e.preventDefault();
		jQuery('input.qty').val(1);
		jQuery(".loading_img").show();
		jQuery(".news_post ul.filter_category_menu li a").removeClass('active_cat');
		jQuery(this).addClass('active_cat');
		catid = jQuery(this).attr("catid");
		jQuery.ajax({
			url: ajaxurl,
			type: "post",
			data: {action: "filter_by_ajax_post", catid: catid},
			success: function (response) {
				//alert(response);
				jQuery(".replace_filter_html").html(response);
				AA                 = 4;
				var count_all_post = jQuery('.count_all_post').val();
				jQuery(".total_page_count").html(count_all_post);
				jQuery('input.qty').val(1);
				jQuery('.news_post_list li:lt(' + AA + ')').show();
				jQuery(".loading_img").hide();
			}
		});
	});
	jQuery(".resource_posts ul.filter_category_menu > li > a").click(function (e) {
		e.preventDefault();
		jQuery('input.qty').val(1);
		jQuery(".loading_img").show();
		jQuery(".resource_posts ul.filter_category_menu li a").removeClass('active_cat');
		jQuery(this).addClass('active_cat');
		catid = jQuery(this).attr("catid");
		jQuery.ajax({
			url: ajaxurl,
			type: "post",
			data: {action: "filter_by_ajax_resource_post", catid: catid},
			success: function (response) {
				//alert(response);
				jQuery(".replace_filter_html").html(response);
				BB                 = 4;
				var count_all_post = jQuery('.count_all_post').val();
				jQuery(".total_page_count").html(count_all_post);
				jQuery('input.qty').val(1);
				jQuery('.resource_post_list li:lt(' + BB + ')').show();
				jQuery(".loading_img").hide();
			}
		});
	});
	jQuery(".about_content ul.filter_category_menu > li > a").click(function (e) {
		e.preventDefault();
		jQuery(".loading_img").show();
		jQuery(".about_content ul.sidenav_list.filter_category_menu li a").removeClass('active_cat');
		jQuery(".square_indicators li").removeClass('active_indicator')
		jQuery(this).addClass('active_cat');
		con_id = jQuery(this).attr("catid");
		jQuery("#indicator_" + con_id).addClass('active_indicator');
		jQuery.ajax({
			url: ajaxurl,
			type: "post",
			data: {action: "filter_by_ajax_content", con_id: con_id},
			success: function (response) {
				//alert(response);
				jQuery(".replace_filter_html").html(response);
				jQuery(".loading_img").hide();
			}
		});
	});
	jQuery(".finance_content .sidebar ul.filter_category_menu > li > a").click(function (e) {
		e.preventDefault();
		jQuery(".loading_img").show();
		//alert("Clicked On Parent!");
		jQuery(".finance_content ul.filter_category_menu li a").removeClass('active_cat');
		jQuery(".square_indicators li").removeClass('active_indicator')
		jQuery(this).addClass('active_cat');
		con_id      = jQuery(this).attr("catid");
		var page_id = jQuery('.sidenav_list.filter_category_menu').attr('page-id');
		//alert(page_id);
		jQuery("#indicator_" + con_id).addClass('active_indicator');
		jQuery.ajax({
			url: ajaxurl,
			type: "post",
			data: {action: "filter_by_ajax_finance_content_page", con_id: con_id, page_id: page_id},
			success: function (response) {
				//alert(response);
				jQuery(".replace_filter_html").html(response);
				jQuery(".loading_img").hide();
				jQuery("#form_holder").removeClass('hide');
			}
		});
	});
	jQuery(".finance_content .sidebar ul.filter_category_menu > li ul.sub-menu li a").click(function (e) {
		e.preventDefault();
		jQuery(".loading_img").show();
		jQuery('.sub-menu.sub_categories li a').removeClass('active_cat');
		jQuery(".square_indicators li").removeClass('active_indicator')
		jQuery(this).addClass('active_cat');
		var page_id = jQuery('.sidenav_list.filter_category_menu').attr('page-id');
		//alert(page_id);
		child_id    = jQuery(this).attr("sub_catid");
		par_id      = jQuery(this).parents('.sub-menu').attr("parid");
		jQuery("#indicator_" + par_id).addClass('active_indicator');
		jQuery.ajax({
			url: ajaxurl,
			type: "post",
			data: {action: "filter_by_ajax_finance_sub_content", child_id: child_id, page_id: page_id},
			success: function (response) {
				//alert(response);
				jQuery(".replace_filter_html").html(response);
				jQuery(".loading_img").hide();
				//here
				jQuery("#form_holder").removeClass('hide');
			}
		});
	});
});
