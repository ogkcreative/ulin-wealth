jQuery(document).ready(function ($) {

    // LAZY LOAD
    $('.lazyload-wrap img').addClass('lazyload');
    $('.lazyload-wrap video').addClass('lazyload');
    $('.lazyload-wrap img').attr('data-sizes', 'auto');
    // IN VIEW ANIMATIONS
    $fadeInSpeed = 700;

    $('.fadeUp').on('inview', function (event, isInView) {
        if (isInView) {
            // element is now visible in the viewport
            $(this).animate({opacity: 1, top: 0}, $fadeInSpeed);
        } else {
            // element has gone out of viewport
        }
    });

    // OWL CAROUSEL

    var sync1 = $("#home_slider");
    var sync2 = $("#home_thumb_slide");
    var slidesPerPage = 4; //globaly define number of elements per page
    var syncedSecondary = true;

    sync1.owlCarousel({
        items: 1,
        slideSpeed: 2000,
        nav: false,
        animateOut: 'fadeOut',
        autoplay: true,
        dots: false,
        loop: false,
        responsiveRefreshRate: 200,
    }).on('changed.owl.carousel', syncPosition);

    sync2
        .on('initialized.owl.carousel', function () {
            sync2.find(".owl-item").eq(0).addClass("current");
        })
        .owlCarousel({
            items: slidesPerPage,
            dots: false,
            nav: false,
            mouseDrag: false,
            smartSpeed: 200,
            slideSpeed: 500,
            slideBy: slidesPerPage, //alternatively you can slide by 1, this way the active slide will stick to the first item in the second carousel
            responsiveRefreshRate: 100
        }).on('changed.owl.carousel', syncPosition2);

    $('.link').on('click', function () {

        window.location = $(this).attr('href');
    });

    function syncPosition(el) {
        //if you set loop to false, you have to restore this next line
        //var current = el.item.index;

        //if you disable loop you have to comment this block
        var count = el.item.count - 1;
        var current = Math.round(el.item.index - (el.item.count / 2) - .5 + 2);

        if (current < 0) {
            current = count;
        }
        if (current > count) {
            current = 0;
        }

        //end block

        sync2
            .find(".owl-item")
            .removeClass("current")
            .eq(current)
            .addClass("current");
        var onscreen = sync2.find('.owl-item.active').length - 1;
        var start = sync2.find('.owl-item.active').first().index();
        var end = sync2.find('.owl-item.active').last().index();

        if (current > end) {
            sync2.data('owl.carousel').to(current, 100, true);
        }
        if (current < start) {
            sync2.data('owl.carousel').to(current - onscreen, 100, true);
        }
    }

    function syncPosition2(el) {
        if (syncedSecondary) {
            var number = el.item.index;
            sync1.data('owl.carousel').to(number, 100, true);
        }
    }

    sync2.on("click", ".owl-item", function (e) {
        e.preventDefault();
        var number = $(this).index();
        sync1.data('owl.carousel').to(number, 300, true);
    });


    $(window).on('scroll', function () {
        if ($(window).scrollTop() >= $(document).height() - $(window).height() - 150) {
            $(".load-mored").click();
        }
    });


    // Mobile Home Slider

    var currentSlideIndexMobile = 0;

    var homeSliderMobile = $('#home_slider_mobile')
    homeSliderMobile.owlCarousel({
        items: 1,
        loop: true,
        nav: false,
        dots: false,
        autoplay: true,
        animateOut: 'fadeOut',
    }).on('dragged.owl.carousel', function (el) {
        var index = (el.item.index - el.relatedTarget._clones.length / 2 % el.item.count);
        homeSliderMobileNav.data('owl.carousel').to(index, 500, true);
    });

    var homeSliderMobileNav = $('#home_thumb_slide_mobile')
    homeSliderMobileNav.owlCarousel({
        items: 1,
        loop: true,
        nav: true,
        autoplay: true, 
        dots: false
    }).on('changed.owl.carousel', function (el) {
        var index = (el.item.index - el.relatedTarget._clones.length / 2 % el.item.count);
        homeSliderMobile.data('owl.carousel').to(index, 1000, true);
    });


// featured_slide

    $('.featured_slide').owlCarousel({
        loop: true,
        rewind: false,
        margin: 0,
        autoplay: true,
        items: 5,
        autoplayTimeout: 1000,
        autoplaySpeed: 1000,
        responsiveClass: true,
        responsive: {
            0: {
                items: 2,
                nav: false
            },
            768: {
                items: 3,
                nav: false
            },
            1200: {
                items: 5,
                nav: false,
                loop: true
            }
        }
    });

// Upcoming Event
    var carousel = $(".events");
    carousel.owlCarousel({
        loop: true,
        //margin:100,
        autoplay: true,
        items: 3,
        slideSpeed: 2000,
        //animateOut: 'fadeOut',
        responsiveClass: true,
        responsive: {
            0: {

                items: 1,
                nav: false,
                autoWidth: false,
                stagePadding: 0,

            },
            480: {
                items: 1,
                nav: false,
                autoWidth: false,
                stagePadding: 0,
            },

            600: {
                items: 2,
                nav: false,
                margin: 40
            },
            768: {
                items: 3,
                nav: false,
                margin: 40
            },
            1000: {
                items: 3,
                nav: false,
                loop: true,
                margin: 60
            }
        }
    });

    checkClasses();
    carousel.on('translated.owl.carousel', function (event) {
        checkClasses();
    });

    function checkClasses() {
        var total = $('.events .owl-stage .owl-item.active').length;

        $('.events .owl-stage .owl-item').removeClass('firstActiveItem lastActiveItem');

        $('.events .owl-stage .owl-item.active').each(function (index) {
            if (index === 0) {
                // this is the first one
                $(this).addClass('firstActiveItem');
            }

            if (index === total - 1 && total > 1) {
                // this is the last one
                $(this).addClass('lastActiveItem');
            }
        });
    }

    var viewportWidth = $(window).width();
    if (viewportWidth < 767) {
        $(".secondary_nav .filter_category_menu").addClass("view_menu_mobile");
    } else {
        $(".secondary_nav .filter_category_menu").removeClass("view_menu_mobile");

    }

    $(window).resize(function () {

        if (viewportWidth < 767) {
            $(".secondary_nav .filter_category_menu").addClass("view_menu_mobile");
        } else {
            $(".secondary_nav .filter_category_menu").removeClass("view_menu_mobile");

        }
    });


    //Responsive Menu
    $(".view-menu").click(function () {
        $(this).toggleClass('active');
        $(".main_nav").slideToggle('slow');
    });
    $('.menu-item-has-children > a').after('<span></span>');
    $(".menu-item-has-children > span").click(function () {
        if (!$(this).siblings('ul.sub-menu').hasClass('open')) {
            $('ul.sub-menu').removeClass('open');
        }
        $(this).siblings('ul.sub-menu').toggleClass('open');
    });
    $(".menu-item-has-children > span").click(function () {
        if (!$(this).hasClass('close_icn')) {
            $('.close_icn').removeClass('close_icn');
        }
        $(this).toggleClass('close_icn');

    });

    jQuery(".sidebar.secondary_nav span.select-dropdown").click(function () {
        jQuery('.sidebar.secondary_nav ul.filter_category_menu.view_menu_mobile').toggleClass("open-mobile").slideToggle();
        jQuery(this).toggleClass("open-dropdown");
        jQuery(".sub_menu_text").removeClass('sub_menu_text');
    });
    jQuery(".filter_category_menu.view_menu_mobile > li > a").click(function () {
        jQuery(".sub_menu_text").removeClass('sub_menu_text');
        //jQuery('.select-dropdown').removeClass('open-dropdown');
        //jQuery('.select-dropdown').removeClass('open-dropdown');
        var txt = jQuery(this).text();
        jQuery('.select-dropdown').text(txt);
        jQuery(".sidenav_list.filter_category_menu.view_menu_mobile.open-mobile").toggleClass("open-mobile");
        jQuery('.select-dropdown').trigger('click');
    });
    jQuery(".sub-menu.sub_categories > li > a").click(function () {

        jQuery('.active_cat').removeClass('active_cat');

        jQuery(this).addClass('active_cat');

        var subtxt = jQuery(this).text();

        jQuery('.select-dropdown').text(subtxt);

    });

    // Load More Blog Post//
    mob_news_post_size_li = jQuery(".news_post_list_desk li").size();
    MA = 4;
    jQuery('.news_post_list_desk li:lt(' + MA + ')').show();
    jQuery('.scroll_load').on("click", function () {
        MA = (MA + 4 <= mob_news_post_size_li) ? MA + 4 : mob_news_post_size_li;
        jQuery('.news_post_list_desk li:lt(' + MA + ')').show();
        var count_elements = jQuery('.news_post_list_desk li').length;
        if (MA == mob_news_post_size_li) {
            jQuery('.news_post .scroll_load').hide();

        }
    });


    resource_post_size_li = jQuery(".resource_post_list_desk li").size();
    MR = 4;
    jQuery('.resource_post_list_desk li:lt(' + MR + ')').show();
    jQuery('.scroll_load').on("click", function () {
        MR = (MR + 4 <= resource_post_size_li) ? MR + 4 : resource_post_size_li;
        jQuery('.resource_post_list_desk li:lt(' + MR + ')').show();
        var count_elements = jQuery('.resource_post_list_desk li').length;
        if (MR == resource_post_size_li) {
            jQuery('.resource_posts .scroll_load').hide();

        }
    });

});
// Scroll click on sub menu // 
jQuery(document).ready(function ($) {
    $(".scroll_submenu a").on('click', function (event) {

        if (this.hash !== "") {
            //event.preventDefault();
            var hash = this.hash;
            var id = hash.match(/[\d\.]+/g);
            if (id == "2") {
                var offset_value = "150";
            } else if (id == "3") {
                var offset_value = "210";
            } else if (id == "4") {
                var offset_value = "270";
            } else if (id == "5") {
                var offset_value = "320";
            } else if (id == "6") {
                var offset_value = "380";
            } else {
                offset_value = 0;
            }
            $('html, body').animate({
                scrollTop: $(hash).offset().top - offset_value
            }, 900, function () {
                window.location.hash = hash;
            });
        } // End if
        setTimeout(function () {
            $('.sidenav_list.filter_category_menu ' + hash + ' > a').trigger('click');
        }, 1000);

    });
    if (window.location.hash !== "") {
        //event.preventDefault();
        var hash = window.location.hash;
        var id = hash.match(/[\d\.]+/g);
        if (id == "2") {
            var offset_value = "150";
        } else if (id == "3") {
            var offset_value = "210";
        } else if (id == "4") {
            var offset_value = "270";
        } else if (id == "5") {
            var offset_value = "320";
        } else if (id == "6") {
            var offset_value = "385";
        } else {
            offset_value = 0;
        }
        $('html, body').animate({
            scrollTop: $(hash).offset().top - offset_value
        }, 900, function () {
            window.location.hash = hash;
        });
        setTimeout(function () {
            $('.sidenav_list.filter_category_menu ' + hash + ' > a').trigger('click');
        }, 1000);
    }


});

// jQuery(document).ready(function ($) {
//     $('.toggle-more').on('click', function () {
//         $(this).closest('.member_disc').find('.excerpt').hide(function () {
//             $(this).closest('.member_disc').find('.full-desc').slideToggle('slow');
//         });
//
//     });
//     $('.toggle-hide').on('click', function () {
//
//         $(this).closest('.member_disc').find('.full-desc').slideToggle('slow', function () {
//             $(this).closest('.member_disc').find('.excerpt').show();
//         });
//     });
// });
