<?php
/* Template Name: Radio Show */
get_header(); ?>
    <div class="container">
        <?php $radio_copy_heading = get_field('radio_copy_heading');
        $radio_copy_content = get_field('radio_copy_content');
        ?>
        <div class="text-block align-left">
            <?php if ($radio_copy_heading) { ?>
                <h2><?php echo $radio_copy_heading; ?></h2>
            <?php } ?>
            <?php echo $radio_copy_content; ?>
        </div>
        <div class="archive_section">
            <h2>Radio Archives</h2>
            <?php $paged = get_query_var('paged') ? get_query_var('paged', 1) : get_query_var('page', 1);

            $radio_args = array(
                'post_type' => 'radio_archives',
                'paged' => $paged,
                'order' => 'DESC',
                'posts_per_page' => 100,
            );

            $radio_query = new WP_Query($radio_args); ?>
            <?php if ($radio_query->have_posts()) { ?>
                <?php if (wp_is_mobile()) {
                    $desk_and_mob_class = 'radio_post_list_mob';
                } else {
                    $desk_and_mob_class = 'radio_post_list_desk';
                } ?>
                <ul class="archive_list <?php echo $desk_and_mob_class; ?>">
                    <?php while ($radio_query->have_posts()):
                        $radio_query->the_post();
                        $radio_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail');

                        ?>
                        <li>
                            <div class="post_thumb">
                                    <?php if ($radio_image) { ?>
                                        <img src="<?php echo $radio_image[0]; ?>" alt="">
                                    <?php } else { ?>
                                        <img src="<?php bloginfo('template_url') ?>/assets/images/placeholder.png"
                                             alt="">
                                    <?php } ?>
                            </div>
                            <div class="post_disc">
                                <h4><a href="javascript:void(0)"><?php the_title(); ?>…</a></h4>
                                <h3><?php echo get_the_date('F j, Y') ?> </h3>
                                <?php if (get_field('add_audio_short_code')) { ?>
                                    <div class="audio_section">
                                        <?php echo get_field('add_audio_short_code'); ?>
                                    </div>
                                <?php } ?>
                            </div>

                        </li>
                    <?php endwhile;
                    wp_reset_postdata(); ?>
                </ul>
            <?php }
            global $radio_query;
            $current_url = home_url($wp->request) . '/' ?>
            <div class="align-center">

                <?php //echo '<a class="load-mored btn" data-current-url="' . $current_url . '" data-max-pages="' . $radio_query->max_num_pages . '">loading... </a><span class="loader"></span>';  ?>


                <?php if (wp_is_mobile()) { ?>
                    <a class="scroll_load_m load-mored btn">loading...</a>
                <?php } else { ?>
                    <a class="scroll_load load-mored btn">loading...</a>

                <?php } ?>
            </div>
        </div>
        <style type="text/css">
            .archive_list li {
                display: none;
            }
        </style>

        <script type="text/javascript">

            jQuery(document).on({
                ajaxStart: function () {
                    jQuery('span.loader').addClass("loading");
                },
                ajaxStop: function () {
                    jQuery('span.loader').removeClass("loading");
                }
            });

            jQuery(document).ready(function () {

                des_member_list_li = jQuery(".archive_list.radio_post_list_desk li").size();
                MB = 12;

                jQuery('.archive_list.radio_post_list_desk li:lt(' + MB + ')').show();
                jQuery('.scroll_load').live("click", function () {
                    MB = (MB + 6 <= des_member_list_li) ? MB + 6 : des_member_list_li;
                    jQuery('.archive_list.radio_post_list_desk li:lt(' + MB + ')').show();
                    var member_list = jQuery('.archive_list.radio_post_list_desk li').length;
                    if (MB == des_member_list_li) {
                        jQuery('.scroll_load').hide();
                    }
                });


                mob_member_list_li = jQuery(".archive_list.radio_post_list_mob  li").size();

                MBS = 6;
                jQuery('.archive_list.radio_post_list_mob li:lt(' + MBS + ')').show();
                jQuery('.scroll_load_m').live("click", function () {
                    MBS = (MBS + 6 <= mob_member_list_li) ? MBS + 6 : mob_member_list_li;
                    jQuery('.archive_list.radio_post_list_mob li:lt(' + MBS + ')').show();
                    var member_list = jQuery('.archive_list.radio_post_list_mob li').length;
                    if (MBS == mob_member_list_li) {
                        jQuery('.scroll_load_m').hide();
                    }
                });


            });

        </script>
    </div>
    <!--radio archieve_END-->

    <div class="contact_callout mobile">
        <div class="container">
            <?php if (get_field('contact_callout_heading')) { ?>
                <h2><?php echo get_field('contact_callout_heading'); ?></h2>
            <?php } ?>
            <?php if (get_field('contact_callout_button_link')) { ?>
                <a href="<?php echo get_field('contact_callout_button_link'); ?>"
                   class="btn"><?php echo get_field('contact_callout_button_text'); ?></a>
            <?php } ?>

        </div>
    </div>

<?php get_footer(); ?>