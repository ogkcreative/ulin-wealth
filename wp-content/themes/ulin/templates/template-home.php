<?php
/* Template Name: Homepage */
get_header(); ?>
<a class="finra" href="https://brokercheck.finra.org/" target="_blank"><img
            src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-brokercheck-b.png"></a>
<div class="intro_section">
    <?php $copy_headline = get_field('copy_headline');
    $copy_paragraph      = get_field('copy_paragraph');
    $custom_link_text    = get_field('custom_link_text');
    $button_url          = get_field('button_url');
    ?>
    <div class="intro_text">
        <div class="text_inner">
            <?php if ($copy_headline) { ?>
                <h2><?php echo $copy_headline; ?> </h2>
            <?php } ?>
            <?php echo $copy_paragraph; ?>
            <?php if ($button_url) { ?>
                <a href="<?php echo $button_url; ?>" class="btn"><?php echo $custom_link_text; ?></a>
            <?php } ?>
        </div>
    </div><!--Intro_video_End-->
    <div class="intro_video">
        <?php $video_url   = get_field('video_url');
        $placeholder_image = wp_get_attachment_image_url(get_field('placeholder_image_for_video'), 'full');
        ?>
        <?php if ($video_url) { ?>
            <?php echo do_shortcode('[video width:100%; autoplay="0" poster="'.$placeholder_image.'" loop="1" controls="0" src="'.$video_url.'"]'); ?>
        <?php } ?>
    </div>
</div>
<!--Intro_Section_End-->
<div class="container">
    <?php $full_copy_heading = get_field('full_copy_heading');
    $full_copy_content       = get_field('full_copy_content'); ?>
    <div class="text-block align-center">
        <h2><?php echo $full_copy_heading; ?></h2>
        <?php echo $full_copy_content; ?>
    </div>
</div>
<!--text-block_END-->

<?php if (have_rows('featured_in_carousel')): ?>
<div class="featured_main">
    <div class="container">
        <a href="/news-events/" title="news-events">
            <h2>Featured In</h2>
        </a>
        <div class="owl-carousel featured_slide">
            <?php while (have_rows('featured_in_carousel')): the_row();
                $featured_logo = wp_get_attachment_image(get_sub_field('featured_logo'), 'full');
                $featured_link = (get_sub_field('link')) ? get_sub_field('link') : ["url" => "/news-events"];
                ?>
                <?php if ($featured_logo) { ?>
                    <a href="<?= (isset($featured_link['url'])) ? $featured_link['url'] : "javascript:void(0)" ?>"
                       title="<?= (isset($featured_link['title'])) ? $featured_link['title'] : "" ?>"
                       target="<?= (isset($featured_link['target'])) ? $featured_link['target'] : "_parent" ?>">
                        <div class="item"> <?php echo $featured_logo; ?></div>
                    </a>
                <?php } ?>
            <?php endwhile; ?>
        </div>
    </div>
</div>
<?php endif; ?>
<!--Featured_END-->

<div class="three_sections">

    <?php if (have_rows('3sections')): ?>
        <?php $i = 1;
        while (have_rows('3sections')): the_row();
            $three_section_headline = get_sub_field('3_section_headline');
            $short_content          = get_sub_field('short_content');
            $custom_button          = get_sub_field('custom_button');
            $button_text            = get_sub_field('button_text');
            $button_link            = get_sub_field('button_link'); ?>


            <div class="column <?php if ($i == 1) {
                echo "one";
            } elseif ($i == 2) {
                echo "two";
            } else {
                echo "three";
            } ?>">
                <h2><?php echo $three_section_headline; ?></h2>
                <?php echo $short_content; ?>

                <?php if ($custom_button == '1') { ?>
                    <a href="<?php echo $button_link; ?>" class="btn white"><?php echo $button_text; ?></a>
                <?php } ?>
            </div>
            <?php $i++; endwhile; endif; ?>

</div> <!--three_sections_END-->

<?php if (get_field('radio_heading')) { ?>
<div class="radio_callout"
     style="background-image:url(<?php echo wp_get_attachment_image_url(get_field('radio_bg_image'), 'full'); ?>);">
    <div class="container">
        <h2><?php echo get_field('radio_heading'); ?></h2>
        <?php if (get_field('radio_button_link')) { ?>
            <a href="<?php echo get_field('radio_button_link'); ?>"
               class="btn white"><?php echo get_field('listing_button_text'); ?></a>
        <?php } ?>
    </div>
</div> <!--radio_callout_END-->
<?php } ?>
<?php if (get_field('news_heading')) { ?>
<div class="news_callout">
    <div class="container">
        <h2><?php echo get_field('news_heading'); ?></h2>
        <?php if (get_field('news_button_link')) { ?>
            <a href="<?php echo get_field('news_button_link'); ?>"
               class="btn"><?php echo get_field('news_listing_button_text'); ?></a>
        <?php } ?>
    </div>
</div> <!--radio_callout_END-->
<?php } ?>
<div class="guides-section">
    <div class="container">
        <div class="text-wrap">
            <a href="<?= get_field('guides_title_link') ?>" class="title-link"><h2><?= get_field('guides_title') ?></h2></a>
            <p><?= get_field('guides_text') ?></p>
        </div>
        <?php if(have_rows('guides_buttons')): ?>
            <div class="btn-wrap">
                <?php while(have_rows('guides_buttons')): the_row(); ?>
                <?php $button = get_sub_field('button'); ?>
                    <a href="<?= $button['url'] ?>" class="btn"><?= $button['title'] ?></a>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>
    </div>
</div>
<div class="logos-section">
    <div class="container">
        <h2><?= get_field('logos_title') ?></h2>
        <?php $logos_gallery = get_field('logos_gallery');
        if($logos_gallery): ?>
            <div class="logos">
                <?php foreach( $logos_gallery as $logo ): ?>
                    <div class="logo">
                        <?= wp_get_attachment_image($logo, 'full') ?>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
        <?php $logos_button = get_field('logos_button');
        if($logos_button): ?>
            <div class="btn-wrap">
                <a href="<?= $logos_button['url'] ?>" class="btn"><?= $logos_button['title'] ?></a>
            </div>
        <?php endif; ?>
    </div>
</div>
<div class="upcoming_event" style="display: none;">
    <div class="container">
        <h2>Upcoming Events</h2>

        <?php $event_id = '11';
        $current_date   = date('Y-m-d');

        $event_args = array(
            'post_type'      => 'news_events',
            'meta_key'       => 'event_datepicker',
            'orderby'        => 'meta_value_num',
            'order'          => 'ASC',
            'posts_per_page' => '-1',
            'tax_query'      => array(
                array(
                    'taxonomy' => 'news_events_cat',
                    'field'    => 'term_id',
                    'terms'    => $event_id,

                ),
            ),
        );

        $event_args['meta_query'] = array(
            array(
                'key'     => 'event_datepicker',
                'value'   => $current_date,
                'compare' => '>=',
                'type'    => 'DATE',
            ),

        );
        $event_query              = new WP_Query($event_args);
        if ($event_query->have_posts()) : ?>
            <ul class="owl-carousel events ">
                <?php while ($event_query->have_posts()) : $event_query->the_post();
                    $event_datepicker = get_field('event_datepicker');
                    $date             = new DateTime($event_datepicker);
                    $event_date       = $date->format('F j, Y'); //
                    $button_open_link = get_field('button_open_link'); ?>
                    <li class="item">
                        <?php if ($event_date) { ?>
                            <h3><?php echo $event_date; ?> </h3>
                        <?php } ?>
                        <?php if (get_the_title()) { ?>
                            <h4><?php echo mb_strimwidth(get_the_title(), 0, 60, '...'); ?></h4>
                        <?php } ?>
                        <?php if (get_field('register_button_link')) { ?>
                            <a <?php if ($button_open_link == 'newtab') {
                                echo "target='_blank'";
                            } ?> href="<?php echo get_field('register_button_link'); ?>" class="btn">register</a>
                        <?php } ?>

                    </li>
                <?php endwhile;
                wp_reset_postdata(); ?>
            </ul>
        <?php endif; ?>
    </div>
</div>
<!--Upcoming Events_END-->

<div class="blog_and_news">
    <div class="container">
        <!--Blog_post_End-->
        <div class="news_post">
            <a class="blog-link" href="/blog/">
                <h2>Gain A Wealth of Insight<sup>®</sup> Blog
                </h2>
            </a>

            <?php $news_id = '10';

            $news_args  = array(
                'post_type'      => 'post',
                'order'          => 'DESC',
                'posts_per_page' => '3',
                'post_status'    => 'publish'
            );
            $news_query = new WP_Query($news_args);
            if ($news_query->have_posts()) : ?>
                <ul class="post_list">
                    <?php while ($news_query->have_posts()) : $news_query->the_post();
                        $blogimage = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'blog-thumb', false, '');
                        ?>
                        <li>
                            <?php if ($blogimage) { ?>
                                <div class="post_thumb">
                                    <a href="<?php the_permalink(); ?>"><img src="<?php echo $blogimage[0]; ?>"
                                                                             alt=""></a>
                                </div>
                            <?php } ?>
                            <div class="post_disc">
                                <!--<h3 class="mobile"><?php //the_time( 'F j, Y' ) ?></h3>-->
                                <?php if (get_the_title()) { ?>
                                    <h4>
                                        <a href="<?php the_permalink(); ?>"><?php
                                            echo mb_strimwidth(get_the_title(), 0, 75, '...');

                                            ?></a>
                                    </h4>
                                <?php } ?>
                                <h3>
                                    <span><?php the_time('F j, Y') ?></span>
                                </h3>
                            </div>
                        </li>
                    <?php endwhile;
                    wp_reset_postdata(); ?>
                </ul>
            <?php endif; ?>
        </div>
        <!--Blog_post_End-->
    </div>
</div>

<?php get_footer(); ?>
