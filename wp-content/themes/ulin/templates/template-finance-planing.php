<?php
/* Template Name: Side Bar Categories */
get_header(); ?>

<?php $sidenavigation = get_field('side_categories');
?>
<div class="container finance_content">
    <div class="page_container">
        <div class="sidebar secondary_nav">
            <span class="select-dropdown"><?php the_title(); ?></span>
            <ul class="sidenav_list" page-id="<?php echo get_the_id(); ?>">
                <?php $menu_id = 1;
                foreach ($sidenavigation as $sidenav) {
                    $nav_id        = $menu_id;
                    $nav_hash      = (isset($sidenav['hash'])) ? $sidenav['hash'] : "planning_".$menu_id;
                    $side_menu     = $sidenav['side_navigation'];
                    $side_sub_menu = $sidenav['sub_categories']; ?>
                    <li id="<?php echo $nav_hash; ?>" class="sde-menu <?php if ($side_sub_menu) {
                        echo "menu-item-has-children";
                    } ?>">
                        <a href="<?= $sidenav["side_nav_slug"]; ?>" class="<?php if ($sidenav['sub_active']) {
                            echo "active_cat";
                        } ?>" catid="<?php echo $nav_id; ?>"><?php echo $side_menu; ?></a>
                        <?php if ($side_sub_menu) { ?>
                            <ul class="sub-menu sub_categories" parid="<?php echo $menu_id; ?>">
                                <?php $sub_cat = 1;
                                foreach ($side_sub_menu as $side_sub_nav) {
                                    // print_r($side_sub_nav);
                                    $sub_cat_id         = $menu_id.$sub_cat;
                                    $sub_cat_navigation = $side_sub_nav['sub_cat_navigation']; ?>

                                    <li>
                                        <a href="<?= $sidenav["side_nav_slug"]; ?>" sub_catid="<?php echo $sub_cat_id; ?>">
                                            <?php echo $sub_cat_navigation; ?>
                                        </a>
                                    </li>

                                    <?php $sub_cat++;
                                } ?>

                            </ul>
                            <div class="hide">

                            </div>
                        <?php } ?>
                    </li>
                    <?php $menu_id++;
                } ?>
            </ul>
        </div>

        <div class="page_article">
            <div class="loading_img" style="display:none;">
                <img
                        src="<?php echo site_url(); ?>/wp-content/uploads/2018/08/loading.gif">
            </div>
            <div class="replace_filter_html">
                <?php if (have_rows('side_categories')): ?>
                    <ul class="sidenav_content">

                        <?php $i = 1;
                        while (have_rows('side_categories')): the_row();
                            $headline       = get_sub_field('right_side_headline');
                            $paragraph_text = get_sub_field('right_side_paragraph_text'); ?>
                            <?php if (get_sub_field('sub_active')) { ?>
                                <li>

                                    <h2><?php echo $headline; ?></h2>

                                    <p><?php echo $paragraph_text; ?></p>

                                </li>
                            <?php } ?>
                            <?php $i++; endwhile; ?>

                    </ul>
                <?php endif; ?>
            </div>
            <?php if (have_rows('side_categories')): ?>
            <div class="form hide" id="form_holder">
               <?php while (have_rows('side_categories')): the_row();
                       $form_id =   get_sub_field('form_id');
                       if($form_id)
                           {
                                gravity_form($form_id, false, false, false, '', true, 1);
                           }
                   endwhile;
               ?>

            </div>
            <?php endif; ?>
            <div class="dots_outer">
                <ul class="square_indicators ">
                    <?php $n = 1;
                    foreach ($sidenavigation as $sidenav) { ?>
                        <li class="<?php if ($n == 1) {
                            echo "active_indicator";
                        } ?>" id="indicator_<?php echo $n; ?>"></li>
                        <?php $n++;
                    } ?>
                </ul>
            </div>
        </div>

    </div>
</div>


<div class="container">
    <div class="archive_section featured_news">
        <a class="blog-link" href="/blog/">
            <h2>A Wealth of Insight<sup>®</sup> Blog
            </h2>
        </a>
        <?php $news_id     = '10';
        $news_artical_args = array(
            'post_type'      => 'post',
            'order'          => 'DESC',
            'posts_per_page' => '3',
        );

        $news_artical_query = new WP_Query($news_artical_args); ?>
        <?php if ($news_artical_query->have_posts()) { ?>
            <ul class="archive_list">
                <?php while ($news_artical_query->have_posts()):
                    $news_artical_query->the_post();
                    $news_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail');

                    ?>
                    <li>
                        <div class="post_thumb">
                            <a href="<?php the_permalink(); ?>">
                                <?php if ($news_image) { ?>
                                    <img src="<?php echo $news_image[0]; ?>" alt="">
                                <?php } else { ?>
                                    <img src="<?php bloginfo('template_url') ?>/assets/images/placeholder.png"
                                         alt="">
                                <?php } ?>
                            </a>
                        </div>
                        <div class="post_disc">
                            <h4>
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            </h4>
                            <h3><?php echo get_the_date('F j, Y') ?> </h3>
                        </div>
                    </li>
                <?php endwhile;
                wp_reset_postdata(); ?>
            </ul>
        <?php } ?>
    </div>
</div>
<div class="contact_callout">
    <div class="container">
        <?php echo get_field('contact_callout'); ?>

    </div>
</div>
<?php get_footer(); ?>
