<?php
/* Template Name: News Events */
get_header(); ?>


    <div class="container">
        <div class="archive_section">
            <h2>News Articles</h2>
            <section class="basic-page-content">
                <?= get_field('page_content'); ?>
            </section>
        </div>
        <script type="text/javascript">

            jQuery(document).on({
                ajaxStart: function () {
                    jQuery('span.loader').addClass("loading");
                },
                ajaxStop: function () {
                    jQuery('span.loader').removeClass("loading");
                }
            });

            jQuery(document).ready(function () {

                var count = 1;
                jQuery(".load-mored").click(function () {
                    count++;
                    var max_pages = jQuery(this).attr('data-max-pages');


                    var page_url = jQuery(this).attr('data-current-url');
                    jQuery.ajax({
                        url: page_url + "page/" + count,
                        success: function (data) {
                            jQuery(data).find(".archive_list li").appendTo("ul.archive_list");
                        }
                    });
                    if (max_pages == count) {
                        jQuery(".load-mored").hide(500);
                        jQuery('span.loader').removeClass("loading");
                    } // Hide Button if no more post available

                });


            });

        </script>
    </div>
    <!--Contact_form_END-->
<?php if (have_rows('Two_cta_section')): ?>
    <div class="two_cta">
        <?php $t = 1;
        while (have_rows('Two_cta_section')): the_row();
            $title = get_sub_field('ftp_cta_title');
            $button_text = get_sub_field('ftp_cta_button_text');
            $button_url = get_sub_field('ftp_cta_button_url'); ?>
            <div class="<?php if ($t == 1) {
                echo "news_cta";
            } else {
                echo "radio_cta";
            } ?> align-center">
                <?php if ($title) { ?>
                    <h2><?php echo $title; ?></h2>
                <?php } ?>
                <?php if ($button_url) { ?>
                    <a href="<?php echo $button_url; ?>" class="btn white"><?php echo $button_text; ?></a>
                <?php } ?>
            </div>
            <?php $t++; endwhile; ?>
    </div>
<?php endif; ?>
    <!--Two_cta_END-->

<?php get_footer(); ?>