<?php
/* Template Name: Blog */
get_header();
$taxonomy = 'category';
$tax_terms = get_terms($taxonomy, array('hide_empty' => false));
$blog_posts = new WP_Query(array('post_type' => 'post', 'post_status' => 'publish', 'posts_per_page' => -1, 'paged' => 1));
//==Total Post===//
$total_post_count = new WP_Query(array('post_type' => 'post', 'post_status' => 'publish'));
$total_posts = ceil($total_post_count->found_posts / 4); ?>

    <div class="container news_post">
        <div class="blog_container">
            <div class="sidebar secondary_nav">
                <h2><?= get_field('blog_title') ?></h2>
                <span class="select-dropdown">FILTER BY TOPIC</span>
                <ul class="blog_list filter_category_menu">
                    <?php foreach ($tax_terms as $tax_term) {
                        $taxid = $tax_term->term_id; ?>
                        <li class="all item"><a catid="<?php echo $taxid; ?>"><?php echo $tax_term->name ?></a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
            <?php if (!empty($blog_posts)) { ?>
                <div class="blog_article">
                    <div class="loading_img" style="display:none;"><img
                                src="<?php echo site_url(); ?>/wp-content/uploads/2018/08/loading.gif"></div>
                    <div class="replace_filter_html">
                        <input type="hidden" name="cat_id" class="cat_id_for_pagination" value="0">
                        <ul class="blog_posts news_post_list_desk">
                            <?php while ($blog_posts->have_posts()) {
                                $blog_posts->the_post();
                                $blogimage = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'blog-thumb', false, '');
                                ?>
                                <li>
                                    <?php if ($blogimage) { ?>
                                        <div class="post_thumb">
                                            <a href="<?php the_permalink(); ?>"><img src="<?php echo $blogimage[0]; ?>"
                                                                                     alt=""></a>
                                        </div>
                                    <?php } ?>
                                    <div class="post_disc">
                                        <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                        <h3><?php echo get_the_date('F j, Y') ?> </h3>
                                    </div>
                                </li>
                            <?php }
                            wp_reset_query(); ?>
                        </ul>
                        <?php $count_post = $blog_posts->post_count;
                        if ($count_post > 4) { ?>
                            <div class="align-center">
                                <a class="load-mored btn load_more btn-load-more scroll_load">Loading... </a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="blog_subscribe">
        <?php $newsletter = get_field('inner_page_event_information_form_title', 'options'); ?>
        <div class="container">
            <?php if ($newsletter) { ?> <h2><?php echo $newsletter; ?></h2><?php } ?>
            <?= get_field('add_form') ?>
        </div>
    </div>
    <!--blog_subscribe_END-->

<?php if (have_rows('Two_cta_section')): ?>
    <div class="two_cta">
        <?php $t = 1;
        while (have_rows('Two_cta_section')): the_row();
            $title = get_sub_field('ftp_cta_title');
            $button_text = get_sub_field('ftp_cta_button_text');
            $button_url = get_sub_field('ftp_cta_button_url'); ?>
            <div class="<?php if ($t == 1) {
                echo "news_cta";
            } else {
                echo "radio_cta";
            } ?> align-center">
                <?php if ($title) { ?>
                    <h2><?php echo $title; ?></h2>
                <?php } ?>
                <?php if ($button_url) { ?>
                    <a href="<?php echo $button_url; ?>" class="btn white"><?php echo $button_text; ?></a>
                <?php } ?>
            </div>
            <?php $t++; endwhile; ?>
    </div>
<?php endif; ?>
    <!--Two_cta_END-->

<?php get_footer(); ?>
