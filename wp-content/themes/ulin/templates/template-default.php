<?php
/* Template Name: Basic Page */
get_header(); ?>



<section class="basic-page-content">
    <div class="container">
        <div class="contact_form">
            <h2><?= get_field('page_heading'); ?></h2>
            <?= get_field('page_content'); ?>
        </div>

    </div>
</section>
<?php if (!is_page('client-center')): ?>
<div class="contact_callout">
    <div class="container">
        <?php echo get_field( 'contact_callout' ); ?>
    </div>
</div>
<?php endif; ?> 




<?php get_footer();
