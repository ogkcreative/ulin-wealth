<?php
/* Template Name: About */
get_header(); ?>
<?php $sidenavigation = get_field( 'side_categories' );
?>
	<div class="container about_content top_filter_sec">
		<div class="page_container">
			<div class="sidebar secondary_nav">
				<span class="select-dropdown"><?php the_title(); ?></span>
				<ul class="sidenav_list">
					<?php $menu_id = 1;
					foreach ( $sidenavigation as $sidenav ) {
						$nav_id    = $menu_id;
						$side_menu = $sidenav['side_navigation'];
						?>
						<li id="about_<?php echo $nav_id; ?>">
                            <a href="<?= $sidenav["side_nav_slug"]; ?>" class="<?php if ($sidenav['sub_active'] ) {
								echo "active_cat";
							} ?>" catid="<?php echo $nav_id; ?>"><?php echo $side_menu; ?></a>
						</li>
						<?php $menu_id ++;
					} ?>
				</ul>
			</div>

			<div class="page_article">
				<div class="loading_img" style="display:none;"><img
							src="<?php echo site_url(); ?>/wp-content/uploads/2018/08/loading.gif"></div>
				<div class="replace_filter_html bbb">

					<?php if ( have_rows( 'side_categories' ) ): ?>

						<ul class="sidenav_content">

							<?php $i = 1;
							while ( have_rows( 'side_categories' ) ): the_row();
								$headline       = get_sub_field( 'right_side_headline' );
								$paragraph_text = get_sub_field( 'right_side_paragraph_text' ); ?>
								<?php if ( get_sub_field('sub_active') ) { ?>
									<li>

										<h2><?php echo $headline; ?></h2>

										<?php echo $paragraph_text; ?>

									</li>
								<?php } ?>

								<?php $i ++; endwhile; ?>

						</ul>
					<?php endif; ?>

				</div>
				<div class="dots_outer">
					<ul class="square_indicators">
						<?php $n = 1;
						foreach ( $sidenavigation as $sidenav ) { ?>
							<li class="<?php if ( $n == 1 ) {
								echo "active_indicator";
							} ?>" id="indicator_<?php echo $n; ?>"></li>
							<?php $n ++;
						} ?>
					</ul>
				</div>
			</div>


		</div>
	</div>

	<div class="container" id="teams">
		<div class="team_members">
			<?php if ( get_field( 'team_heading' ) ) { ?>
				<h2><?php echo get_field( 'team_heading' ); ?></h2>
			<?php } ?>
			<?php if ( have_rows( 'our_team' ) ): $c = 1; ?>
				<ul class="members">
					<?php while ( have_rows( 'our_team' ) ): the_row();
						$team_image         = wp_get_attachment_image( get_sub_field( 'team_image' ), 'full' );
						$team_name          = get_sub_field( 'team_name' );
						$designation        = get_sub_field( 'team_designation' );
						$team_short_content = get_sub_field( 'team_short_content' ); ?>
						<li>
							<?php if ( $team_image ) { ?>
								<div class="member_pic"> <?php echo $team_image; ?></div>
							<?php } ?>
							<div class="member_disc">
								<?php if ( $team_name ) { ?>
									<h4><?php echo $team_name; ?></h4>
								<?php } ?>
								<?php if ( $designation ) { ?>
									<h3><?php echo $designation; ?></h3>
								<?php } ?>
								<?php
								$wrapped = wordwrap( $team_short_content, 225 );
								$lines   = explode( "\n", $wrapped );
								$new_str = $lines[0] . '...'; ?>
								<div class="excerpt">
									<p><?= $new_str; ?></p>
									<a class="toggle-more" href="#inline_<?= $c ?>" data-lity> Read More</a></div>
								<!--								<div class="full-desc hide">-->
								<!--									<p>--><? //= $team_short_content ?><!--</p>-->
								<!--									<a class="toggle-hide">Show Less</a>-->
								<!--								</div>-->
							</div>
							<div id="inline_<?= $c ?>" class="lity-hide">
								<div class="container">
									<?php if ( $team_image ) { ?>
										<div class="member_pic"> <?php echo $team_image; ?></div>
									<?php } ?>
									<div class="member_disc">
										<?php if ( $team_name ) { ?>
											<h4><?php echo $team_name; ?></h4>
										<?php } ?>
										<?php if ( $designation ) { ?>
											<h3><?php echo $designation; ?></h3><br>
										<?php } ?>
										<div class="content">
											<p> <?= $team_short_content ?>  </p>
										</div>
									</div>
								</div>
							</div>
						</li>
						<?php $c ++; endwhile; ?>
				</ul>
			<?php endif; ?>
		</div>
	</div>
	<!--team_members_END-->
	<div class="privacy_and_disclosure" style="display: none;">
		<div class="privacy">
			<?php if ( get_field( 'privacy_title' ) ) { ?>
				<h2><?php echo get_field( 'privacy_title' ); ?></h2>
			<?php } ?>
			<?php echo get_field( 'privacy_content' ); ?>
			<?php if ( get_field( 'download_file' ) ) { ?>
				<a href="<?php echo get_field( 'download_file' ); ?>" class="btn"
				   download><?php echo get_field( 'download_button_text' ); ?></a>
			<?php } ?>
		</div>
		<div class="disclosure">
			<?php if ( get_field( 'disclosure_title' ) ) { ?>
				<h2><?php echo get_field( 'disclosure_title' ); ?></h2>
			<?php } ?>
			<?php echo get_field( 'disclosure_content' ); ?>
		</div>
	</div>
    <div class="contact_callout">
        <div class="container">
            <?php echo get_field( 'contact_callout' ); ?>

        </div>
    </div>


<?php get_footer(); ?>