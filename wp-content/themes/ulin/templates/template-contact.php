<?php
/* Template Name: Contact */
get_header(); ?>

    <section class="contact_info">
        <div class="container">
            <?php if (get_field('info_heading')) { ?>
                <h2><?php echo get_field('info_heading'); ?></h2>
            <?php } ?>
            <div class="information_right">
                <div class="column first">
                    <?php if (get_field('contact_address')) { ?>
                        <a href="<?php get_field('google_maps__link'); ?>"
                           class="address"><?php echo get_field('contact_address'); ?></a>
                    <?php } ?>
                </div>
                <div class="column">
                    <?php if (get_field('contact_email')) { ?>
                        <a href="mailto:<?php echo get_field('contact_email'); ?>"
                           class="mail"><?php echo get_field('contact_email'); ?></a>
                    <?php } ?>
                    <?php if (get_field('contact_phone')) { ?>
                        <a href="tel:<?php echo str_replace("-", "", get_field('contact_phone')); ?>"
                           class="phone"><?php echo get_field('contact_phone'); ?></a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
    <!--contact_info_END-->
    <div class="container">
        <div class="contact_form">
            <?php if (get_field('form_heading')) { ?><h2><?php echo get_field('form_heading'); ?></h2>
            <?php } ?>
        </div>
    </div>
    <div class="contact_callout">
        <div class="container">
            <?php echo get_field('contact_callout'); ?>
        </div>
    </div>
    <!--Contact_form_END-->
<?php if (have_rows('Two_cta_section')): ?>
    <div class="two_cta">
        <?php $t = 1;
        while (have_rows('Two_cta_section')): the_row();
            $title = get_sub_field('ftp_cta_title');
            $button_text = get_sub_field('ftp_cta_button_text');
            $button_url = get_sub_field('ftp_cta_button_url'); ?>
            <div class="<?php if ($t == 1) {
                echo "news_cta";
            } else {
                echo "radio_cta";
            } ?> align-center">
                <?php if ($title) { ?>
                    <h2><?php echo $title; ?></h2>
                <?php } ?>
                <?php if ($button_url) { ?>
                    <a href="<?php echo $button_url; ?>" class="btn white"><?php echo $button_text; ?></a>
                <?php } ?>
            </div>
            <?php $t++; endwhile; ?>
    </div>
<?php endif; ?>
    <!--Two_cta_END-->

<?php get_footer(); ?>