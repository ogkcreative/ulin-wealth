window.gwrf;
jQuery(document).ready(function () {
  jQuery(".news_post ul.filter_category_menu > li > a").click(function (e) {
    e.preventDefault();
    jQuery('input.qty').val(1);
    jQuery(".loading_img").show();
    jQuery(".news_post ul.filter_category_menu li a").removeClass('active_cat');
    jQuery(this).addClass('active_cat');
    catid = jQuery(this).attr("catid");
    jQuery.ajax({
      url: ajaxurl,
      type: "post",
      data: {
        action: "filter_by_ajax_post",
        catid: catid
      },
      success: function (response) {
        jQuery(".replace_filter_html").html(response);
        AA = 4;
        var count_all_post = jQuery('.count_all_post').val();
        jQuery(".total_page_count").html(count_all_post);
        jQuery('input.qty').val(1);
        jQuery('.news_post_list li:lt(' + AA + ')').show();
        jQuery(".loading_img").hide();
      }
    });
  });
  jQuery(".resource_posts ul.filter_category_menu > li > a").click(function (e) {
    e.preventDefault();
    jQuery('input.qty').val(1);
    jQuery(".loading_img").show();
    jQuery(".resource_posts ul.filter_category_menu li a").removeClass('active_cat');
    jQuery(this).addClass('active_cat');
    catid = jQuery(this).attr("catid");
    jQuery.ajax({
      url: ajaxurl,
      type: "post",
      data: {
        action: "filter_by_ajax_resource_post",
        catid: catid
      },
      success: function (response) {
        jQuery(".replace_filter_html").html(response);
        BB = 4;
        var count_all_post = jQuery('.count_all_post').val();
        jQuery(".total_page_count").html(count_all_post);
        jQuery('input.qty').val(1);
        jQuery('.resource_post_list li:lt(' + BB + ')').show();
        jQuery(".loading_img").hide();
      }
    });
  });
  jQuery(".about_content ul.filter_category_menu > li > a").click(function (e) {
    e.preventDefault();
    jQuery(".loading_img").show();
    jQuery(".about_content ul.sidenav_list.filter_category_menu li a").removeClass('active_cat');
    jQuery(".square_indicators li").removeClass('active_indicator');
    jQuery(this).addClass('active_cat');
    con_id = jQuery(this).attr("catid");
    jQuery("#indicator_" + con_id).addClass('active_indicator');
    jQuery.ajax({
      url: ajaxurl,
      type: "post",
      data: {
        action: "filter_by_ajax_content",
        con_id: con_id
      },
      success: function (response) {
        jQuery(".replace_filter_html").html(response);
        jQuery(".loading_img").hide();
      }
    });
  });
  jQuery(".finance_content .sidebar ul.filter_category_menu > li > a").click(function (e) {
    e.preventDefault();
    jQuery(".loading_img").show(); 

    jQuery(".finance_content ul.filter_category_menu li a").removeClass('active_cat');
    jQuery(".square_indicators li").removeClass('active_indicator');
    jQuery(this).addClass('active_cat');
    con_id = jQuery(this).attr("catid");
    var page_id = jQuery('.sidenav_list.filter_category_menu').attr('page-id'); 

    jQuery("#indicator_" + con_id).addClass('active_indicator');
    jQuery.ajax({
      url: ajaxurl,
      type: "post",
      data: {
        action: "filter_by_ajax_finance_content_page",
        con_id: con_id,
        page_id: page_id
      },
      success: function (response) {
        jQuery(".replace_filter_html").html(response);
        jQuery(".loading_img").hide();
        jQuery("#form_holder").removeClass('hide');
      }
    });
  });
  jQuery(".finance_content .sidebar ul.filter_category_menu > li ul.sub-menu li a").click(function (e) {
    e.preventDefault();
    jQuery(".loading_img").show();
    jQuery('.sub-menu.sub_categories li a').removeClass('active_cat');
    jQuery(".square_indicators li").removeClass('active_indicator');
    jQuery(this).addClass('active_cat');
    var page_id = jQuery('.sidenav_list.filter_category_menu').attr('page-id'); 

    child_id = jQuery(this).attr("sub_catid");
    par_id = jQuery(this).parents('.sub-menu').attr("parid");
    jQuery("#indicator_" + par_id).addClass('active_indicator');
    jQuery.ajax({
      url: ajaxurl,
      type: "post",
      data: {
        action: "filter_by_ajax_finance_sub_content",
        child_id: child_id,
        page_id: page_id
      },
      success: function (response) {
        jQuery(".replace_filter_html").html(response);
        jQuery(".loading_img").hide(); 

        jQuery("#form_holder").removeClass('hide');
      }
    });
  });
});
(function (factory) {
  if (typeof define == 'function' && define.amd) {
    define(['jquery'], factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(require('jquery'));
  } else {
    factory(jQuery);
  }
})(function ($) {
  var inviewObjects = [],
      viewportSize,
      viewportOffset,
      d = document,
      w = window,
      documentElement = d.documentElement,
      timer;
  $.event.special.inview = {
    add: function (data) {
      inviewObjects.push({
        data: data,
        $element: $(this),
        element: this
      }); 

      if (!timer && inviewObjects.length) {
        timer = setInterval(checkInView, 250);
      }
    },
    remove: function (data) {
      for (var i = 0; i < inviewObjects.length; i++) {
        var inviewObject = inviewObjects[i];

        if (inviewObject.element === this && inviewObject.data.guid === data.guid) {
          inviewObjects.splice(i, 1);
          break;
        }
      } 


      if (!inviewObjects.length) {
        clearInterval(timer);
        timer = null;
      }
    }
  };

  function getViewportSize() {
    var mode,
        domObject,
        size = {
      height: w.innerHeight,
      width: w.innerWidth
    }; 

    if (!size.height) {
      mode = d.compatMode;

      if (mode || !$.support.boxModel) {
        domObject = mode === 'CSS1Compat' ? documentElement : 
        d.body; 

        size = {
          height: domObject.clientHeight,
          width: domObject.clientWidth
        };
      }
    }

    return size;
  }

  function getViewportOffset() {
    return {
      top: w.pageYOffset || documentElement.scrollTop || d.body.scrollTop,
      left: w.pageXOffset || documentElement.scrollLeft || d.body.scrollLeft
    };
  }

  function checkInView() {
    if (!inviewObjects.length) {
      return;
    }

    var i = 0,
        $elements = $.map(inviewObjects, function (inviewObject) {
      var selector = inviewObject.data.selector,
          $element = inviewObject.$element;
      return selector ? $element.find(selector) : $element;
    });
    viewportSize = viewportSize || getViewportSize();
    viewportOffset = viewportOffset || getViewportOffset();

    for (; i < inviewObjects.length; i++) {
      if (!$.contains(documentElement, $elements[i][0])) {
        continue;
      }

      var $element = $($elements[i]),
          elementSize = {
        height: $element[0].offsetHeight,
        width: $element[0].offsetWidth
      },
          elementOffset = $element.offset(),
          inView = $element.data('inview'); 

      if (!viewportOffset || !viewportSize) {
        return;
      }

      if (elementOffset.top + elementSize.height > viewportOffset.top && elementOffset.top < viewportOffset.top + viewportSize.height && elementOffset.left + elementSize.width > viewportOffset.left && elementOffset.left < viewportOffset.left + viewportSize.width) {
        if (!inView) {
          $element.data('inview', true).trigger('inview', [true]);
        }
      } else if (inView) {
        $element.data('inview', false).trigger('inview', [false]);
      }
    }
  }

  $(w).on("scroll resize scrollstop", function () {
    viewportSize = viewportOffset = null;
  }); 

  if (!documentElement.addEventListener && documentElement.attachEvent) {
    documentElement.attachEvent("onfocusin", function () {
      viewportOffset = null;
    });
  }
});
(function (window, factory) {
  var lazySizes = factory(window, window.document);
  window.lazySizes = lazySizes;

  if (typeof module == 'object' && module.exports) {
    module.exports = lazySizes;
  }
})(window, function l(window, document) {
  'use strict';

  if (!document.getElementsByClassName) {
    return;
  }

  var lazysizes, lazySizesConfig;
  var docElem = document.documentElement;
  var Date = window.Date;
  var supportPicture = window.HTMLPictureElement;
  var _addEventListener = 'addEventListener';
  var _getAttribute = 'getAttribute';
  var addEventListener = window[_addEventListener];
  var setTimeout = window.setTimeout;
  var requestAnimationFrame = window.requestAnimationFrame || setTimeout;
  var requestIdleCallback = window.requestIdleCallback;
  var regPicture = /^picture$/i;
  var loadEvents = ['load', 'error', 'lazyincluded', '_lazyloaded'];
  var regClassCache = {};
  var forEach = Array.prototype.forEach;

  var hasClass = function (ele, cls) {
    if (!regClassCache[cls]) {
      regClassCache[cls] = new RegExp('(\\s|^)' + cls + '(\\s|$)');
    }

    return regClassCache[cls].test(ele[_getAttribute]('class') || '') && regClassCache[cls];
  };

  var addClass = function (ele, cls) {
    if (!hasClass(ele, cls)) {
      ele.setAttribute('class', (ele[_getAttribute]('class') || '').trim() + ' ' + cls);
    }
  };

  var removeClass = function (ele, cls) {
    var reg;

    if (reg = hasClass(ele, cls)) {
      ele.setAttribute('class', (ele[_getAttribute]('class') || '').replace(reg, ' '));
    }
  };

  var addRemoveLoadEvents = function (dom, fn, add) {
    var action = add ? _addEventListener : 'removeEventListener';

    if (add) {
      addRemoveLoadEvents(dom, fn);
    }

    loadEvents.forEach(function (evt) {
      dom[action](evt, fn);
    });
  };

  var triggerEvent = function (elem, name, detail, noBubbles, noCancelable) {
    var event = document.createEvent('CustomEvent');

    if (!detail) {
      detail = {};
    }

    detail.instance = lazysizes;
    event.initCustomEvent(name, !noBubbles, !noCancelable, detail);
    elem.dispatchEvent(event);
    return event;
  };

  var updatePolyfill = function (el, full) {
    var polyfill;

    if (!supportPicture && (polyfill = window.picturefill || lazySizesConfig.pf)) {
      polyfill({
        reevaluate: true,
        elements: [el]
      });
    } else if (full && full.src) {
      el.src = full.src;
    }
  };

  var getCSS = function (elem, style) {
    return (getComputedStyle(elem, null) || {})[style];
  };

  var getWidth = function (elem, parent, width) {
    width = width || elem.offsetWidth;

    while (width < lazySizesConfig.minSize && parent && !elem._lazysizesWidth) {
      width = parent.offsetWidth;
      parent = parent.parentNode;
    }

    return width;
  };

  var rAF = function () {
    var running, waiting;
    var firstFns = [];
    var secondFns = [];
    var fns = firstFns;

    var run = function () {
      var runFns = fns;
      fns = firstFns.length ? secondFns : firstFns;
      running = true;
      waiting = false;

      while (runFns.length) {
        runFns.shift()();
      }

      running = false;
    };

    var rafBatch = function (fn, queue) {
      if (running && !queue) {
        fn.apply(this, arguments);
      } else {
        fns.push(fn);

        if (!waiting) {
          waiting = true;
          (document.hidden ? setTimeout : requestAnimationFrame)(run);
        }
      }
    };

    rafBatch._lsFlush = run;
    return rafBatch;
  }();

  var rAFIt = function (fn, simple) {
    return simple ? function () {
      rAF(fn);
    } : function () {
      var that = this;
      var args = arguments;
      rAF(function () {
        fn.apply(that, args);
      });
    };
  };

  var throttle = function (fn) {
    var running;
    var lastTime = 0;
    var gDelay = 125;
    var RIC_DEFAULT_TIMEOUT = 666;
    var rICTimeout = RIC_DEFAULT_TIMEOUT;

    var run = function () {
      running = false;
      lastTime = Date.now();
      fn();
    };

    var idleCallback = requestIdleCallback ? function () {
      requestIdleCallback(run, {
        timeout: rICTimeout
      });

      if (rICTimeout !== RIC_DEFAULT_TIMEOUT) {
        rICTimeout = RIC_DEFAULT_TIMEOUT;
      }
    } : rAFIt(function () {
      setTimeout(run);
    }, true);
    return function (isPriority) {
      var delay;

      if (isPriority = isPriority === true) {
        rICTimeout = 44;
      }

      if (running) {
        return;
      }

      running = true;
      delay = gDelay - (Date.now() - lastTime);

      if (delay < 0) {
        delay = 0;
      }

      if (isPriority || delay < 9 && requestIdleCallback) {
        idleCallback();
      } else {
        setTimeout(idleCallback, delay);
      }
    };
  }; 


  var debounce = function (func) {
    var timeout, timestamp;
    var wait = 99;

    var run = function () {
      timeout = null;
      func();
    };

    var later = function () {
      var last = Date.now() - timestamp;

      if (last < wait) {
        setTimeout(later, wait - last);
      } else {
        (requestIdleCallback || run)(run);
      }
    };

    return function () {
      timestamp = Date.now();

      if (!timeout) {
        timeout = setTimeout(later, wait);
      }
    };
  };

  var loader = function () {
    var preloadElems, isCompleted, resetPreloadingTimer, loadMode, started;
    var eLvW, elvH, eLtop, eLleft, eLright, eLbottom;
    var defaultExpand, preloadExpand, hFac;
    var regImg = /^img$/i;
    var regIframe = /^iframe$/i;
    var supportScroll = 'onscroll' in window && !/glebot/.test(navigator.userAgent);
    var shrinkExpand = 0;
    var currentExpand = 0;
    var isLoading = 0;
    var lowRuns = -1;

    var resetPreloading = function (e) {
      isLoading--;

      if (e && e.target) {
        addRemoveLoadEvents(e.target, resetPreloading);
      }

      if (!e || isLoading < 0 || !e.target) {
        isLoading = 0;
      }
    };

    var isNestedVisible = function (elem, elemExpand) {
      var outerRect;
      var parent = elem;
      var visible = getCSS(document.body, 'visibility') == 'hidden' || getCSS(elem, 'visibility') != 'hidden';
      eLtop -= elemExpand;
      eLbottom += elemExpand;
      eLleft -= elemExpand;
      eLright += elemExpand;

      while (visible && (parent = parent.offsetParent) && parent != document.body && parent != docElem) {
        visible = (getCSS(parent, 'opacity') || 1) > 0;

        if (visible && getCSS(parent, 'overflow') != 'visible') {
          outerRect = parent.getBoundingClientRect();
          visible = eLright > outerRect.left && eLleft < outerRect.right && eLbottom > outerRect.top - 1 && eLtop < outerRect.bottom + 1;
        }
      }

      return visible;
    };

    var checkElements = function () {
      var eLlen, i, rect, autoLoadElem, loadedSomething, elemExpand, elemNegativeExpand, elemExpandVal, beforeExpandVal;
      var lazyloadElems = lazysizes.elements;

      if ((loadMode = lazySizesConfig.loadMode) && isLoading < 8 && (eLlen = lazyloadElems.length)) {
        i = 0;
        lowRuns++;

        if (preloadExpand == null) {
          if (!('expand' in lazySizesConfig)) {
            lazySizesConfig.expand = docElem.clientHeight > 500 && docElem.clientWidth > 500 ? 500 : 370;
          }

          defaultExpand = lazySizesConfig.expand;
          preloadExpand = defaultExpand * lazySizesConfig.expFactor;
        }

        if (currentExpand < preloadExpand && isLoading < 1 && lowRuns > 2 && loadMode > 2 && !document.hidden) {
          currentExpand = preloadExpand;
          lowRuns = 0;
        } else if (loadMode > 1 && lowRuns > 1 && isLoading < 6) {
          currentExpand = defaultExpand;
        } else {
          currentExpand = shrinkExpand;
        }

        for (; i < eLlen; i++) {
          if (!lazyloadElems[i] || lazyloadElems[i]._lazyRace) {
            continue;
          }

          if (!supportScroll) {
            unveilElement(lazyloadElems[i]);
            continue;
          }

          if (!(elemExpandVal = lazyloadElems[i][_getAttribute]('data-expand')) || !(elemExpand = elemExpandVal * 1)) {
            elemExpand = currentExpand;
          }

          if (beforeExpandVal !== elemExpand) {
            eLvW = innerWidth + elemExpand * hFac;
            elvH = innerHeight + elemExpand;
            elemNegativeExpand = elemExpand * -1;
            beforeExpandVal = elemExpand;
          }

          rect = lazyloadElems[i].getBoundingClientRect();

          if ((eLbottom = rect.bottom) >= elemNegativeExpand && (eLtop = rect.top) <= elvH && (eLright = rect.right) >= elemNegativeExpand * hFac && (eLleft = rect.left) <= eLvW && (eLbottom || eLright || eLleft || eLtop) && (lazySizesConfig.loadHidden || getCSS(lazyloadElems[i], 'visibility') != 'hidden') && (isCompleted && isLoading < 3 && !elemExpandVal && (loadMode < 3 || lowRuns < 4) || isNestedVisible(lazyloadElems[i], elemExpand))) {
            unveilElement(lazyloadElems[i]);
            loadedSomething = true;

            if (isLoading > 9) {
              break;
            }
          } else if (!loadedSomething && isCompleted && !autoLoadElem && isLoading < 4 && lowRuns < 4 && loadMode > 2 && (preloadElems[0] || lazySizesConfig.preloadAfterLoad) && (preloadElems[0] || !elemExpandVal && (eLbottom || eLright || eLleft || eLtop || lazyloadElems[i][_getAttribute](lazySizesConfig.sizesAttr) != 'auto'))) {
            autoLoadElem = preloadElems[0] || lazyloadElems[i];
          }
        }

        if (autoLoadElem && !loadedSomething) {
          unveilElement(autoLoadElem);
        }
      }
    };

    var throttledCheckElements = throttle(checkElements);

    var switchLoadingClass = function (e) {
      addClass(e.target, lazySizesConfig.loadedClass);
      removeClass(e.target, lazySizesConfig.loadingClass);
      addRemoveLoadEvents(e.target, rafSwitchLoadingClass);
      triggerEvent(e.target, 'lazyloaded');
    };

    var rafedSwitchLoadingClass = rAFIt(switchLoadingClass);

    var rafSwitchLoadingClass = function (e) {
      rafedSwitchLoadingClass({
        target: e.target
      });
    };

    var changeIframeSrc = function (elem, src) {
      try {
        elem.contentWindow.location.replace(src);
      } catch (e) {
        elem.src = src;
      }
    };

    var handleSources = function (source) {
      var customMedia;

      var sourceSrcset = source[_getAttribute](lazySizesConfig.srcsetAttr);

      if (customMedia = lazySizesConfig.customMedia[source[_getAttribute]('data-media') || source[_getAttribute]('media')]) {
        source.setAttribute('media', customMedia);
      }

      if (sourceSrcset) {
        source.setAttribute('srcset', sourceSrcset);
      }
    };

    var lazyUnveil = rAFIt(function (elem, detail, isAuto, sizes, isImg) {
      var src, srcset, parent, isPicture, event, firesLoad;

      if (!(event = triggerEvent(elem, 'lazybeforeunveil', detail)).defaultPrevented) {
        if (sizes) {
          if (isAuto) {
            addClass(elem, lazySizesConfig.autosizesClass);
          } else {
            elem.setAttribute('sizes', sizes);
          }
        }

        srcset = elem[_getAttribute](lazySizesConfig.srcsetAttr);
        src = elem[_getAttribute](lazySizesConfig.srcAttr);

        if (isImg) {
          parent = elem.parentNode;
          isPicture = parent && regPicture.test(parent.nodeName || '');
        }

        firesLoad = detail.firesLoad || 'src' in elem && (srcset || src || isPicture);
        event = {
          target: elem
        };

        if (firesLoad) {
          addRemoveLoadEvents(elem, resetPreloading, true);
          clearTimeout(resetPreloadingTimer);
          resetPreloadingTimer = setTimeout(resetPreloading, 2500);
          addClass(elem, lazySizesConfig.loadingClass);
          addRemoveLoadEvents(elem, rafSwitchLoadingClass, true);
        }

        if (isPicture) {
          forEach.call(parent.getElementsByTagName('source'), handleSources);
        }

        if (srcset) {
          elem.setAttribute('srcset', srcset);
        } else if (src && !isPicture) {
          if (regIframe.test(elem.nodeName)) {
            changeIframeSrc(elem, src);
          } else {
            elem.src = src;
          }
        }

        if (isImg && (srcset || isPicture)) {
          updatePolyfill(elem, {
            src: src
          });
        }
      }

      if (elem._lazyRace) {
        delete elem._lazyRace;
      }

      removeClass(elem, lazySizesConfig.lazyClass);
      rAF(function () {
        if (!firesLoad || elem.complete && elem.naturalWidth > 1) {
          if (firesLoad) {
            resetPreloading(event);
          } else {
            isLoading--;
          }

          switchLoadingClass(event);
        }
      }, true);
    });

    var unveilElement = function (elem) {
      var detail;
      var isImg = regImg.test(elem.nodeName); 

      var sizes = isImg && (elem[_getAttribute](lazySizesConfig.sizesAttr) || elem[_getAttribute]('sizes'));

      var isAuto = sizes == 'auto';

      if ((isAuto || !isCompleted) && isImg && (elem[_getAttribute]('src') || elem.srcset) && !elem.complete && !hasClass(elem, lazySizesConfig.errorClass)) {
        return;
      }

      detail = triggerEvent(elem, 'lazyunveilread').detail;

      if (isAuto) {
        autoSizer.updateElem(elem, true, elem.offsetWidth);
      }

      elem._lazyRace = true;
      isLoading++;
      lazyUnveil(elem, detail, isAuto, sizes, isImg);
    };

    var onload = function () {
      if (isCompleted) {
        return;
      }

      if (Date.now() - started < 999) {
        setTimeout(onload, 999);
        return;
      }

      var afterScroll = debounce(function () {
        lazySizesConfig.loadMode = 3;
        throttledCheckElements();
      });
      isCompleted = true;
      lazySizesConfig.loadMode = 3;
      throttledCheckElements();
      addEventListener('scroll', function () {
        if (lazySizesConfig.loadMode == 3) {
          lazySizesConfig.loadMode = 2;
        }

        afterScroll();
      }, true);
    };

    return {
      _: function () {
        started = Date.now();
        lazysizes.elements = document.getElementsByClassName(lazySizesConfig.lazyClass);
        preloadElems = document.getElementsByClassName(lazySizesConfig.lazyClass + ' ' + lazySizesConfig.preloadClass);
        hFac = lazySizesConfig.hFac;
        addEventListener('scroll', throttledCheckElements, true);
        addEventListener('resize', throttledCheckElements, true);

        if (window.MutationObserver) {
          new MutationObserver(throttledCheckElements).observe(docElem, {
            childList: true,
            subtree: true,
            attributes: true
          });
        } else {
          docElem[_addEventListener]('DOMNodeInserted', throttledCheckElements, true);

          docElem[_addEventListener]('DOMAttrModified', throttledCheckElements, true);

          setInterval(throttledCheckElements, 999);
        }

        addEventListener('hashchange', throttledCheckElements, true); 

        ['focus', 'mouseover', 'click', 'load', 'transitionend', 'animationend', 'webkitAnimationEnd'].forEach(function (name) {
          document[_addEventListener](name, throttledCheckElements, true);
        });

        if (/d$|^c/.test(document.readyState)) {
          onload();
        } else {
          addEventListener('load', onload);

          document[_addEventListener]('DOMContentLoaded', throttledCheckElements);

          setTimeout(onload, 20000);
        }

        if (lazysizes.elements.length) {
          checkElements();

          rAF._lsFlush();
        } else {
          throttledCheckElements();
        }
      },
      checkElems: throttledCheckElements,
      unveil: unveilElement
    };
  }();

  var autoSizer = function () {
    var autosizesElems;
    var sizeElement = rAFIt(function (elem, parent, event, width) {
      var sources, i, len;
      elem._lazysizesWidth = width;
      width += 'px';
      elem.setAttribute('sizes', width);

      if (regPicture.test(parent.nodeName || '')) {
        sources = parent.getElementsByTagName('source');

        for (i = 0, len = sources.length; i < len; i++) {
          sources[i].setAttribute('sizes', width);
        }
      }

      if (!event.detail.dataAttr) {
        updatePolyfill(elem, event.detail);
      }
    });

    var getSizeElement = function (elem, dataAttr, width) {
      var event;
      var parent = elem.parentNode;

      if (parent) {
        width = getWidth(elem, parent, width);
        event = triggerEvent(elem, 'lazybeforesizes', {
          width: width,
          dataAttr: !!dataAttr
        });

        if (!event.defaultPrevented) {
          width = event.detail.width;

          if (width && width !== elem._lazysizesWidth) {
            sizeElement(elem, parent, event, width);
          }
        }
      }
    };

    var updateElementsSizes = function () {
      var i;
      var len = autosizesElems.length;

      if (len) {
        i = 0;

        for (; i < len; i++) {
          getSizeElement(autosizesElems[i]);
        }
      }
    };

    var debouncedUpdateElementsSizes = debounce(updateElementsSizes);
    return {
      _: function () {
        autosizesElems = document.getElementsByClassName(lazySizesConfig.autosizesClass);
        addEventListener('resize', debouncedUpdateElementsSizes);
      },
      checkElems: debouncedUpdateElementsSizes,
      updateElem: getSizeElement
    };
  }();

  var init = function () {
    if (!init.i) {
      init.i = true;

      autoSizer._();

      loader._();
    }
  };

  (function () {
    var prop;
    var lazySizesDefaults = {
      lazyClass: 'lazyload',
      loadedClass: 'lazyloaded',
      loadingClass: 'lazyloading',
      preloadClass: 'lazypreload',
      errorClass: 'lazyerror',
      autosizesClass: 'lazyautosizes',
      srcAttr: 'data-src',
      srcsetAttr: 'data-srcset',
      sizesAttr: 'data-sizes',
      minSize: 40,
      customMedia: {},
      init: true,
      expFactor: 1.5,
      hFac: 0.8,
      loadMode: 2,
      loadHidden: true
    };
    lazySizesConfig = window.lazySizesConfig || window.lazysizesConfig || {};

    for (prop in lazySizesDefaults) {
      if (!(prop in lazySizesConfig)) {
        lazySizesConfig[prop] = lazySizesDefaults[prop];
      }
    }

    window.lazySizesConfig = lazySizesConfig;
    setTimeout(function () {
      if (lazySizesConfig.init) {
        init();
      }
    });
  })();

  lazysizes = {
    cfg: lazySizesConfig,
    autoSizer: autoSizer,
    loader: loader,
    init: init,
    uP: updatePolyfill,
    aC: addClass,
    rC: removeClass,
    hC: hasClass,
    fire: triggerEvent,
    gW: getWidth,
    rAF: rAF
  };
  return lazysizes;
});
(function (window, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['jquery'], function ($) {
      return factory(window, $);
    });
  } else if (typeof module === 'object' && typeof module.exports === 'object') {
    module.exports = factory(window, require('jquery'));
  } else {
    window.lity = factory(window, window.jQuery || window.Zepto);
  }
})(typeof window !== "undefined" ? window : this, function (window, $) {
  'use strict';

  var document = window.document;

  var _win = $(window);

  var _deferred = $.Deferred;

  var _html = $('html');

  var _instances = [];
  var _attrAriaHidden = 'aria-hidden';

  var _dataAriaHidden = 'lity-' + _attrAriaHidden;

  var _focusableElementsSelector = 'a[href],area[href],input:not([disabled]),select:not([disabled]),textarea:not([disabled]),button:not([disabled]),iframe,object,embed,[contenteditable],[tabindex]:not([tabindex^="-"])';
  var _defaultOptions = {
    esc: true,
    handler: null,
    handlers: {
      image: imageHandler,
      inline: inlineHandler,
      youtube: youtubeHandler,
      vimeo: vimeoHandler,
      googlemaps: googlemapsHandler,
      facebookvideo: facebookvideoHandler,
      iframe: iframeHandler
    },
    template: '<div class="lity" role="dialog" aria-label="Dialog Window (Press escape to close)" tabindex="-1"><div class="lity-wrap" data-lity-close role="document"><div class="lity-loader" aria-hidden="true">Loading...</div><div class="lity-container"><div class="lity-content"></div><button class="lity-close" type="button" aria-label="Close (Press escape to close)" data-lity-close>&times;</button></div></div></div>'
  };
  var _imageRegexp = /(^data:image\/)|(\.(png|jpe?g|gif|svg|webp|bmp|ico|tiff?)(\?\S*)?$)/i;
  var _youtubeRegex = /(youtube(-nocookie)?\.com|youtu\.be)\/(watch\?v=|v\/|u\/|embed\/?)?([\w-]{11})(.*)?/i;
  var _vimeoRegex = /(vimeo(pro)?.com)\/(?:[^\d]+)?(\d+)\??(.*)?$/;
  var _googlemapsRegex = /((maps|www)\.)?google\.([^\/\?]+)\/?((maps\/?)?\?)(.*)/i;
  var _facebookvideoRegex = /(facebook\.com)\/([a-z0-9_-]*)\/videos\/([0-9]*)(.*)?$/i;

  var _transitionEndEvent = function () {
    var el = document.createElement('div');
    var transEndEventNames = {
      WebkitTransition: 'webkitTransitionEnd',
      MozTransition: 'transitionend',
      OTransition: 'oTransitionEnd otransitionend',
      transition: 'transitionend'
    };

    for (var name in transEndEventNames) {
      if (el.style[name] !== undefined) {
        return transEndEventNames[name];
      }
    }

    return false;
  }();

  function transitionEnd(element) {
    var deferred = _deferred();

    if (!_transitionEndEvent || !element.length) {
      deferred.resolve();
    } else {
      element.one(_transitionEndEvent, deferred.resolve);
      setTimeout(deferred.resolve, 500);
    }

    return deferred.promise();
  }

  function settings(currSettings, key, value) {
    if (arguments.length === 1) {
      return $.extend({}, currSettings);
    }

    if (typeof key === 'string') {
      if (typeof value === 'undefined') {
        return typeof currSettings[key] === 'undefined' ? null : currSettings[key];
      }

      currSettings[key] = value;
    } else {
      $.extend(currSettings, key);
    }

    return this;
  }

  function parseQueryParams(params) {
    var pairs = decodeURI(params.split('#')[0]).split('&');
    var obj = {},
        p;

    for (var i = 0, n = pairs.length; i < n; i++) {
      if (!pairs[i]) {
        continue;
      }

      p = pairs[i].split('=');
      obj[p[0]] = p[1];
    }

    return obj;
  }

  function appendQueryParams(url, params) {
    return url + (url.indexOf('?') > -1 ? '&' : '?') + $.param(params);
  }

  function transferHash(originalUrl, newUrl) {
    var pos = originalUrl.indexOf('#');

    if (-1 === pos) {
      return newUrl;
    }

    if (pos > 0) {
      originalUrl = originalUrl.substr(pos);
    }

    return newUrl + originalUrl;
  }

  function error(msg) {
    return $('<span class="lity-error"/>').append(msg);
  }

  function imageHandler(target, instance) {
    var desc = instance.opener() && instance.opener().data('lity-desc') || 'Image with no description';
    var img = $('<img src="' + target + '" alt="' + desc + '"/>');

    var deferred = _deferred();

    var failed = function () {
      deferred.reject(error('Failed loading image'));
    };

    img.on('load', function () {
      if (this.naturalWidth === 0) {
        return failed();
      }

      deferred.resolve(img);
    }).on('error', failed);
    return deferred.promise();
  }

  imageHandler.test = function (target) {
    return _imageRegexp.test(target);
  };

  function inlineHandler(target, instance) {
    var el, placeholder, hasHideClass;

    try {
      el = $(target);
    } catch (e) {
      return false;
    }

    if (!el.length) {
      return false;
    }

    placeholder = $('<i style="display:none !important"/>');
    hasHideClass = el.hasClass('lity-hide');
    instance.element().one('lity:remove', function () {
      placeholder.before(el).remove();

      if (hasHideClass && !el.closest('.lity-content').length) {
        el.addClass('lity-hide');
      }
    });
    return el.removeClass('lity-hide').after(placeholder);
  }

  function youtubeHandler(target) {
    var matches = _youtubeRegex.exec(target);

    if (!matches) {
      return false;
    }

    return iframeHandler(transferHash(target, appendQueryParams('https://www.youtube' + (matches[2] || '') + '.com/embed/' + matches[4], $.extend({
      autoplay: 1
    }, parseQueryParams(matches[5] || '')))));
  }

  function vimeoHandler(target) {
    var matches = _vimeoRegex.exec(target);

    if (!matches) {
      return false;
    }

    return iframeHandler(transferHash(target, appendQueryParams('https://player.vimeo.com/video/' + matches[3], $.extend({
      autoplay: 1
    }, parseQueryParams(matches[4] || '')))));
  }

  function facebookvideoHandler(target) {
    var matches = _facebookvideoRegex.exec(target);

    if (!matches) {
      return false;
    }

    if (0 !== target.indexOf('http')) {
      target = 'https:' + target;
    }

    return iframeHandler(transferHash(target, appendQueryParams('https://www.facebook.com/plugins/video.php?href=' + target, $.extend({
      autoplay: 1
    }, parseQueryParams(matches[4] || '')))));
  }

  function googlemapsHandler(target) {
    var matches = _googlemapsRegex.exec(target);

    if (!matches) {
      return false;
    }

    return iframeHandler(transferHash(target, appendQueryParams('https://www.google.' + matches[3] + '/maps?' + matches[6], {
      output: matches[6].indexOf('layer=c') > 0 ? 'svembed' : 'embed'
    })));
  }

  function iframeHandler(target) {
    return '<div class="lity-iframe-container"><iframe frameborder="0" allowfullscreen src="' + target + '"/></div>';
  }

  function winHeight() {
    return document.documentElement.clientHeight ? document.documentElement.clientHeight : Math.round(_win.height());
  }

  function keydown(e) {
    var current = currentInstance();

    if (!current) {
      return;
    } 


    if (e.keyCode === 27 && !!current.options('esc')) {
      current.close();
    } 


    if (e.keyCode === 9) {
      handleTabKey(e, current);
    }
  }

  function handleTabKey(e, instance) {
    var focusableElements = instance.element().find(_focusableElementsSelector);
    var focusedIndex = focusableElements.index(document.activeElement);

    if (e.shiftKey && focusedIndex <= 0) {
      focusableElements.get(focusableElements.length - 1).focus();
      e.preventDefault();
    } else if (!e.shiftKey && focusedIndex === focusableElements.length - 1) {
      focusableElements.get(0).focus();
      e.preventDefault();
    }
  }

  function resize() {
    $.each(_instances, function (i, instance) {
      instance.resize();
    });
  }

  function registerInstance(instanceToRegister) {
    if (1 === _instances.unshift(instanceToRegister)) {
      _html.addClass('lity-active');

      _win.on({
        resize: resize,
        keydown: keydown
      });
    }

    $('body > *').not(instanceToRegister.element()).addClass('lity-hidden').each(function () {
      var el = $(this);

      if (undefined !== el.data(_dataAriaHidden)) {
        return;
      }

      el.data(_dataAriaHidden, el.attr(_attrAriaHidden) || null);
    }).attr(_attrAriaHidden, 'true');
  }

  function removeInstance(instanceToRemove) {
    var show;
    instanceToRemove.element().attr(_attrAriaHidden, 'true');

    if (1 === _instances.length) {
      _html.removeClass('lity-active');

      _win.off({
        resize: resize,
        keydown: keydown
      });
    }

    _instances = $.grep(_instances, function (instance) {
      return instanceToRemove !== instance;
    });

    if (!!_instances.length) {
      show = _instances[0].element();
    } else {
      show = $('.lity-hidden');
    }

    show.removeClass('lity-hidden').each(function () {
      var el = $(this),
          oldAttr = el.data(_dataAriaHidden);

      if (!oldAttr) {
        el.removeAttr(_attrAriaHidden);
      } else {
        el.attr(_attrAriaHidden, oldAttr);
      }

      el.removeData(_dataAriaHidden);
    });
  }

  function currentInstance() {
    if (0 === _instances.length) {
      return null;
    }

    return _instances[0];
  }

  function factory(target, instance, handlers, preferredHandler) {
    var handler = 'inline',
        content;
    var currentHandlers = $.extend({}, handlers);

    if (preferredHandler && currentHandlers[preferredHandler]) {
      content = currentHandlers[preferredHandler](target, instance);
      handler = preferredHandler;
    } else {
      $.each(['inline', 'iframe'], function (i, name) {
        delete currentHandlers[name];
        currentHandlers[name] = handlers[name];
      });
      $.each(currentHandlers, function (name, currentHandler) {
        if (!currentHandler) {
          return true;
        }

        if (currentHandler.test && !currentHandler.test(target, instance)) {
          return true;
        }

        content = currentHandler(target, instance);

        if (false !== content) {
          handler = name;
          return false;
        }
      });
    }

    return {
      handler: handler,
      content: content || ''
    };
  }

  function Lity(target, options, opener, activeElement) {
    var self = this;
    var result;
    var isReady = false;
    var isClosed = false;
    var element;
    var content;
    options = $.extend({}, _defaultOptions, options);
    element = $(options.template); 

    self.element = function () {
      return element;
    };

    self.opener = function () {
      return opener;
    };

    self.options = $.proxy(settings, self, options);
    self.handlers = $.proxy(settings, self, options.handlers);

    self.resize = function () {
      if (!isReady || isClosed) {
        return;
      }

      content.css('max-height', winHeight() + 'px').trigger('lity:resize', [self]);
    };

    self.close = function () {
      if (!isReady || isClosed) {
        return;
      }

      isClosed = true;
      removeInstance(self);

      var deferred = _deferred(); 


      if (activeElement && (document.activeElement === element[0] || $.contains(element[0], document.activeElement))) {
        try {
          activeElement.focus();
        } catch (e) {
        }
      }

      content.trigger('lity:close', [self]);
      element.removeClass('lity-opened').addClass('lity-closed');
      transitionEnd(content.add(element)).always(function () {
        content.trigger('lity:remove', [self]);
        element.remove();
        element = undefined;
        deferred.resolve();
      });
      return deferred.promise();
    }; 


    result = factory(target, self, options.handlers, options.handler);
    element.attr(_attrAriaHidden, 'false').addClass('lity-loading lity-opened lity-' + result.handler).appendTo('body').focus().on('click', '[data-lity-close]', function (e) {
      if ($(e.target).is('[data-lity-close]')) {
        self.close();
      }
    }).trigger('lity:open', [self]);
    registerInstance(self);
    $.when(result.content).always(ready);

    function ready(result) {
      content = $(result).css('max-height', winHeight() + 'px');
      element.find('.lity-loader').each(function () {
        var loader = $(this);
        transitionEnd(loader).always(function () {
          loader.remove();
        });
      });
      element.removeClass('lity-loading').find('.lity-content').empty().append(content);
      isReady = true;
      content.trigger('lity:ready', [self]);
    }
  }

  function lity(target, options, opener) {
    if (!target.preventDefault) {
      opener = $(opener);
    } else {
      target.preventDefault();
      opener = $(this);
      target = opener.data('lity-target') || opener.attr('href') || opener.attr('src');
    }

    var instance = new Lity(target, $.extend({}, opener.data('lity-options') || opener.data('lity'), options), opener, document.activeElement);

    if (!target.preventDefault) {
      return instance;
    }
  }

  lity.version = '2.3.1';
  lity.options = $.proxy(settings, lity, _defaultOptions);
  lity.handlers = $.proxy(settings, lity, _defaultOptions.handlers);
  lity.current = currentInstance;
  $(document).on('click.lity', '[data-lity]', lity);
  return lity;
});

;

(function ($, window, document, undefined) {
  function Owl(element, options) {
    this.settings = null;

    this.options = $.extend({}, Owl.Defaults, options);

    this.$element = $(element);

    this._handlers = {};

    this._plugins = {};

    this._supress = {};

    this._current = null;

    this._speed = null;

    this._coordinates = [];

    this._breakpoint = null;

    this._width = null;

    this._items = [];

    this._clones = [];

    this._mergers = [];

    this._widths = [];

    this._invalidated = {};

    this._pipe = [];

    this._drag = {
      time: null,
      target: null,
      pointer: null,
      stage: {
        start: null,
        current: null
      },
      direction: null
    };

    this._states = {
      current: {},
      tags: {
        'initializing': ['busy'],
        'animating': ['busy'],
        'dragging': ['interacting']
      }
    };
    $.each(['onResize', 'onThrottledResize'], $.proxy(function (i, handler) {
      this._handlers[handler] = $.proxy(this[handler], this);
    }, this));
    $.each(Owl.Plugins, $.proxy(function (key, plugin) {
      this._plugins[key.charAt(0).toLowerCase() + key.slice(1)] = new plugin(this);
    }, this));
    $.each(Owl.Workers, $.proxy(function (priority, worker) {
      this._pipe.push({
        'filter': worker.filter,
        'run': $.proxy(worker.run, this)
      });
    }, this));
    this.setup();
    this.initialize();
  }


  Owl.Defaults = {
    items: 3,
    loop: false,
    center: false,
    rewind: false,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    freeDrag: false,
    margin: 0,
    stagePadding: 0,
    merge: false,
    mergeFit: true,
    autoWidth: false,
    startPosition: 0,
    rtl: false,
    smartSpeed: 250,
    fluidSpeed: false,
    dragEndSpeed: false,
    responsive: {},
    responsiveRefreshRate: 200,
    responsiveBaseElement: window,
    fallbackEasing: 'swing',
    info: false,
    nestedItemSelector: false,
    itemElement: 'div',
    stageElement: 'div',
    refreshClass: 'owl-refresh',
    loadedClass: 'owl-loaded',
    loadingClass: 'owl-loading',
    rtlClass: 'owl-rtl',
    responsiveClass: 'owl-responsive',
    dragClass: 'owl-drag',
    itemClass: 'owl-item',
    stageClass: 'owl-stage',
    stageOuterClass: 'owl-stage-outer',
    grabClass: 'owl-grab'
  };

  Owl.Width = {
    Default: 'default',
    Inner: 'inner',
    Outer: 'outer'
  };

  Owl.Type = {
    Event: 'event',
    State: 'state'
  };

  Owl.Plugins = {};

  Owl.Workers = [{
    filter: ['width', 'settings'],
    run: function () {
      this._width = this.$element.width();
    }
  }, {
    filter: ['width', 'items', 'settings'],
    run: function (cache) {
      cache.current = this._items && this._items[this.relative(this._current)];
    }
  }, {
    filter: ['items', 'settings'],
    run: function () {
      this.$stage.children('.cloned').remove();
    }
  }, {
    filter: ['width', 'items', 'settings'],
    run: function (cache) {
      var margin = this.settings.margin || '',
          grid = !this.settings.autoWidth,
          rtl = this.settings.rtl,
          css = {
        'width': 'auto',
        'margin-left': rtl ? margin : '',
        'margin-right': rtl ? '' : margin
      };
      !grid && this.$stage.children().css(css);
      cache.css = css;
    }
  }, {
    filter: ['width', 'items', 'settings'],
    run: function (cache) {
      var width = (this.width() / this.settings.items).toFixed(3) - this.settings.margin,
          merge = null,
          iterator = this._items.length,
          grid = !this.settings.autoWidth,
          widths = [];
      cache.items = {
        merge: false,
        width: width
      };

      while (iterator--) {
        merge = this._mergers[iterator];
        merge = this.settings.mergeFit && Math.min(merge, this.settings.items) || merge;
        cache.items.merge = merge > 1 || cache.items.merge;
        widths[iterator] = !grid ? this._items[iterator].width() : width * merge;
      }

      this._widths = widths;
    }
  }, {
    filter: ['items', 'settings'],
    run: function () {
      var clones = [],
          items = this._items,
          settings = this.settings,
      view = Math.max(settings.items * 2, 4),
          size = Math.ceil(items.length / 2) * 2,
          repeat = settings.loop && items.length ? settings.rewind ? view : Math.max(view, size) : 0,
          append = '',
          prepend = '';
      repeat /= 2;

      while (repeat > 0) {
        clones.push(this.normalize(clones.length / 2, true));
        append = append + items[clones[clones.length - 1]][0].outerHTML;
        clones.push(this.normalize(items.length - 1 - (clones.length - 1) / 2, true));
        prepend = items[clones[clones.length - 1]][0].outerHTML + prepend;
        repeat -= 1;
      }

      this._clones = clones;
      $(append).addClass('cloned').appendTo(this.$stage);
      $(prepend).addClass('cloned').prependTo(this.$stage);
    }
  }, {
    filter: ['width', 'items', 'settings'],
    run: function () {
      var rtl = this.settings.rtl ? 1 : -1,
          size = this._clones.length + this._items.length,
          iterator = -1,
          previous = 0,
          current = 0,
          coordinates = [];

      while (++iterator < size) {
        previous = coordinates[iterator - 1] || 0;
        current = this._widths[this.relative(iterator)] + this.settings.margin;
        coordinates.push(previous + current * rtl);
      }

      this._coordinates = coordinates;
    }
  }, {
    filter: ['width', 'items', 'settings'],
    run: function () {
      var padding = this.settings.stagePadding,
          coordinates = this._coordinates,
          css = {
        'width': Math.ceil(Math.abs(coordinates[coordinates.length - 1])) + padding * 2,
        'padding-left': padding || '',
        'padding-right': padding || ''
      };
      this.$stage.css(css);
    }
  }, {
    filter: ['width', 'items', 'settings'],
    run: function (cache) {
      var iterator = this._coordinates.length,
          grid = !this.settings.autoWidth,
          items = this.$stage.children();

      if (grid && cache.items.merge) {
        while (iterator--) {
          cache.css.width = this._widths[this.relative(iterator)];
          items.eq(iterator).css(cache.css);
        }
      } else if (grid) {
        cache.css.width = cache.items.width;
        items.css(cache.css);
      }
    }
  }, {
    filter: ['items'],
    run: function () {
      this._coordinates.length < 1 && this.$stage.removeAttr('style');
    }
  }, {
    filter: ['width', 'items', 'settings'],
    run: function (cache) {
      cache.current = cache.current ? this.$stage.children().index(cache.current) : 0;
      cache.current = Math.max(this.minimum(), Math.min(this.maximum(), cache.current));
      this.reset(cache.current);
    }
  }, {
    filter: ['position'],
    run: function () {
      this.animate(this.coordinates(this._current));
    }
  }, {
    filter: ['width', 'position', 'items', 'settings'],
    run: function () {
      var rtl = this.settings.rtl ? 1 : -1,
          padding = this.settings.stagePadding * 2,
          begin = this.coordinates(this.current()) + padding,
          end = begin + this.width() * rtl,
          inner,
          outer,
          matches = [],
          i,
          n;

      for (i = 0, n = this._coordinates.length; i < n; i++) {
        inner = this._coordinates[i - 1] || 0;
        outer = Math.abs(this._coordinates[i]) + padding * rtl;

        if (this.op(inner, '<=', begin) && this.op(inner, '>', end) || this.op(outer, '<', begin) && this.op(outer, '>', end)) {
          matches.push(i);
        }
      }

      this.$stage.children('.active').removeClass('active');
      this.$stage.children(':eq(' + matches.join('), :eq(') + ')').addClass('active');
      this.$stage.children('.center').removeClass('center');

      if (this.settings.center) {
        this.$stage.children().eq(this.current()).addClass('center');
      }
    }
  }];

  Owl.prototype.initializeStage = function () {
    this.$stage = this.$element.find('.' + this.settings.stageClass); 

    if (this.$stage.length) {
      return;
    }

    this.$element.addClass(this.options.loadingClass); 

    this.$stage = $('<' + this.settings.stageElement + ' class="' + this.settings.stageClass + '"/>').wrap('<div class="' + this.settings.stageOuterClass + '"/>'); 

    this.$element.append(this.$stage.parent());
  };


  Owl.prototype.initializeItems = function () {
    var $items = this.$element.find('.owl-item'); 

    if ($items.length) {
      this._items = $items.get().map(function (item) {
        return $(item);
      });
      this._mergers = this._items.map(function () {
        return 1;
      });
      this.refresh();
      return;
    } 


    this.replace(this.$element.children().not(this.$stage.parent())); 

    if (this.isVisible()) {
      this.refresh();
    } else {
      this.invalidate('width');
    }

    this.$element.removeClass(this.options.loadingClass).addClass(this.options.loadedClass);
  };


  Owl.prototype.initialize = function () {
    this.enter('initializing');
    this.trigger('initialize');
    this.$element.toggleClass(this.settings.rtlClass, this.settings.rtl);

    if (this.settings.autoWidth && !this.is('pre-loading')) {
      var imgs, nestedSelector, width;
      imgs = this.$element.find('img');
      nestedSelector = this.settings.nestedItemSelector ? '.' + this.settings.nestedItemSelector : undefined;
      width = this.$element.children(nestedSelector).width();

      if (imgs.length && width <= 0) {
        this.preloadAutoWidthImages(imgs);
      }
    }

    this.initializeStage();
    this.initializeItems(); 

    this.registerEventHandlers();
    this.leave('initializing');
    this.trigger('initialized');
  };


  Owl.prototype.isVisible = function () {
    return this.settings.checkVisibility ? this.$element.is(':visible') : true;
  };


  Owl.prototype.setup = function () {
    var viewport = this.viewport(),
        overwrites = this.options.responsive,
        match = -1,
        settings = null;

    if (!overwrites) {
      settings = $.extend({}, this.options);
    } else {
      $.each(overwrites, function (breakpoint) {
        if (breakpoint <= viewport && breakpoint > match) {
          match = Number(breakpoint);
        }
      });
      settings = $.extend({}, this.options, overwrites[match]);

      if (typeof settings.stagePadding === 'function') {
        settings.stagePadding = settings.stagePadding();
      }

      delete settings.responsive; 

      if (settings.responsiveClass) {
        this.$element.attr('class', this.$element.attr('class').replace(new RegExp('(' + this.options.responsiveClass + '-)\\S+\\s', 'g'), '$1' + match));
      }
    }

    this.trigger('change', {
      property: {
        name: 'settings',
        value: settings
      }
    });
    this._breakpoint = match;
    this.settings = settings;
    this.invalidate('settings');
    this.trigger('changed', {
      property: {
        name: 'settings',
        value: this.settings
      }
    });
  };


  Owl.prototype.optionsLogic = function () {
    if (this.settings.autoWidth) {
      this.settings.stagePadding = false;
      this.settings.merge = false;
    }
  };


  Owl.prototype.prepare = function (item) {
    var event = this.trigger('prepare', {
      content: item
    });

    if (!event.data) {
      event.data = $('<' + this.settings.itemElement + '/>').addClass(this.options.itemClass).append(item);
    }

    this.trigger('prepared', {
      content: event.data
    });
    return event.data;
  };


  Owl.prototype.update = function () {
    var i = 0,
        n = this._pipe.length,
        filter = $.proxy(function (p) {
      return this[p];
    }, this._invalidated),
        cache = {};

    while (i < n) {
      if (this._invalidated.all || $.grep(this._pipe[i].filter, filter).length > 0) {
        this._pipe[i].run(cache);
      }

      i++;
    }

    this._invalidated = {};
    !this.is('valid') && this.enter('valid');
  };


  Owl.prototype.width = function (dimension) {
    dimension = dimension || Owl.Width.Default;

    switch (dimension) {
      case Owl.Width.Inner:
      case Owl.Width.Outer:
        return this._width;

      default:
        return this._width - this.settings.stagePadding * 2 + this.settings.margin;
    }
  };


  Owl.prototype.refresh = function () {
    this.enter('refreshing');
    this.trigger('refresh');
    this.setup();
    this.optionsLogic();
    this.$element.addClass(this.options.refreshClass);
    this.update();
    this.$element.removeClass(this.options.refreshClass);
    this.leave('refreshing');
    this.trigger('refreshed');
  };


  Owl.prototype.onThrottledResize = function () {
    window.clearTimeout(this.resizeTimer);
    this.resizeTimer = window.setTimeout(this._handlers.onResize, this.settings.responsiveRefreshRate);
  };


  Owl.prototype.onResize = function () {
    if (!this._items.length) {
      return false;
    }

    if (this._width === this.$element.width()) {
      return false;
    }

    if (!this.isVisible()) {
      return false;
    }

    this.enter('resizing');

    if (this.trigger('resize').isDefaultPrevented()) {
      this.leave('resizing');
      return false;
    }

    this.invalidate('width');
    this.refresh();
    this.leave('resizing');
    this.trigger('resized');
  };


  Owl.prototype.registerEventHandlers = function () {
    if ($.support.transition) {
      this.$stage.on($.support.transition.end + '.owl.core', $.proxy(this.onTransitionEnd, this));
    }

    if (this.settings.responsive !== false) {
      this.on(window, 'resize', this._handlers.onThrottledResize);
    }

    if (this.settings.mouseDrag) {
      this.$element.addClass(this.options.dragClass);
      this.$stage.on('mousedown.owl.core', $.proxy(this.onDragStart, this));
      this.$stage.on('dragstart.owl.core selectstart.owl.core', function () {
        return false;
      });
    }

    if (this.settings.touchDrag) {
      this.$stage.on('touchstart.owl.core', $.proxy(this.onDragStart, this));
      this.$stage.on('touchcancel.owl.core', $.proxy(this.onDragEnd, this));
    }
  };


  Owl.prototype.onDragStart = function (event) {
    var stage = null;

    if (event.which === 3) {
      return;
    }

    if ($.support.transform) {
      stage = this.$stage.css('transform').replace(/.*\(|\)| /g, '').split(',');
      stage = {
        x: stage[stage.length === 16 ? 12 : 4],
        y: stage[stage.length === 16 ? 13 : 5]
      };
    } else {
      stage = this.$stage.position();
      stage = {
        x: this.settings.rtl ? stage.left + this.$stage.width() - this.width() + this.settings.margin : stage.left,
        y: stage.top
      };
    }

    if (this.is('animating')) {
      $.support.transform ? this.animate(stage.x) : this.$stage.stop();
      this.invalidate('position');
    }

    this.$element.toggleClass(this.options.grabClass, event.type === 'mousedown');
    this.speed(0);
    this._drag.time = new Date().getTime();
    this._drag.target = $(event.target);
    this._drag.stage.start = stage;
    this._drag.stage.current = stage;
    this._drag.pointer = this.pointer(event);
    $(document).on('mouseup.owl.core touchend.owl.core', $.proxy(this.onDragEnd, this));
    $(document).one('mousemove.owl.core touchmove.owl.core', $.proxy(function (event) {
      var delta = this.difference(this._drag.pointer, this.pointer(event));
      $(document).on('mousemove.owl.core touchmove.owl.core', $.proxy(this.onDragMove, this));

      if (Math.abs(delta.x) < Math.abs(delta.y) && this.is('valid')) {
        return;
      }

      event.preventDefault();
      this.enter('dragging');
      this.trigger('drag');
    }, this));
  };


  Owl.prototype.onDragMove = function (event) {
    var minimum = null,
        maximum = null,
        pull = null,
        delta = this.difference(this._drag.pointer, this.pointer(event)),
        stage = this.difference(this._drag.stage.start, delta);

    if (!this.is('dragging')) {
      return;
    }

    event.preventDefault();

    if (this.settings.loop) {
      minimum = this.coordinates(this.minimum());
      maximum = this.coordinates(this.maximum() + 1) - minimum;
      stage.x = ((stage.x - minimum) % maximum + maximum) % maximum + minimum;
    } else {
      minimum = this.settings.rtl ? this.coordinates(this.maximum()) : this.coordinates(this.minimum());
      maximum = this.settings.rtl ? this.coordinates(this.minimum()) : this.coordinates(this.maximum());
      pull = this.settings.pullDrag ? -1 * delta.x / 5 : 0;
      stage.x = Math.max(Math.min(stage.x, minimum + pull), maximum + pull);
    }

    this._drag.stage.current = stage;
    this.animate(stage.x);
  };


  Owl.prototype.onDragEnd = function (event) {
    var delta = this.difference(this._drag.pointer, this.pointer(event)),
        stage = this._drag.stage.current,
        direction = delta.x > 0 ^ this.settings.rtl ? 'left' : 'right';
    $(document).off('.owl.core');
    this.$element.removeClass(this.options.grabClass);

    if (delta.x !== 0 && this.is('dragging') || !this.is('valid')) {
      this.speed(this.settings.dragEndSpeed || this.settings.smartSpeed);
      this.current(this.closest(stage.x, delta.x !== 0 ? direction : this._drag.direction));
      this.invalidate('position');
      this.update();
      this._drag.direction = direction;

      if (Math.abs(delta.x) > 3 || new Date().getTime() - this._drag.time > 300) {
        this._drag.target.one('click.owl.core', function () {
          return false;
        });
      }
    }

    if (!this.is('dragging')) {
      return;
    }

    this.leave('dragging');
    this.trigger('dragged');
  };


  Owl.prototype.closest = function (coordinate, direction) {
    var position = -1,
        pull = 30,
        width = this.width(),
        coordinates = this.coordinates();

    if (!this.settings.freeDrag) {
      $.each(coordinates, $.proxy(function (index, value) {
        if (direction === 'left' && coordinate > value - pull && coordinate < value + pull) {
          position = index; 
        } else if (direction === 'right' && coordinate > value - width - pull && coordinate < value - width + pull) {
          position = index + 1;
        } else if (this.op(coordinate, '<', value) && this.op(coordinate, '>', coordinates[index + 1] !== undefined ? coordinates[index + 1] : value - width)) {
          position = direction === 'left' ? index + 1 : index;
        }

        return position === -1;
      }, this));
    }

    if (!this.settings.loop) {
      if (this.op(coordinate, '>', coordinates[this.minimum()])) {
        position = coordinate = this.minimum();
      } else if (this.op(coordinate, '<', coordinates[this.maximum()])) {
        position = coordinate = this.maximum();
      }
    }

    return position;
  };


  Owl.prototype.animate = function (coordinate) {
    var animate = this.speed() > 0;
    this.is('animating') && this.onTransitionEnd();

    if (animate) {
      this.enter('animating');
      this.trigger('translate');
    }

    if ($.support.transform3d && $.support.transition) {
      this.$stage.css({
        transform: 'translate3d(' + coordinate + 'px,0px,0px)',
        transition: this.speed() / 1000 + 's'
      });
    } else if (animate) {
      this.$stage.animate({
        left: coordinate + 'px'
      }, this.speed(), this.settings.fallbackEasing, $.proxy(this.onTransitionEnd, this));
    } else {
      this.$stage.css({
        left: coordinate + 'px'
      });
    }
  };


  Owl.prototype.is = function (state) {
    return this._states.current[state] && this._states.current[state] > 0;
  };


  Owl.prototype.current = function (position) {
    if (position === undefined) {
      return this._current;
    }

    if (this._items.length === 0) {
      return undefined;
    }

    position = this.normalize(position);

    if (this._current !== position) {
      var event = this.trigger('change', {
        property: {
          name: 'position',
          value: position
        }
      });

      if (event.data !== undefined) {
        position = this.normalize(event.data);
      }

      this._current = position;
      this.invalidate('position');
      this.trigger('changed', {
        property: {
          name: 'position',
          value: this._current
        }
      });
    }

    return this._current;
  };


  Owl.prototype.invalidate = function (part) {
    if ($.type(part) === 'string') {
      this._invalidated[part] = true;
      this.is('valid') && this.leave('valid');
    }

    return $.map(this._invalidated, function (v, i) {
      return i;
    });
  };


  Owl.prototype.reset = function (position) {
    position = this.normalize(position);

    if (position === undefined) {
      return;
    }

    this._speed = 0;
    this._current = position;
    this.suppress(['translate', 'translated']);
    this.animate(this.coordinates(position));
    this.release(['translate', 'translated']);
  };


  Owl.prototype.normalize = function (position, relative) {
    var n = this._items.length,
        m = relative ? 0 : this._clones.length;

    if (!this.isNumeric(position) || n < 1) {
      position = undefined;
    } else if (position < 0 || position >= n + m) {
      position = ((position - m / 2) % n + n) % n + m / 2;
    }

    return position;
  };


  Owl.prototype.relative = function (position) {
    position -= this._clones.length / 2;
    return this.normalize(position, true);
  };


  Owl.prototype.maximum = function (relative) {
    var settings = this.settings,
        maximum = this._coordinates.length,
        iterator,
        reciprocalItemsWidth,
        elementWidth;

    if (settings.loop) {
      maximum = this._clones.length / 2 + this._items.length - 1;
    } else if (settings.autoWidth || settings.merge) {
      iterator = this._items.length;

      if (iterator) {
        reciprocalItemsWidth = this._items[--iterator].width();
        elementWidth = this.$element.width();

        while (iterator--) {
          reciprocalItemsWidth += this._items[iterator].width() + this.settings.margin;

          if (reciprocalItemsWidth > elementWidth) {
            break;
          }
        }
      }

      maximum = iterator + 1;
    } else if (settings.center) {
      maximum = this._items.length - 1;
    } else {
      maximum = this._items.length - settings.items;
    }

    if (relative) {
      maximum -= this._clones.length / 2;
    }

    return Math.max(maximum, 0);
  };


  Owl.prototype.minimum = function (relative) {
    return relative ? 0 : this._clones.length / 2;
  };


  Owl.prototype.items = function (position) {
    if (position === undefined) {
      return this._items.slice();
    }

    position = this.normalize(position, true);
    return this._items[position];
  };


  Owl.prototype.mergers = function (position) {
    if (position === undefined) {
      return this._mergers.slice();
    }

    position = this.normalize(position, true);
    return this._mergers[position];
  };


  Owl.prototype.clones = function (position) {
    var odd = this._clones.length / 2,
        even = odd + this._items.length,
        map = function (index) {
      return index % 2 === 0 ? even + index / 2 : odd - (index + 1) / 2;
    };

    if (position === undefined) {
      return $.map(this._clones, function (v, i) {
        return map(i);
      });
    }

    return $.map(this._clones, function (v, i) {
      return v === position ? map(i) : null;
    });
  };


  Owl.prototype.speed = function (speed) {
    if (speed !== undefined) {
      this._speed = speed;
    }

    return this._speed;
  };


  Owl.prototype.coordinates = function (position) {
    var multiplier = 1,
        newPosition = position - 1,
        coordinate;

    if (position === undefined) {
      return $.map(this._coordinates, $.proxy(function (coordinate, index) {
        return this.coordinates(index);
      }, this));
    }

    if (this.settings.center) {
      if (this.settings.rtl) {
        multiplier = -1;
        newPosition = position + 1;
      }

      coordinate = this._coordinates[position];
      coordinate += (this.width() - coordinate + (this._coordinates[newPosition] || 0)) / 2 * multiplier;
    } else {
      coordinate = this._coordinates[newPosition] || 0;
    }

    coordinate = Math.ceil(coordinate);
    return coordinate;
  };


  Owl.prototype.duration = function (from, to, factor) {
    if (factor === 0) {
      return 0;
    }

    return Math.min(Math.max(Math.abs(to - from), 1), 6) * Math.abs(factor || this.settings.smartSpeed);
  };


  Owl.prototype.to = function (position, speed) {
    var current = this.current(),
        revert = null,
        distance = position - this.relative(current),
        direction = (distance > 0) - (distance < 0),
        items = this._items.length,
        minimum = this.minimum(),
        maximum = this.maximum();

    if (this.settings.loop) {
      if (!this.settings.rewind && Math.abs(distance) > items / 2) {
        distance += direction * -1 * items;
      }

      position = current + distance;
      revert = ((position - minimum) % items + items) % items + minimum;

      if (revert !== position && revert - distance <= maximum && revert - distance > 0) {
        current = revert - distance;
        position = revert;
        this.reset(current);
      }
    } else if (this.settings.rewind) {
      maximum += 1;
      position = (position % maximum + maximum) % maximum;
    } else {
      position = Math.max(minimum, Math.min(maximum, position));
    }

    this.speed(this.duration(current, position, speed));
    this.current(position);

    if (this.isVisible()) {
      this.update();
    }
  };


  Owl.prototype.next = function (speed) {
    speed = speed || false;
    this.to(this.relative(this.current()) + 1, speed);
  };


  Owl.prototype.prev = function (speed) {
    speed = speed || false;
    this.to(this.relative(this.current()) - 1, speed);
  };


  Owl.prototype.onTransitionEnd = function (event) {
    if (event !== undefined) {
      event.stopPropagation(); 

      if ((event.target || event.srcElement || event.originalTarget) !== this.$stage.get(0)) {
        return false;
      }
    }

    this.leave('animating');
    this.trigger('translated');
  };


  Owl.prototype.viewport = function () {
    var width;

    if (this.options.responsiveBaseElement !== window) {
      width = $(this.options.responsiveBaseElement).width();
    } else if (window.innerWidth) {
      width = window.innerWidth;
    } else if (document.documentElement && document.documentElement.clientWidth) {
      width = document.documentElement.clientWidth;
    } else {
      console.warn('Can not detect viewport width.');
    }

    return width;
  };


  Owl.prototype.replace = function (content) {
    this.$stage.empty();
    this._items = [];

    if (content) {
      content = content instanceof jQuery ? content : $(content);
    }

    if (this.settings.nestedItemSelector) {
      content = content.find('.' + this.settings.nestedItemSelector);
    }

    content.filter(function () {
      return this.nodeType === 1;
    }).each($.proxy(function (index, item) {
      item = this.prepare(item);
      this.$stage.append(item);

      this._items.push(item);

      this._mergers.push(item.find('[data-merge]').addBack('[data-merge]').attr('data-merge') * 1 || 1);
    }, this));
    this.reset(this.isNumeric(this.settings.startPosition) ? this.settings.startPosition : 0);
    this.invalidate('items');
  };


  Owl.prototype.add = function (content, position) {
    var current = this.relative(this._current);
    position = position === undefined ? this._items.length : this.normalize(position, true);
    content = content instanceof jQuery ? content : $(content);
    this.trigger('add', {
      content: content,
      position: position
    });
    content = this.prepare(content);

    if (this._items.length === 0 || position === this._items.length) {
      this._items.length === 0 && this.$stage.append(content);
      this._items.length !== 0 && this._items[position - 1].after(content);

      this._items.push(content);

      this._mergers.push(content.find('[data-merge]').addBack('[data-merge]').attr('data-merge') * 1 || 1);
    } else {
      this._items[position].before(content);

      this._items.splice(position, 0, content);

      this._mergers.splice(position, 0, content.find('[data-merge]').addBack('[data-merge]').attr('data-merge') * 1 || 1);
    }

    this._items[current] && this.reset(this._items[current].index());
    this.invalidate('items');
    this.trigger('added', {
      content: content,
      position: position
    });
  };


  Owl.prototype.remove = function (position) {
    position = this.normalize(position, true);

    if (position === undefined) {
      return;
    }

    this.trigger('remove', {
      content: this._items[position],
      position: position
    });

    this._items[position].remove();

    this._items.splice(position, 1);

    this._mergers.splice(position, 1);

    this.invalidate('items');
    this.trigger('removed', {
      content: null,
      position: position
    });
  };


  Owl.prototype.preloadAutoWidthImages = function (images) {
    images.each($.proxy(function (i, element) {
      this.enter('pre-loading');
      element = $(element);
      $(new Image()).one('load', $.proxy(function (e) {
        element.attr('src', e.target.src);
        element.css('opacity', 1);
        this.leave('pre-loading');
        !this.is('pre-loading') && !this.is('initializing') && this.refresh();
      }, this)).attr('src', element.attr('src') || element.attr('data-src') || element.attr('data-src-retina'));
    }, this));
  };


  Owl.prototype.destroy = function () {
    this.$element.off('.owl.core');
    this.$stage.off('.owl.core');
    $(document).off('.owl.core');

    if (this.settings.responsive !== false) {
      window.clearTimeout(this.resizeTimer);
      this.off(window, 'resize', this._handlers.onThrottledResize);
    }

    for (var i in this._plugins) {
      this._plugins[i].destroy();
    }

    this.$stage.children('.cloned').remove();
    this.$stage.unwrap();
    this.$stage.children().contents().unwrap();
    this.$stage.children().unwrap();
    this.$stage.remove();
    this.$element.removeClass(this.options.refreshClass).removeClass(this.options.loadingClass).removeClass(this.options.loadedClass).removeClass(this.options.rtlClass).removeClass(this.options.dragClass).removeClass(this.options.grabClass).attr('class', this.$element.attr('class').replace(new RegExp(this.options.responsiveClass + '-\\S+\\s', 'g'), '')).removeData('owl.carousel');
  };


  Owl.prototype.op = function (a, o, b) {
    var rtl = this.settings.rtl;

    switch (o) {
      case '<':
        return rtl ? a > b : a < b;

      case '>':
        return rtl ? a < b : a > b;

      case '>=':
        return rtl ? a <= b : a >= b;

      case '<=':
        return rtl ? a >= b : a <= b;

      default:
        break;
    }
  };


  Owl.prototype.on = function (element, event, listener, capture) {
    if (element.addEventListener) {
      element.addEventListener(event, listener, capture);
    } else if (element.attachEvent) {
      element.attachEvent('on' + event, listener);
    }
  };


  Owl.prototype.off = function (element, event, listener, capture) {
    if (element.removeEventListener) {
      element.removeEventListener(event, listener, capture);
    } else if (element.detachEvent) {
      element.detachEvent('on' + event, listener);
    }
  };


  Owl.prototype.trigger = function (name, data, namespace, state, enter) {
    var status = {
      item: {
        count: this._items.length,
        index: this.current()
      }
    },
        handler = $.camelCase($.grep(['on', name, namespace], function (v) {
      return v;
    }).join('-').toLowerCase()),
        event = $.Event([name, 'owl', namespace || 'carousel'].join('.').toLowerCase(), $.extend({
      relatedTarget: this
    }, status, data));

    if (!this._supress[name]) {
      $.each(this._plugins, function (name, plugin) {
        if (plugin.onTrigger) {
          plugin.onTrigger(event);
        }
      });
      this.register({
        type: Owl.Type.Event,
        name: name
      });
      this.$element.trigger(event);

      if (this.settings && typeof this.settings[handler] === 'function') {
        this.settings[handler].call(this, event);
      }
    }

    return event;
  };


  Owl.prototype.enter = function (name) {
    $.each([name].concat(this._states.tags[name] || []), $.proxy(function (i, name) {
      if (this._states.current[name] === undefined) {
        this._states.current[name] = 0;
      }

      this._states.current[name]++;
    }, this));
  };


  Owl.prototype.leave = function (name) {
    $.each([name].concat(this._states.tags[name] || []), $.proxy(function (i, name) {
      this._states.current[name]--;
    }, this));
  };


  Owl.prototype.register = function (object) {
    if (object.type === Owl.Type.Event) {
      if (!$.event.special[object.name]) {
        $.event.special[object.name] = {};
      }

      if (!$.event.special[object.name].owl) {
        var _default = $.event.special[object.name]._default;

        $.event.special[object.name]._default = function (e) {
          if (_default && _default.apply && (!e.namespace || e.namespace.indexOf('owl') === -1)) {
            return _default.apply(this, arguments);
          }

          return e.namespace && e.namespace.indexOf('owl') > -1;
        };

        $.event.special[object.name].owl = true;
      }
    } else if (object.type === Owl.Type.State) {
      if (!this._states.tags[object.name]) {
        this._states.tags[object.name] = object.tags;
      } else {
        this._states.tags[object.name] = this._states.tags[object.name].concat(object.tags);
      }

      this._states.tags[object.name] = $.grep(this._states.tags[object.name], $.proxy(function (tag, i) {
        return $.inArray(tag, this._states.tags[object.name]) === i;
      }, this));
    }
  };


  Owl.prototype.suppress = function (events) {
    $.each(events, $.proxy(function (index, event) {
      this._supress[event] = true;
    }, this));
  };


  Owl.prototype.release = function (events) {
    $.each(events, $.proxy(function (index, event) {
      delete this._supress[event];
    }, this));
  };


  Owl.prototype.pointer = function (event) {
    var result = {
      x: null,
      y: null
    };
    event = event.originalEvent || event || window.event;
    event = event.touches && event.touches.length ? event.touches[0] : event.changedTouches && event.changedTouches.length ? event.changedTouches[0] : event;

    if (event.pageX) {
      result.x = event.pageX;
      result.y = event.pageY;
    } else {
      result.x = event.clientX;
      result.y = event.clientY;
    }

    return result;
  };


  Owl.prototype.isNumeric = function (number) {
    return !isNaN(parseFloat(number));
  };


  Owl.prototype.difference = function (first, second) {
    return {
      x: first.x - second.x,
      y: first.y - second.y
    };
  };


  $.fn.owlCarousel = function (option) {
    var args = Array.prototype.slice.call(arguments, 1);
    return this.each(function () {
      var $this = $(this),
          data = $this.data('owl.carousel');

      if (!data) {
        data = new Owl(this, typeof option == 'object' && option);
        $this.data('owl.carousel', data);
        $.each(['next', 'prev', 'to', 'destroy', 'refresh', 'replace', 'add', 'remove'], function (i, event) {
          data.register({
            type: Owl.Type.Event,
            name: event
          });
          data.$element.on(event + '.owl.carousel.core', $.proxy(function (e) {
            if (e.namespace && e.relatedTarget !== this) {
              this.suppress([event]);
              data[event].apply(this, [].slice.call(arguments, 1));
              this.release([event]);
            }
          }, data));
        });
      }

      if (typeof option == 'string' && option.charAt(0) !== '_') {
        data[option].apply(data, args);
      }
    });
  };


  $.fn.owlCarousel.Constructor = Owl;
})(window.Zepto || window.jQuery, window, document);


;

(function ($, window, document, undefined) {
  var AutoRefresh = function (carousel) {
    this._core = carousel;

    this._interval = null;

    this._visible = null;

    this._handlers = {
      'initialized.owl.carousel': $.proxy(function (e) {
        if (e.namespace && this._core.settings.autoRefresh) {
          this.watch();
        }
      }, this)
    }; 

    this._core.options = $.extend({}, AutoRefresh.Defaults, this._core.options); 

    this._core.$element.on(this._handlers);
  };


  AutoRefresh.Defaults = {
    autoRefresh: true,
    autoRefreshInterval: 500
  };

  AutoRefresh.prototype.watch = function () {
    if (this._interval) {
      return;
    }

    this._visible = this._core.isVisible();
    this._interval = window.setInterval($.proxy(this.refresh, this), this._core.settings.autoRefreshInterval);
  };


  AutoRefresh.prototype.refresh = function () {
    if (this._core.isVisible() === this._visible) {
      return;
    }

    this._visible = !this._visible;

    this._core.$element.toggleClass('owl-hidden', !this._visible);

    this._visible && this._core.invalidate('width') && this._core.refresh();
  };


  AutoRefresh.prototype.destroy = function () {
    var handler, property;
    window.clearInterval(this._interval);

    for (handler in this._handlers) {
      this._core.$element.off(handler, this._handlers[handler]);
    }

    for (property in Object.getOwnPropertyNames(this)) {
      typeof this[property] != 'function' && (this[property] = null);
    }
  };

  $.fn.owlCarousel.Constructor.Plugins.AutoRefresh = AutoRefresh;
})(window.Zepto || window.jQuery, window, document);


;

(function ($, window, document, undefined) {
  var Lazy = function (carousel) {
    this._core = carousel;

    this._loaded = [];

    this._handlers = {
      'initialized.owl.carousel change.owl.carousel resized.owl.carousel': $.proxy(function (e) {
        if (!e.namespace) {
          return;
        }

        if (!this._core.settings || !this._core.settings.lazyLoad) {
          return;
        }

        if (e.property && e.property.name == 'position' || e.type == 'initialized') {
          var settings = this._core.settings,
              n = settings.center && Math.ceil(settings.items / 2) || settings.items,
              i = settings.center && n * -1 || 0,
              position = (e.property && e.property.value !== undefined ? e.property.value : this._core.current()) + i,
              clones = this._core.clones().length,
              load = $.proxy(function (i, v) {
            this.load(v);
          }, this);

          while (i++ < n) {
            this.load(clones / 2 + this._core.relative(position));
            clones && $.each(this._core.clones(this._core.relative(position)), load);
            position++;
          }
        }
      }, this)
    }; 

    this._core.options = $.extend({}, Lazy.Defaults, this._core.options); 

    this._core.$element.on(this._handlers);
  };


  Lazy.Defaults = {
    lazyLoad: false
  };

  Lazy.prototype.load = function (position) {
    var $item = this._core.$stage.children().eq(position),
        $elements = $item && $item.find('.owl-lazy');

    if (!$elements || $.inArray($item.get(0), this._loaded) > -1) {
      return;
    }

    $elements.each($.proxy(function (index, element) {
      var $element = $(element),
          image,
          url = window.devicePixelRatio > 1 && $element.attr('data-src-retina') || $element.attr('data-src') || $element.attr('data-srcset');

      this._core.trigger('load', {
        element: $element,
        url: url
      }, 'lazy');

      if ($element.is('img')) {
        $element.one('load.owl.lazy', $.proxy(function () {
          $element.css('opacity', 1);

          this._core.trigger('loaded', {
            element: $element,
            url: url
          }, 'lazy');
        }, this)).attr('src', url);
      } else if ($element.is('source')) {
        $element.one('load.owl.lazy', $.proxy(function () {
          this._core.trigger('loaded', {
            element: $element,
            url: url
          }, 'lazy');
        }, this)).attr('srcset', url);
      } else {
        image = new Image();
        image.onload = $.proxy(function () {
          $element.css({
            'background-image': 'url("' + url + '")',
            'opacity': '1'
          });

          this._core.trigger('loaded', {
            element: $element,
            url: url
          }, 'lazy');
        }, this);
        image.src = url;
      }
    }, this));

    this._loaded.push($item.get(0));
  };


  Lazy.prototype.destroy = function () {
    var handler, property;

    for (handler in this.handlers) {
      this._core.$element.off(handler, this.handlers[handler]);
    }

    for (property in Object.getOwnPropertyNames(this)) {
      typeof this[property] != 'function' && (this[property] = null);
    }
  };

  $.fn.owlCarousel.Constructor.Plugins.Lazy = Lazy;
})(window.Zepto || window.jQuery, window, document);


;

(function ($, window, document, undefined) {
  var AutoHeight = function (carousel) {
    this._core = carousel;

    this._handlers = {
      'initialized.owl.carousel refreshed.owl.carousel': $.proxy(function (e) {
        if (e.namespace && this._core.settings.autoHeight) {
          this.update();
        }
      }, this),
      'changed.owl.carousel': $.proxy(function (e) {
        if (e.namespace && this._core.settings.autoHeight && e.property.name === 'position') {
          console.log('update called');
          this.update();
        }
      }, this),
      'loaded.owl.lazy': $.proxy(function (e) {
        if (e.namespace && this._core.settings.autoHeight && e.element.closest('.' + this._core.settings.itemClass).index() === this._core.current()) {
          this.update();
        }
      }, this)
    }; 

    this._core.options = $.extend({}, AutoHeight.Defaults, this._core.options); 

    this._core.$element.on(this._handlers);

    this._intervalId = null;
    var refThis = this; 

    $(window).on('load', function () {
      if (refThis._core.settings.autoHeight) {
        refThis.update();
      }
    }); 

    $(window).resize(function () {
      if (refThis._core.settings.autoHeight) {
        if (refThis._intervalId != null) {
          clearTimeout(refThis._intervalId);
        }

        refThis._intervalId = setTimeout(function () {
          refThis.update();
        }, 250);
      }
    });
  };


  AutoHeight.Defaults = {
    autoHeight: false,
    autoHeightClass: 'owl-height'
  };

  AutoHeight.prototype.update = function () {
    var start = this._core._current,
        end = start + this._core.settings.items,
        visible = this._core.$stage.children().toArray().slice(start, end),
        heights = [],
        maxheight = 0;

    $.each(visible, function (index, item) {
      heights.push($(item).height());
    });
    maxheight = Math.max.apply(null, heights);

    this._core.$stage.parent().height(maxheight).addClass(this._core.settings.autoHeightClass);
  };

  AutoHeight.prototype.destroy = function () {
    var handler, property;

    for (handler in this._handlers) {
      this._core.$element.off(handler, this._handlers[handler]);
    }

    for (property in Object.getOwnPropertyNames(this)) {
      typeof this[property] !== 'function' && (this[property] = null);
    }
  };

  $.fn.owlCarousel.Constructor.Plugins.AutoHeight = AutoHeight;
})(window.Zepto || window.jQuery, window, document);


;

(function ($, window, document, undefined) {
  var Video = function (carousel) {
    this._core = carousel;

    this._videos = {};

    this._playing = null;

    this._handlers = {
      'initialized.owl.carousel': $.proxy(function (e) {
        if (e.namespace) {
          this._core.register({
            type: 'state',
            name: 'playing',
            tags: ['interacting']
          });
        }
      }, this),
      'resize.owl.carousel': $.proxy(function (e) {
        if (e.namespace && this._core.settings.video && this.isInFullScreen()) {
          e.preventDefault();
        }
      }, this),
      'refreshed.owl.carousel': $.proxy(function (e) {
        if (e.namespace && this._core.is('resizing')) {
          this._core.$stage.find('.cloned .owl-video-frame').remove();
        }
      }, this),
      'changed.owl.carousel': $.proxy(function (e) {
        if (e.namespace && e.property.name === 'position' && this._playing) {
          this.stop();
        }
      }, this),
      'prepared.owl.carousel': $.proxy(function (e) {
        if (!e.namespace) {
          return;
        }

        var $element = $(e.content).find('.owl-video');

        if ($element.length) {
          $element.css('display', 'none');
          this.fetch($element, $(e.content));
        }
      }, this)
    }; 

    this._core.options = $.extend({}, Video.Defaults, this._core.options); 

    this._core.$element.on(this._handlers);

    this._core.$element.on('click.owl.video', '.owl-video-play-icon', $.proxy(function (e) {
      this.play(e);
    }, this));
  };


  Video.Defaults = {
    video: false,
    videoHeight: false,
    videoWidth: false
  };

  Video.prototype.fetch = function (target, item) {
    var type = function () {
      if (target.attr('data-vimeo-id')) {
        return 'vimeo';
      } else if (target.attr('data-vzaar-id')) {
        return 'vzaar';
      } else {
        return 'youtube';
      }
    }(),
        id = target.attr('data-vimeo-id') || target.attr('data-youtube-id') || target.attr('data-vzaar-id'),
        width = target.attr('data-width') || this._core.settings.videoWidth,
        height = target.attr('data-height') || this._core.settings.videoHeight,
        url = target.attr('href');

    if (url) {
      id = url.match(/(http:|https:|)\/\/(player.|www.|app.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com)|vzaar\.com)\/(video\/|videos\/|embed\/|channels\/.+\/|groups\/.+\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/);

      if (id[3].indexOf('youtu') > -1) {
        type = 'youtube';
      } else if (id[3].indexOf('vimeo') > -1) {
        type = 'vimeo';
      } else if (id[3].indexOf('vzaar') > -1) {
        type = 'vzaar';
      } else {
        throw new Error('Video URL not supported.');
      }

      id = id[6];
    } else {
      throw new Error('Missing video URL.');
    }

    this._videos[url] = {
      type: type,
      id: id,
      width: width,
      height: height
    };
    item.attr('data-video', url);
    this.thumbnail(target, this._videos[url]);
  };


  Video.prototype.thumbnail = function (target, video) {
    var tnLink,
        icon,
        path,
        dimensions = video.width && video.height ? 'style="width:' + video.width + 'px;height:' + video.height + 'px;"' : '',
        customTn = target.find('img'),
        srcType = 'src',
        lazyClass = '',
        settings = this._core.settings,
        create = function (path) {
      icon = '<div class="owl-video-play-icon"></div>';

      if (settings.lazyLoad) {
        tnLink = '<div class="owl-video-tn ' + lazyClass + '" ' + srcType + '="' + path + '"></div>';
      } else {
        tnLink = '<div class="owl-video-tn" style="opacity:1;background-image:url(' + path + ')"></div>';
      }

      target.after(tnLink);
      target.after(icon);
    }; 


    target.wrap('<div class="owl-video-wrapper"' + dimensions + '></div>');

    if (this._core.settings.lazyLoad) {
      srcType = 'data-src';
      lazyClass = 'owl-lazy';
    } 


    if (customTn.length) {
      create(customTn.attr(srcType));
      customTn.remove();
      return false;
    }

    if (video.type === 'youtube') {
      path = "//img.youtube.com/vi/" + video.id + "/hqdefault.jpg";
      create(path);
    } else if (video.type === 'vimeo') {
      $.ajax({
        type: 'GET',
        url: '//vimeo.com/api/v2/video/' + video.id + '.json',
        jsonp: 'callback',
        dataType: 'jsonp',
        success: function (data) {
          path = data[0].thumbnail_large;
          create(path);
        }
      });
    } else if (video.type === 'vzaar') {
      $.ajax({
        type: 'GET',
        url: '//vzaar.com/api/videos/' + video.id + '.json',
        jsonp: 'callback',
        dataType: 'jsonp',
        success: function (data) {
          path = data.framegrab_url;
          create(path);
        }
      });
    }
  };


  Video.prototype.stop = function () {
    this._core.trigger('stop', null, 'video');

    this._playing.find('.owl-video-frame').remove();

    this._playing.removeClass('owl-video-playing');

    this._playing = null;

    this._core.leave('playing');

    this._core.trigger('stopped', null, 'video');
  };


  Video.prototype.play = function (event) {
    var target = $(event.target),
        item = target.closest('.' + this._core.settings.itemClass),
        video = this._videos[item.attr('data-video')],
        width = video.width || '100%',
        height = video.height || this._core.$stage.height(),
        html;

    if (this._playing) {
      return;
    }

    this._core.enter('playing');

    this._core.trigger('play', null, 'video');

    item = this._core.items(this._core.relative(item.index()));

    this._core.reset(item.index());

    if (video.type === 'youtube') {
      html = '<iframe width="' + width + '" height="' + height + '" src="//www.youtube.com/embed/' + video.id + '?autoplay=1&rel=0&v=' + video.id + '" frameborder="0" allowfullscreen></iframe>';
    } else if (video.type === 'vimeo') {
      html = '<iframe src="//player.vimeo.com/video/' + video.id + '?autoplay=1" width="' + width + '" height="' + height + '" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
    } else if (video.type === 'vzaar') {
      html = '<iframe frameborder="0"' + 'height="' + height + '"' + 'width="' + width + '" allowfullscreen mozallowfullscreen webkitAllowFullScreen ' + 'src="//view.vzaar.com/' + video.id + '/player?autoplay=true"></iframe>';
    }

    $('<div class="owl-video-frame">' + html + '</div>').insertAfter(item.find('.owl-video'));
    this._playing = item.addClass('owl-video-playing');
  };


  Video.prototype.isInFullScreen = function () {
    var element = document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement;
    return element && $(element).parent().hasClass('owl-video-frame');
  };


  Video.prototype.destroy = function () {
    var handler, property;

    this._core.$element.off('click.owl.video');

    for (handler in this._handlers) {
      this._core.$element.off(handler, this._handlers[handler]);
    }

    for (property in Object.getOwnPropertyNames(this)) {
      typeof this[property] != 'function' && (this[property] = null);
    }
  };

  $.fn.owlCarousel.Constructor.Plugins.Video = Video;
})(window.Zepto || window.jQuery, window, document);


;

(function ($, window, document, undefined) {
  var Animate = function (scope) {
    this.core = scope;
    this.core.options = $.extend({}, Animate.Defaults, this.core.options);
    this.swapping = true;
    this.previous = undefined;
    this.next = undefined;
    this.handlers = {
      'change.owl.carousel': $.proxy(function (e) {
        if (e.namespace && e.property.name == 'position') {
          this.previous = this.core.current();
          this.next = e.property.value;
        }
      }, this),
      'drag.owl.carousel dragged.owl.carousel translated.owl.carousel': $.proxy(function (e) {
        if (e.namespace) {
          this.swapping = e.type == 'translated';
        }
      }, this),
      'translate.owl.carousel': $.proxy(function (e) {
        if (e.namespace && this.swapping && (this.core.options.animateOut || this.core.options.animateIn)) {
          this.swap();
        }
      }, this)
    };
    this.core.$element.on(this.handlers);
  };


  Animate.Defaults = {
    animateOut: false,
    animateIn: false
  };

  Animate.prototype.swap = function () {
    if (this.core.settings.items !== 1) {
      return;
    }

    if (!$.support.animation || !$.support.transition) {
      return;
    }

    this.core.speed(0);
    var left,
        clear = $.proxy(this.clear, this),
        previous = this.core.$stage.children().eq(this.previous),
        next = this.core.$stage.children().eq(this.next),
        incoming = this.core.settings.animateIn,
        outgoing = this.core.settings.animateOut;

    if (this.core.current() === this.previous) {
      return;
    }

    if (outgoing) {
      left = this.core.coordinates(this.previous) - this.core.coordinates(this.next);
      previous.one($.support.animation.end, clear).css({
        'left': left + 'px'
      }).addClass('animated owl-animated-out').addClass(outgoing);
    }

    if (incoming) {
      next.one($.support.animation.end, clear).addClass('animated owl-animated-in').addClass(incoming);
    }
  };

  Animate.prototype.clear = function (e) {
    $(e.target).css({
      'left': ''
    }).removeClass('animated owl-animated-out owl-animated-in').removeClass(this.core.settings.animateIn).removeClass(this.core.settings.animateOut);
    this.core.onTransitionEnd();
  };


  Animate.prototype.destroy = function () {
    var handler, property;

    for (handler in this.handlers) {
      this.core.$element.off(handler, this.handlers[handler]);
    }

    for (property in Object.getOwnPropertyNames(this)) {
      typeof this[property] != 'function' && (this[property] = null);
    }
  };

  $.fn.owlCarousel.Constructor.Plugins.Animate = Animate;
})(window.Zepto || window.jQuery, window, document);


;

(function ($, window, document, undefined) {
  var Autoplay = function (carousel) {
    this._core = carousel;

    this._call = null;

    this._time = 0;

    this._timeout = 0;

    this._paused = true;

    this._handlers = {
      'changed.owl.carousel': $.proxy(function (e) {
        if (e.namespace && e.property.name === 'settings') {
          if (this._core.settings.autoplay) {
            this.play();
          } else {
            this.stop();
          }
        } else if (e.namespace && e.property.name === 'position' && this._paused) {
          this._time = 0;
        }
      }, this),
      'initialized.owl.carousel': $.proxy(function (e) {
        if (e.namespace && this._core.settings.autoplay) {
          this.play();
        }
      }, this),
      'play.owl.autoplay': $.proxy(function (e, t, s) {
        if (e.namespace) {
          this.play(t, s);
        }
      }, this),
      'stop.owl.autoplay': $.proxy(function (e) {
        if (e.namespace) {
          this.stop();
        }
      }, this),
      'mouseover.owl.autoplay': $.proxy(function () {
        if (this._core.settings.autoplayHoverPause && this._core.is('rotating')) {
          this.pause();
        }
      }, this),
      'mouseleave.owl.autoplay': $.proxy(function () {
        if (this._core.settings.autoplayHoverPause && this._core.is('rotating')) {
          this.play();
        }
      }, this),
      'touchstart.owl.core': $.proxy(function () {
        if (this._core.settings.autoplayHoverPause && this._core.is('rotating')) {
          this.pause();
        }
      }, this),
      'touchend.owl.core': $.proxy(function () {
        if (this._core.settings.autoplayHoverPause) {
          this.play();
        }
      }, this)
    }; 

    this._core.$element.on(this._handlers); 


    this._core.options = $.extend({}, Autoplay.Defaults, this._core.options);
  };


  Autoplay.Defaults = {
    autoplay: false,
    autoplayTimeout: 5000,
    autoplayHoverPause: false,
    autoplaySpeed: false
  };

  Autoplay.prototype._next = function (speed) {
    this._call = window.setTimeout($.proxy(this._next, this, speed), this._timeout * (Math.round(this.read() / this._timeout) + 1) - this.read());

    if (this._core.is('busy') || this._core.is('interacting') || document.hidden) {
      return;
    }

    this._core.next(speed || this._core.settings.autoplaySpeed);
  };


  Autoplay.prototype.read = function () {
    return new Date().getTime() - this._time;
  };


  Autoplay.prototype.play = function (timeout, speed) {
    var elapsed;

    if (!this._core.is('rotating')) {
      this._core.enter('rotating');
    }

    timeout = timeout || this._core.settings.autoplayTimeout; 

    elapsed = Math.min(this._time % (this._timeout || timeout), timeout);

    if (this._paused) {
      this._time = this.read();
      this._paused = false;
    } else {
      window.clearTimeout(this._call);
    } 


    this._time += this.read() % timeout - elapsed;
    this._timeout = timeout;
    this._call = window.setTimeout($.proxy(this._next, this, speed), timeout - elapsed);
  };


  Autoplay.prototype.stop = function () {
    if (this._core.is('rotating')) {
      this._time = 0;
      this._paused = true;
      window.clearTimeout(this._call);

      this._core.leave('rotating');
    }
  };


  Autoplay.prototype.pause = function () {
    if (this._core.is('rotating') && !this._paused) {
      this._time = this.read();
      this._paused = true;
      window.clearTimeout(this._call);
    }
  };


  Autoplay.prototype.destroy = function () {
    var handler, property;
    this.stop();

    for (handler in this._handlers) {
      this._core.$element.off(handler, this._handlers[handler]);
    }

    for (property in Object.getOwnPropertyNames(this)) {
      typeof this[property] != 'function' && (this[property] = null);
    }
  };

  $.fn.owlCarousel.Constructor.Plugins.autoplay = Autoplay;
})(window.Zepto || window.jQuery, window, document);


;

(function ($, window, document, undefined) {
  'use strict';

  var Navigation = function (carousel) {
    this._core = carousel;

    this._initialized = false;

    this._pages = [];

    this._controls = {};

    this._templates = [];

    this.$element = this._core.$element;

    this._overrides = {
      next: this._core.next,
      prev: this._core.prev,
      to: this._core.to
    };

    this._handlers = {
      'prepared.owl.carousel': $.proxy(function (e) {
        if (e.namespace && this._core.settings.dotsData) {
          this._templates.push('<div class="' + this._core.settings.dotClass + '">' + $(e.content).find('[data-dot]').addBack('[data-dot]').attr('data-dot') + '</div>');
        }
      }, this),
      'added.owl.carousel': $.proxy(function (e) {
        if (e.namespace && this._core.settings.dotsData) {
          this._templates.splice(e.position, 0, this._templates.pop());
        }
      }, this),
      'remove.owl.carousel': $.proxy(function (e) {
        if (e.namespace && this._core.settings.dotsData) {
          this._templates.splice(e.position, 1);
        }
      }, this),
      'changed.owl.carousel': $.proxy(function (e) {
        if (e.namespace && e.property.name == 'position') {
          this.draw();
        }
      }, this),
      'initialized.owl.carousel': $.proxy(function (e) {
        if (e.namespace && !this._initialized) {
          this._core.trigger('initialize', null, 'navigation');

          this.initialize();
          this.update();
          this.draw();
          this._initialized = true;

          this._core.trigger('initialized', null, 'navigation');
        }
      }, this),
      'refreshed.owl.carousel': $.proxy(function (e) {
        if (e.namespace && this._initialized) {
          this._core.trigger('refresh', null, 'navigation');

          this.update();
          this.draw();

          this._core.trigger('refreshed', null, 'navigation');
        }
      }, this)
    }; 

    this._core.options = $.extend({}, Navigation.Defaults, this._core.options); 

    this.$element.on(this._handlers);
  };


  Navigation.Defaults = {
    nav: false,
    navText: ['<span aria-label="' + 'Previous' + '">&#x2039;</span>', '<span aria-label="' + 'Next' + '">&#x203a;</span>'],
    navSpeed: false,
    navElement: 'button type="button" role="presentation"',
    navContainer: false,
    navContainerClass: 'owl-nav',
    navClass: ['owl-prev', 'owl-next'],
    slideBy: 1,
    dotClass: 'owl-dot',
    dotsClass: 'owl-dots',
    dots: true,
    dotsEach: false,
    dotsData: false,
    dotsSpeed: false,
    dotsContainer: false
  };

  Navigation.prototype.initialize = function () {
    var override,
        settings = this._core.settings; 

    this._controls.$relative = (settings.navContainer ? $(settings.navContainer) : $('<div>').addClass(settings.navContainerClass).appendTo(this.$element)).addClass('disabled');
    this._controls.$previous = $('<' + settings.navElement + '>').addClass(settings.navClass[0]).html(settings.navText[0]).prependTo(this._controls.$relative).on('click', $.proxy(function (e) {
      this.prev(settings.navSpeed);
    }, this));
    this._controls.$next = $('<' + settings.navElement + '>').addClass(settings.navClass[1]).html(settings.navText[1]).appendTo(this._controls.$relative).on('click', $.proxy(function (e) {
      this.next(settings.navSpeed);
    }, this)); 

    if (!settings.dotsData) {
      this._templates = [$('<button role="button">').addClass(settings.dotClass).append($('<span>')).prop('outerHTML')];
    }

    this._controls.$absolute = (settings.dotsContainer ? $(settings.dotsContainer) : $('<div>').addClass(settings.dotsClass).appendTo(this.$element)).addClass('disabled');

    this._controls.$absolute.on('click', 'button', $.proxy(function (e) {
      var index = $(e.target).parent().is(this._controls.$absolute) ? $(e.target).index() : $(e.target).parent().index();
      e.preventDefault();
      this.to(index, settings.dotsSpeed);
    }, this));


    for (override in this._overrides) {
      this._core[override] = $.proxy(this[override], this);
    }
  };


  Navigation.prototype.destroy = function () {
    var handler, control, property, override, settings;
    settings = this._core.settings;

    for (handler in this._handlers) {
      this.$element.off(handler, this._handlers[handler]);
    }

    for (control in this._controls) {
      if (control === '$relative' && settings.navContainer) {
        this._controls[control].html('');
      } else {
        this._controls[control].remove();
      }
    }

    for (override in this.overides) {
      this._core[override] = this._overrides[override];
    }

    for (property in Object.getOwnPropertyNames(this)) {
      typeof this[property] != 'function' && (this[property] = null);
    }
  };


  Navigation.prototype.update = function () {
    var i,
        j,
        k,
        lower = this._core.clones().length / 2,
        upper = lower + this._core.items().length,
        maximum = this._core.maximum(true),
        settings = this._core.settings,
        size = settings.center || settings.autoWidth || settings.dotsData ? 1 : settings.dotsEach || settings.items;

    if (settings.slideBy !== 'page') {
      settings.slideBy = Math.min(settings.slideBy, settings.items);
    }

    if (settings.dots || settings.slideBy == 'page') {
      this._pages = [];

      for (i = lower, j = 0, k = 0; i < upper; i++) {
        if (j >= size || j === 0) {
          this._pages.push({
            start: Math.min(maximum, i - lower),
            end: i - lower + size - 1
          });

          if (Math.min(maximum, i - lower) === maximum) {
            break;
          }

          j = 0, ++k;
        }

        j += this._core.mergers(this._core.relative(i));
      }
    }
  };


  Navigation.prototype.draw = function () {
    var difference,
        settings = this._core.settings,
        disabled = this._core.items().length <= settings.items,
        index = this._core.relative(this._core.current()),
        loop = settings.loop || settings.rewind;

    this._controls.$relative.toggleClass('disabled', !settings.nav || disabled);

    if (settings.nav) {
      this._controls.$previous.toggleClass('disabled', !loop && index <= this._core.minimum(true));

      this._controls.$next.toggleClass('disabled', !loop && index >= this._core.maximum(true));
    }

    this._controls.$absolute.toggleClass('disabled', !settings.dots || disabled);

    if (settings.dots) {
      difference = this._pages.length - this._controls.$absolute.children().length;

      if (settings.dotsData && difference !== 0) {
        this._controls.$absolute.html(this._templates.join(''));
      } else if (difference > 0) {
        this._controls.$absolute.append(new Array(difference + 1).join(this._templates[0]));
      } else if (difference < 0) {
        this._controls.$absolute.children().slice(difference).remove();
      }

      this._controls.$absolute.find('.active').removeClass('active');

      this._controls.$absolute.children().eq($.inArray(this.current(), this._pages)).addClass('active');
    }
  };


  Navigation.prototype.onTrigger = function (event) {
    var settings = this._core.settings;
    event.page = {
      index: $.inArray(this.current(), this._pages),
      count: this._pages.length,
      size: settings && (settings.center || settings.autoWidth || settings.dotsData ? 1 : settings.dotsEach || settings.items)
    };
  };


  Navigation.prototype.current = function () {
    var current = this._core.relative(this._core.current());

    return $.grep(this._pages, $.proxy(function (page, index) {
      return page.start <= current && page.end >= current;
    }, this)).pop();
  };


  Navigation.prototype.getPosition = function (successor) {
    var position,
        length,
        settings = this._core.settings;

    if (settings.slideBy == 'page') {
      position = $.inArray(this.current(), this._pages);
      length = this._pages.length;
      successor ? ++position : --position;
      position = this._pages[(position % length + length) % length].start;
    } else {
      position = this._core.relative(this._core.current());
      length = this._core.items().length;
      successor ? position += settings.slideBy : position -= settings.slideBy;
    }

    return position;
  };


  Navigation.prototype.next = function (speed) {
    $.proxy(this._overrides.to, this._core)(this.getPosition(true), speed);
  };


  Navigation.prototype.prev = function (speed) {
    $.proxy(this._overrides.to, this._core)(this.getPosition(false), speed);
  };


  Navigation.prototype.to = function (position, speed, standard) {
    var length;

    if (!standard && this._pages.length) {
      length = this._pages.length;
      $.proxy(this._overrides.to, this._core)(this._pages[(position % length + length) % length].start, speed);
    } else {
      $.proxy(this._overrides.to, this._core)(position, speed);
    }
  };

  $.fn.owlCarousel.Constructor.Plugins.Navigation = Navigation;
})(window.Zepto || window.jQuery, window, document);


;

(function ($, window, document, undefined) {
  'use strict';

  var Hash = function (carousel) {
    this._core = carousel;

    this._hashes = {};

    this.$element = this._core.$element;

    this._handlers = {
      'initialized.owl.carousel': $.proxy(function (e) {
        if (e.namespace && this._core.settings.startPosition === 'URLHash') {
          $(window).trigger('hashchange.owl.navigation');
        }
      }, this),
      'prepared.owl.carousel': $.proxy(function (e) {
        if (e.namespace) {
          var hash = $(e.content).find('[data-hash]').addBack('[data-hash]').attr('data-hash');

          if (!hash) {
            return;
          }

          this._hashes[hash] = e.content;
        }
      }, this),
      'changed.owl.carousel': $.proxy(function (e) {
        if (e.namespace && e.property.name === 'position') {
          var current = this._core.items(this._core.relative(this._core.current())),
              hash = $.map(this._hashes, function (item, hash) {
            return item === current ? hash : null;
          }).join();

          if (!hash || window.location.hash.slice(1) === hash) {
            return;
          }

          window.location.hash = hash;
        }
      }, this)
    }; 

    this._core.options = $.extend({}, Hash.Defaults, this._core.options); 

    this.$element.on(this._handlers); 

    $(window).on('hashchange.owl.navigation', $.proxy(function (e) {
      var hash = window.location.hash.substring(1),
          items = this._core.$stage.children(),
          position = this._hashes[hash] && items.index(this._hashes[hash]);

      if (position === undefined || position === this._core.current()) {
        return;
      }

      this._core.to(this._core.relative(position), false, true);
    }, this));
  };


  Hash.Defaults = {
    URLhashListener: false
  };

  Hash.prototype.destroy = function () {
    var handler, property;
    $(window).off('hashchange.owl.navigation');

    for (handler in this._handlers) {
      this._core.$element.off(handler, this._handlers[handler]);
    }

    for (property in Object.getOwnPropertyNames(this)) {
      typeof this[property] != 'function' && (this[property] = null);
    }
  };

  $.fn.owlCarousel.Constructor.Plugins.Hash = Hash;
})(window.Zepto || window.jQuery, window, document);


;

(function ($, window, document, undefined) {
  var style = $('<support>').get(0).style,
      prefixes = 'Webkit Moz O ms'.split(' '),
      events = {
    transition: {
      end: {
        WebkitTransition: 'webkitTransitionEnd',
        MozTransition: 'transitionend',
        OTransition: 'oTransitionEnd',
        transition: 'transitionend'
      }
    },
    animation: {
      end: {
        WebkitAnimation: 'webkitAnimationEnd',
        MozAnimation: 'animationend',
        OAnimation: 'oAnimationEnd',
        animation: 'animationend'
      }
    }
  },
      tests = {
    csstransforms: function () {
      return !!test('transform');
    },
    csstransforms3d: function () {
      return !!test('perspective');
    },
    csstransitions: function () {
      return !!test('transition');
    },
    cssanimations: function () {
      return !!test('animation');
    }
  };

  function test(property, prefixed) {
    var result = false,
        upper = property.charAt(0).toUpperCase() + property.slice(1);
    $.each((property + ' ' + prefixes.join(upper + ' ') + upper).split(' '), function (i, property) {
      if (style[property] !== undefined) {
        result = prefixed ? property : true;
        return false;
      }
    });
    return result;
  }

  function prefixed(property) {
    return test(property, true);
  }

  if (tests.csstransitions()) {
    $.support.transition = new String(prefixed('transition'));
    $.support.transition.end = events.transition.end[$.support.transition];
  }

  if (tests.cssanimations()) {
    $.support.animation = new String(prefixed('animation'));
    $.support.animation.end = events.animation.end[$.support.animation];
  }

  if (tests.csstransforms()) {
    $.support.transform = new String(prefixed('transform'));
    $.support.transform3d = tests.csstransforms3d();
  }
})(window.Zepto || window.jQuery, window, document);
!function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['jquery'], factory);
  } else if (typeof exports === 'object') {
    factory(require('jquery'));
  } else {
    factory(root.jQuery);
  }
}(this, function ($) {
  'use strict';

  var PLUGIN_NAME = 'vide';

  var DEFAULTS = {
    volume: 1,
    playbackRate: 1,
    muted: true,
    loop: true,
    autoplay: true,
    position: '50% 50%',
    posterType: 'detect',
    resizing: true,
    bgColor: 'transparent',
    className: ''
  };

  var NOT_IMPLEMENTED_MSG = 'Not implemented';

  function parseOptions(str) {
    var obj = {};
    var delimiterIndex;
    var option;
    var prop;
    var val;
    var arr;
    var len;
    var i; 

    arr = str.replace(/\s*:\s*/g, ':').replace(/\s*,\s*/g, ',').split(','); 

    for (i = 0, len = arr.length; i < len; i++) {
      option = arr[i]; 

      if (option.search(/^(http|https|ftp):\/\//) !== -1 || option.search(':') === -1) {
        break;
      }

      delimiterIndex = option.indexOf(':');
      prop = option.substring(0, delimiterIndex);
      val = option.substring(delimiterIndex + 1); 

      if (!val) {
        val = undefined;
      } 


      if (typeof val === 'string') {
        val = val === 'true' || (val === 'false' ? false : val);
      } 


      if (typeof val === 'string') {
        val = !isNaN(val) ? +val : val;
      }

      obj[prop] = val;
    } 


    if (prop == null && val == null) {
      return str;
    }

    return obj;
  }


  function parsePosition(str) {
    str = '' + str; 

    var args = str.split(/\s+/);
    var x = '50%';
    var y = '50%';
    var len;
    var arg;
    var i;

    for (i = 0, len = args.length; i < len; i++) {
      arg = args[i]; 

      if (arg === 'left') {
        x = '0%';
      } else if (arg === 'right') {
        x = '100%';
      } else if (arg === 'top') {
        y = '0%';
      } else if (arg === 'bottom') {
        y = '100%';
      } else if (arg === 'center') {
        if (i === 0) {
          x = '50%';
        } else {
          y = '50%';
        }
      } else {
        if (i === 0) {
          x = arg;
        } else {
          y = arg;
        }
      }
    }

    return {
      x: x,
      y: y
    };
  }


  function findPoster(path, callback) {
    var onLoad = function () {
      callback(this.src);
    };

    $('<img src="' + path + '.gif">').on('load', onLoad);
    $('<img src="' + path + '.jpg">').on('load', onLoad);
    $('<img src="' + path + '.jpeg">').on('load', onLoad);
    $('<img src="' + path + '.png">').on('load', onLoad);
  }


  function Vide(element, path, options) {
    this.$element = $(element); 

    if (typeof path === 'string') {
      path = parseOptions(path);
    } 


    if (!options) {
      options = {};
    } else if (typeof options === 'string') {
      options = parseOptions(options);
    } 


    if (typeof path === 'string') {
      path = path.replace(/\.\w*$/, '');
    } else if (typeof path === 'object') {
      for (var i in path) {
        if (path.hasOwnProperty(i)) {
          path[i] = path[i].replace(/\.\w*$/, '');
        }
      }
    }

    this.settings = $.extend({}, DEFAULTS, options);
    this.path = path; 

    try {
      this.init();
    } catch (e) {
      if (e.message !== NOT_IMPLEMENTED_MSG) {
        throw e;
      }
    }
  }


  Vide.prototype.init = function () {
    var vide = this;
    var path = vide.path;
    var poster = path;
    var sources = '';
    var $element = vide.$element;
    var settings = vide.settings;
    var position = parsePosition(settings.position);
    var posterType = settings.posterType;
    var $video;
    var $wrapper; 

    $wrapper = vide.$wrapper = $('<div>').addClass(settings.className).css({
      position: 'absolute',
      'z-index': -1,
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
      overflow: 'hidden',
      '-webkit-background-size': 'cover',
      '-moz-background-size': 'cover',
      '-o-background-size': 'cover',
      'background-size': 'cover',
      'background-color': settings.bgColor,
      'background-repeat': 'no-repeat',
      'background-position': position.x + ' ' + position.y
    }); 

    if (typeof path === 'object') {
      if (path.poster) {
        poster = path.poster;
      } else {
        if (path.mp4) {
          poster = path.mp4;
        } else if (path.webm) {
          poster = path.webm;
        } else if (path.ogv) {
          poster = path.ogv;
        }
      }
    } 


    if (posterType === 'detect') {
      findPoster(poster, function (url) {
        $wrapper.css('background-image', 'url(' + url + ')');
      });
    } else if (posterType !== 'none') {
      $wrapper.css('background-image', 'url(' + poster + '.' + posterType + ')');
    } 


    if ($element.css('position') === 'static') {
      $element.css('position', 'relative');
    }

    $element.prepend($wrapper);

    if (typeof path === 'object') {
      if (path.mp4) {
        sources += '<source src="' + path.mp4 + '.mp4" type="video/mp4">';
      }

      if (path.webm) {
        sources += '<source src="' + path.webm + '.webm" type="video/webm">';
      }

      if (path.ogv) {
        sources += '<source src="' + path.ogv + '.ogv" type="video/ogg">';
      }

      $video = vide.$video = $('<video>' + sources + '</video>');
    } else {
      $video = vide.$video = $('<video>' + '<source src="' + path + '.mp4" type="video/mp4">' + '<source src="' + path + '.webm" type="video/webm">' + '<source src="' + path + '.ogv" type="video/ogg">' + '</video>');
    } 


    try {
      $video 
      .prop({
        autoplay: settings.autoplay,
        loop: settings.loop,
        volume: settings.volume,
        muted: settings.muted,
        defaultMuted: settings.muted,
        playbackRate: settings.playbackRate,
        defaultPlaybackRate: settings.playbackRate
      });
    } catch (e) {
      throw new Error(NOT_IMPLEMENTED_MSG);
    } 


    $video.css({
      margin: 'auto',
      position: 'absolute',
      'z-index': -1,
      top: position.y,
      left: position.x,
      '-webkit-transform': 'translate(-' + position.x + ', -' + position.y + ')',
      '-ms-transform': 'translate(-' + position.x + ', -' + position.y + ')',
      '-moz-transform': 'translate(-' + position.x + ', -' + position.y + ')',
      transform: 'translate(-' + position.x + ', -' + position.y + ')',
      visibility: 'hidden',
      opacity: 0
    }) 
    .one('canplaythrough.' + PLUGIN_NAME, function () {
      vide.resize();
    }) 
    .one('playing.' + PLUGIN_NAME, function () {
      $video.css({
        visibility: 'visible',
        opacity: 1
      });
      $wrapper.css('background-image', 'none');
    }); 

    $element.on('resize.' + PLUGIN_NAME, function () {
      if (settings.resizing) {
        vide.resize();
      }
    }); 

    $wrapper.append($video);
  };


  Vide.prototype.getVideoObject = function () {
    return this.$video[0];
  };


  Vide.prototype.resize = function () {
    if (!this.$video) {
      return;
    }

    var $wrapper = this.$wrapper;
    var $video = this.$video;
    var video = $video[0]; 

    var videoHeight = video.videoHeight;
    var videoWidth = video.videoWidth; 

    var wrapperHeight = $wrapper.height();
    var wrapperWidth = $wrapper.width();

    if (wrapperWidth / videoWidth > wrapperHeight / videoHeight) {
      $video.css({
        width: wrapperWidth + 2,
        height: 'auto'
      });
    } else {
      $video.css({
        width: 'auto',
        height: wrapperHeight + 2
      });
    }
  };


  Vide.prototype.destroy = function () {
    delete $[PLUGIN_NAME].lookup[this.index];
    this.$video && this.$video.off(PLUGIN_NAME);
    this.$element.off(PLUGIN_NAME).removeData(PLUGIN_NAME);
    this.$wrapper.remove();
  };


  $[PLUGIN_NAME] = {
    lookup: []
  };

  $.fn[PLUGIN_NAME] = function (path, options) {
    var instance;
    this.each(function () {
      instance = $.data(this, PLUGIN_NAME); 

      instance && instance.destroy(); 

      instance = new Vide(this, path, options);
      instance.index = $[PLUGIN_NAME].lookup.push(instance) - 1;
      $.data(this, PLUGIN_NAME, instance);
    });
    return this;
  };

  $(document).ready(function () {
    var $window = $(window); 

    $window.on('resize.' + PLUGIN_NAME, function () {
      for (var len = $[PLUGIN_NAME].lookup.length, i = 0, instance; i < len; i++) {
        instance = $[PLUGIN_NAME].lookup[i];

        if (instance && instance.settings.resizing) {
          instance.resize();
        }
      }
    }); 

    $window.on('unload.' + PLUGIN_NAME, function () {
      return false;
    }); 

    $(document).find('[data-' + PLUGIN_NAME + '-bg]').each(function (i, element) {
      var $element = $(element);
      var options = $element.data(PLUGIN_NAME + '-options');
      var path = $element.data(PLUGIN_NAME + '-bg');
      $element[PLUGIN_NAME](path, options);
    });
  });
});
jQuery(document).ready(function ($) {
  $('.lazyload-wrap img').addClass('lazyload');
  $('.lazyload-wrap video').addClass('lazyload');
  $('.lazyload-wrap img').attr('data-sizes', 'auto'); 

  $fadeInSpeed = 700;
  $('.fadeUp').on('inview', function (event, isInView) {
    if (isInView) {
      $(this).animate({
        opacity: 1,
        top: 0
      }, $fadeInSpeed);
    } else {
    }
  }); 

  var sync1 = $("#home_slider");
  var sync2 = $("#home_thumb_slide");
  var slidesPerPage = 4; 

  var syncedSecondary = true;
  sync1.owlCarousel({
    items: 1,
    slideSpeed: 2000,
    nav: false,
    animateOut: 'fadeOut',
    autoplay: true,
    dots: false,
    loop: false,
    responsiveRefreshRate: 200
  }).on('changed.owl.carousel', syncPosition);
  sync2.on('initialized.owl.carousel', function () {
    sync2.find(".owl-item").eq(0).addClass("current");
  }).owlCarousel({
    items: slidesPerPage,
    dots: false,
    nav: false,
    mouseDrag: false,
    smartSpeed: 200,
    slideSpeed: 500,
    slideBy: slidesPerPage,
    responsiveRefreshRate: 100
  }).on('changed.owl.carousel', syncPosition2);
  $('.link').on('click', function () {
    window.location = $(this).attr('href');
  });

  function syncPosition(el) {
    var count = el.item.count - 1;
    var current = Math.round(el.item.index - el.item.count / 2 - .5 + 2);

    if (current < 0) {
      current = count;
    }

    if (current > count) {
      current = 0;
    } 


    sync2.find(".owl-item").removeClass("current").eq(current).addClass("current");
    var onscreen = sync2.find('.owl-item.active').length - 1;
    var start = sync2.find('.owl-item.active').first().index();
    var end = sync2.find('.owl-item.active').last().index();

    if (current > end) {
      sync2.data('owl.carousel').to(current, 100, true);
    }

    if (current < start) {
      sync2.data('owl.carousel').to(current - onscreen, 100, true);
    }
  }

  function syncPosition2(el) {
    if (syncedSecondary) {
      var number = el.item.index;
      sync1.data('owl.carousel').to(number, 100, true);
    }
  }

  sync2.on("click", ".owl-item", function (e) {
    e.preventDefault();
    var number = $(this).index();
    sync1.data('owl.carousel').to(number, 300, true);
  });
  $(window).on('scroll', function () {
    if ($(window).scrollTop() >= $(document).height() - $(window).height() - 150) {
      $(".load-mored").click();
    }
  }); 

  var currentSlideIndexMobile = 0;
  var homeSliderMobile = $('#home_slider_mobile');
  homeSliderMobile.owlCarousel({
    items: 1,
    loop: true,
    nav: false,
    dots: false,
    autoplay: true,
    animateOut: 'fadeOut'
  }).on('dragged.owl.carousel', function (el) {
    var index = el.item.index - el.relatedTarget._clones.length / 2 % el.item.count;
    homeSliderMobileNav.data('owl.carousel').to(index, 500, true);
  });
  var homeSliderMobileNav = $('#home_thumb_slide_mobile');
  homeSliderMobileNav.owlCarousel({
    items: 1,
    loop: true,
    nav: true,
    autoplay: true,
    dots: false
  }).on('changed.owl.carousel', function (el) {
    var index = el.item.index - el.relatedTarget._clones.length / 2 % el.item.count;
    homeSliderMobile.data('owl.carousel').to(index, 1000, true);
  }); 

  $('.featured_slide').owlCarousel({
    loop: true,
    rewind: false,
    margin: 0,
    autoplay: true,
    items: 5,
    autoplayTimeout: 1000,
    autoplaySpeed: 1000,
    responsiveClass: true,
    responsive: {
      0: {
        items: 2,
        nav: false
      },
      768: {
        items: 3,
        nav: false
      },
      1200: {
        items: 5,
        nav: false,
        loop: true
      }
    }
  }); 

  var carousel = $(".events");
  carousel.owlCarousel({
    loop: true,
    autoplay: true,
    items: 3,
    slideSpeed: 2000,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: false,
        autoWidth: false,
        stagePadding: 0
      },
      480: {
        items: 1,
        nav: false,
        autoWidth: false,
        stagePadding: 0
      },
      600: {
        items: 2,
        nav: false,
        margin: 40
      },
      768: {
        items: 3,
        nav: false,
        margin: 40
      },
      1000: {
        items: 3,
        nav: false,
        loop: true,
        margin: 60
      }
    }
  });
  checkClasses();
  carousel.on('translated.owl.carousel', function (event) {
    checkClasses();
  });

  function checkClasses() {
    var total = $('.events .owl-stage .owl-item.active').length;
    $('.events .owl-stage .owl-item').removeClass('firstActiveItem lastActiveItem');
    $('.events .owl-stage .owl-item.active').each(function (index) {
      if (index === 0) {
        $(this).addClass('firstActiveItem');
      }

      if (index === total - 1 && total > 1) {
        $(this).addClass('lastActiveItem');
      }
    });
  }

  var viewportWidth = $(window).width();

  if (viewportWidth < 767) {
    $(".secondary_nav .filter_category_menu").addClass("view_menu_mobile");
  } else {
    $(".secondary_nav .filter_category_menu").removeClass("view_menu_mobile");
  }

  $(window).resize(function () {
    if (viewportWidth < 767) {
      $(".secondary_nav .filter_category_menu").addClass("view_menu_mobile");
    } else {
      $(".secondary_nav .filter_category_menu").removeClass("view_menu_mobile");
    }
  }); 

  $(".view-menu").click(function () {
    $(this).toggleClass('active');
    $(".main_nav").slideToggle('slow');
  });
  $('.menu-item-has-children > a').after('<span></span>');
  $(".menu-item-has-children > span").click(function () {
    if (!$(this).siblings('ul.sub-menu').hasClass('open')) {
      $('ul.sub-menu').removeClass('open');
    }

    $(this).siblings('ul.sub-menu').toggleClass('open');
  });
  $(".menu-item-has-children > span").click(function () {
    if (!$(this).hasClass('close_icn')) {
      $('.close_icn').removeClass('close_icn');
    }

    $(this).toggleClass('close_icn');
  });
  jQuery(".sidebar.secondary_nav span.select-dropdown").click(function () {
    jQuery('.sidebar.secondary_nav ul.filter_category_menu.view_menu_mobile').toggleClass("open-mobile").slideToggle();
    jQuery(this).toggleClass("open-dropdown");
    jQuery(".sub_menu_text").removeClass('sub_menu_text');
  });
  jQuery(".filter_category_menu.view_menu_mobile > li > a").click(function () {
    jQuery(".sub_menu_text").removeClass('sub_menu_text'); 

    var txt = jQuery(this).text();
    jQuery('.select-dropdown').text(txt);
    jQuery(".sidenav_list.filter_category_menu.view_menu_mobile.open-mobile").toggleClass("open-mobile");
    jQuery('.select-dropdown').trigger('click');
  });
  jQuery(".sub-menu.sub_categories > li > a").click(function () {
    jQuery('.active_cat').removeClass('active_cat');
    jQuery(this).addClass('active_cat');
    var subtxt = jQuery(this).text();
    jQuery('.select-dropdown').text(subtxt);
  }); 

  mob_news_post_size_li = jQuery(".news_post_list_desk li").size();
  MA = 4;
  jQuery('.news_post_list_desk li:lt(' + MA + ')').show();
  jQuery('.scroll_load').on("click", function () {
    MA = MA + 4 <= mob_news_post_size_li ? MA + 4 : mob_news_post_size_li;
    jQuery('.news_post_list_desk li:lt(' + MA + ')').show();
    var count_elements = jQuery('.news_post_list_desk li').length;

    if (MA == mob_news_post_size_li) {
      jQuery('.news_post .scroll_load').hide();
    }
  });
  resource_post_size_li = jQuery(".resource_post_list_desk li").size();
  MR = 4;
  jQuery('.resource_post_list_desk li:lt(' + MR + ')').show();
  jQuery('.scroll_load').on("click", function () {
    MR = MR + 4 <= resource_post_size_li ? MR + 4 : resource_post_size_li;
    jQuery('.resource_post_list_desk li:lt(' + MR + ')').show();
    var count_elements = jQuery('.resource_post_list_desk li').length;

    if (MR == resource_post_size_li) {
      jQuery('.resource_posts .scroll_load').hide();
    }
  });
}); 

jQuery(document).ready(function ($) {
  $(".scroll_submenu a").on('click', function (event) {
    if (this.hash !== "") {
      var hash = this.hash;
      var id = hash.match(/[\d\.]+/g);

      if (id == "2") {
        var offset_value = "150";
      } else if (id == "3") {
        var offset_value = "210";
      } else if (id == "4") {
        var offset_value = "270";
      } else if (id == "5") {
        var offset_value = "320";
      } else if (id == "6") {
        var offset_value = "380";
      } else {
        offset_value = 0;
      }

      $('html, body').animate({
        scrollTop: $(hash).offset().top - offset_value
      }, 900, function () {
        window.location.hash = hash;
      });
    } 


    setTimeout(function () {
      $('.sidenav_list.filter_category_menu ' + hash + ' > a').trigger('click');
    }, 1000);
  });

  if (window.location.hash !== "") {
    var hash = window.location.hash;
    var id = hash.match(/[\d\.]+/g);

    if (id == "2") {
      var offset_value = "150";
    } else if (id == "3") {
      var offset_value = "210";
    } else if (id == "4") {
      var offset_value = "270";
    } else if (id == "5") {
      var offset_value = "320";
    } else if (id == "6") {
      var offset_value = "385";
    } else {
      offset_value = 0;
    }

    $('html, body').animate({
      scrollTop: $(hash).offset().top - offset_value
    }, 900, function () {
      window.location.hash = hash;
    });
    setTimeout(function () {
      $('.sidenav_list.filter_category_menu ' + hash + ' > a').trigger('click');
    }, 1000);
  }
}); 
