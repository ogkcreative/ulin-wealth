<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package _s
 */
?>
	<div id="secondary" class="widget-area" role="complementary">
		<?php do_action( 'before_sidebar' ); ?>
		<?php

		if(!$post->post_parent){
			// will display the subpages of this top level page
			$children = wp_list_pages("title_li=&child_of=".$post->ID."&echo=0");
		}
		else{

			if($post->ancestors) {
				// now you can get the the top ID of this page
				// wp is putting the ids DESC, thats why the top level ID is the last one
				$ancestors = end($post->ancestors);
				$children = wp_list_pages("title_li=&child_of=".$ancestors."&echo=0");
			}
		}

		if ($children) { ?>
		  <ul>
		  	<li class="parent_page">
		  		<a href="<?php $parentLink = get_permalink($post->post_parent); echo $parentLink; ?>" title="<?php $parentTitle = get_the_title($post->post_parent); echo $parentTitle; ?>"><?php $parentTitleName = get_the_title($post->post_parent); echo $parentTitleName; ?></a>
		  	</li>
			<?php echo $children; ?>
		  </ul>
		<?php } ?>
	</div><!-- #secondary -->
