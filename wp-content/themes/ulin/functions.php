<?php

/**
 * ALL Non Specific Theme based functions include here
 * if not user /wp-content/plugins/a-custom-functions/a-custom-functions.php file
 **/

function enqueue_theme_scripts() {

    $theme_version = (WP_DEBUG == TRUE)? time(): '1.2';

	 wp_enqueue_script('cat-filter-js', get_template_directory_uri() . '/assets/js/filter.js', array(), $theme_version, true );
	 wp_localize_script('cat-filer-js', 'cat-filer-js-ajax', array(
	                                'ajaxurl' => admin_url('admin-ajax.php')
     ) );
	 wp_enqueue_style( 'custom-style', get_stylesheet_uri() );
    wp_enqueue_style( 'main-styles', get_template_directory_uri() . '/dist/assets/sass/style.css' );

    wp_register_script('main-scripts', get_template_directory_uri() . '/dist/assets/js/scripts-min.js', '', $theme_version, true);
    wp_enqueue_script('main-scripts');
//     wp_enqueue_script( 'custom-script', get_template_directory_uri() . '/style.css', array(), $theme_version, true );

}
add_action( 'wp_enqueue_scripts', 'enqueue_theme_scripts' );

get_template_part('function-filter');
get_template_part('includes/custom','post-type');

function social_func( $atts ) {
    $args = shortcode_atts( array(
        'cat' => 'no',
		'noofpost'=>'-1',

    ), $atts );

	global $post;
	// Get current page URL
	$articalURL = urlencode(get_permalink());
	$articalTitle = str_replace( ' ', '%20', get_the_title());
	$articalThumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );

		// Construct sharing URL without using any script
		$twitterURL = 'https://twitter.com/intent/tweet?text='.$articalTitle.'&amp;url='.$articalURL;
		$facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='.$articalURL;
		$googleURL = 'https://plus.google.com/share?url='.$articalURL;
		$linkedInURL = 'https://www.linkedin.com/shareArticle?mini=true&url='.$articalURL.'&amp;title='.$articalTitle;
		// Based on popular demand added Pinterest too
		$pinterestURL = 'https://pinterest.com/pin/create/button/?url='.$articalURL.'&amp;media='.$articalThumbnail[0].'&amp;description='.$articalTitle;
		// Add sharing button at the end of page/page content

		$content = '<ul>';
		$content .= '<li><a class="crunchify-link crunchify-facebook" href="'.$facebookURL.'" target="_blank"><img src="'.get_template_directory_uri().'/assets/images/fb_share.png"></a></li>';
		$content .= '<li> <a class="crunchify-link crunchify-twitter" href="'. $twitterURL .'" target="_blank"><img src="'.get_template_directory_uri().'/assets/images/twit_share.png"></a></li>';
		$content .= '<li><a class="crunchify-link crunchify-linkedin" href="'.$linkedInURL.'" target="_blank"><img src="'.get_template_directory_uri().'/assets/images/link_share.png"></a></li>';
		$content .= '<li><a class="crunchify-link crunchify-googleplus" href="'.$googleURL.'" target="_blank"><img src="'.get_template_directory_uri().'/assets/images/gplus_share.png"></a></li>';
		$content .= '<li><a class="crunchify-link crunchify-pinterest" href="'.$pinterestURL.'" data-pin-custom="true" target="_blank"><img src="'.get_template_directory_uri().'/assets/images/pint_share.png"></a></li>';
		$content .= '</ul>';
		return $content;

};
add_shortcode( 'social_share', 'social_func' );


function get_excerpt($limit, $source = null){

    $excerpt = $source == "content" ? get_the_content() : get_the_excerpt();
    $excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
    $excerpt = strip_shortcodes($excerpt);
    $excerpt = strip_tags($excerpt);
    $excerpt = substr($excerpt, 0, $limit);
    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
    $excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
    $excerpt = $excerpt.'... ';
    return $excerpt;
}

function add_theme_image_sizes(){
   add_image_size('blog-thumb', 400,200,true);
   add_image_size('blog-main', 1920,600, true);
}
add_action('init','add_theme_image_sizes');
