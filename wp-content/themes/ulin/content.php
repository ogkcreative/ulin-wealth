<?php
/**
 * @package _s
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="featured-image">
		<?php if ( has_post_thumbnail() ) {
			the_post_thumbnail(array(100,100));
		} else { ?>
			<a href="<?php the_permalink(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/blog-placeholder.jpg"></a>
		<?php } ?>
	</div>

	<div class="article-content">
		<header class="entry-header">
			<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>

			<?php if ( 'post' == get_post_type() ) : ?>
			<div class="entry-meta">
				<?php _e('Posted on'); ?> <?php the_time('F j, Y'); ?>
			</div><!-- .entry-meta -->
			<?php endif; ?>
		</header><!-- .entry-header -->


		<div class="entry-content">
			<p><?php echo mb_strimwidth(get_the_excerpt(), 0, 240, '…'); ?> <strong>Read More</strong></p>
		</div><!-- .entry-content -->
	</div>

</article><!-- #post-## -->
