<?php
/* Template Name: 404 Page Not Fount */
get_header(); ?> 

<div class="container">
  <div class="page_404 align-center">
    <h1><span>404</span>Oops! That Page Can’t Be Found.</h1>
    <a href="<?php echo get_home_url(); ?>" class="btn">Back To Home</a>
  </div>
</div>

<?php get_footer(); ?>