<!DOCTYPE html>
<html>
<head>
		<?php $gtm_id = get_field( 'tag_manager_id', 'options' );
	if($gtm_id): ?>
	<!-- Google Tag Manager -->
	<script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', '<?= $gtm_id ?>');</script>
	<!-- End Google Tag Manager -->
	<?php endif; ?>
    <title><?php wp_title('|', true, 'right'); ?></title>
    <meta name="viewport" content="width=device-width">
    <?php wp_head(); ?>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>


</head>
<body <?php body_class(); ?>>
	<?php if($gtm_id): ?>
<noscript>
	<iframe src="https://www.googletagmanager.com/ns.html?id=<?= get_field( 'tag_manager_id', 'options' ) ?>"
	        height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<?php endif; ?>
<style type="text/css">
    .news_post_list_desk li {
        display: none;
    }

    .resource_post_list_desk li {
        display: none;
    }


</style>

<div class="page_wrap">
    <!---header-->
    <header class="container-large">
        <?php
        $address = get_field('address', 'options');
        $address_mobile = get_field('address_mobile', 'options');
        $google_map_link = get_field('google_maps__link', 'options');
        $email = get_field('email', 'options');
        $phone = get_field('phone', 'options');
        $telno = str_replace(".", "", $phone);
        $portal_link = get_field('client_portal_link', 'options');
        ?>
        <div class="logo mobile">
            <a href="<?php bloginfo('url'); ?>">
                <?php if (get_field('logo', 'options')) {
                    echo file_get_contents(wp_get_attachment_image_url(get_field('logo', 'options'), 'full'));
                    ?>
                <?php } else { ?>
                    <h2><?php wp_title(); ?></h2>
                <?php } ?>
            </a>
        </div>


        <div class="top_bar">
            <div class="container">
                <ul>
                    <?php if ($address) { ?>
                        <li class="address"><a target="_blank"
                                               href="<?php echo $google_map_link; ?>"><?php echo $address; ?></a></li>
                    <?php } ?>

            
                    <?php if ($phone) { ?>
                        <li class="phone"><a href="tel:<?php echo $telno; ?>"><?php echo $phone; ?></a></li>
                    <?php } ?>
                    <?php if ($portal_link) { ?>
                        <li class="client_portal"><a href="<?php echo $portal_link; ?>">Client Center</a></li>
                    <?php } ?>
					        <li class="email"><a href="/contact-us/">Contact</a></li>
                </ul>
            </div>
        </div>
        <!--top_bar_End-->
        <div class="top_bar_mobile" style="display: none;">
            <div class="container">
                <ul>
                    <?php if ($address) { ?>
                        <li class="address"><a target="_blank"
                                               href="<?php echo $google_map_link; ?>">Location</a></li>
                    <?php } ?>

                    <?php if ($phone) { ?>
                        <li class="phone"><a href="tel:<?php echo $telno; ?>">Phone</a></li>
                    <?php } ?>
                    <?php if ($portal_link) { ?>
                        <li class="client_portal"><a href="<?php echo $portal_link; ?>">Login</a></li>
                    <?php } ?>
	                <li class="email"><a href="/contact-us/">Contact</a></li>
                </ul>
            </div>
        </div>
        <!--top_bar_End-->
        <div class="main_header">
            <div class="container">
                <div class="logo">
                    <a href="<?php bloginfo('url'); ?>">
                        <?php if (get_field('logo', 'options')) {
                            echo file_get_contents(wp_get_attachment_image_url(get_field('logo', 'options'), 'full'));
                            ?>
                        <?php } else { ?>
                            <h2><?php wp_title(); ?></h2>
                        <?php } ?>
                    </a>
                </div>
                <span class="view-menu">View Menu</span>
                <div class="main_nav">
                    <?php
                    wp_nav_menu(array('menu' => 'main-menu', 'container' => false)); ?>
                </div>
                <div class="social">
                    <?php include('includes/social-icons.php'); ?>
                </div>
            </div>
        </div>
        <?php if (is_front_page()) { ?>
            <?php if (get_field('join_our_team_text', 'options')) { ?>
                <div class="join_team">
                    <h4><?php echo get_field('join_our_team_text', 'options'); ?></h4>
                </div>
            <?php } ?>
        <?php } ?>
    </header>
    <!--Header_End-->
    <?php include_once(get_template_directory() . '/includes/hero.php'); ?>


