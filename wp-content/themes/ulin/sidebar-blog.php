<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package _s
 */
?>
	<div id="secondary" class="widget-area" role="complementary">
		<?php do_action( 'before_sidebar' ); ?>

		<ul>
		<li class="parent_page">
		<a href="<?php echo get_permalink(146); ?>" title="Money Sense Blog"><?php _e('Money Sense Blog'); ?></a>
		</li>
		<?php
		$args = array(
		'orderby' => 'name',
		'order' => 'ASC',
		'exclude' => '1',
		);
		$categories = get_categories($args);
		foreach($categories as $category) { 

		?>		  	
		<li>
		<?php echo '<a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $category->name ) . '" ' . '>' . $category->name.'</a>'; ?>
		</li>
		<?php } ?>
		<li><a href="http://feeds.feedburner.com/UlinCoWealthManagementServicesInBrowardPalmBeachAndMiami"><?php _e('Subscribe via RSS'); ?></a>
		</ul>

		<aside id="search" class="widget widget_search">
			<?php get_search_form(); ?>
		</aside>

		<aside class="widget newsletter-widget">
			<h3><?php _e('Join Our Newsletter'); ?></h3>
			<?php gravity_form(1, false, false, false, '', true, 12); ?>
		</aside>

		<a href="http://archive.constantcontact.com/fs025/1103755119916/archive/1104037446016.html" class="button newsletter-archives" target="_blank"><?php _e('Newsletter Archives'); ?></a>

	</div><!-- #secondary -->
