<?php if ( get_field( 'page_has_hero', get_the_ID() ) && get_field( 'page_has_hero', get_the_ID() ) == true ):
	$hero_image = wp_get_attachment_image_url( get_field( 'hero_image' ), 'full' );

	$hero_headline    = get_field( 'hero_headline' );
	$hero_subheadline = get_field( 'hero_subheadline' );
	?>

	<section class="hero">
		<?php if ( is_front_page() ) { ?>
			<?php if ( have_rows( 'banner_slider' ) ): ?>
				<div id="home_slider" class="owl-carousel owl-theme">
					<?php while ( have_rows( 'banner_slider' ) ): the_row();

						// vars
						$bg_image_upload = wp_get_attachment_image( get_sub_field( 'bg_image_upload' ), 'full' );
						$banner_headline = get_sub_field( 'banner_headline' );
						?>
						<div class="item">
							<?php echo $bg_image_upload; ?>
							<?php if ( $banner_headline ) { ?>
								<div class="slide_caption">
									<h1><?php echo $banner_headline; ?></h1>
								</div>
							<?php } ?>
						</div>

					<?php endwhile; ?>

				</div>

			<?php endif; ?>

			<div class="container-large home_hero_nav">
				<?php if ( have_rows( 'banner_slider' ) ): ?>
					<div id="home_thumb_slide" class="owl-carousel owl-theme">
					<?php while ( have_rows( 'banner_slider' ) ): the_row();
						$financial_text    = get_sub_field( 'financial_text' );
						$financial_heading = get_sub_field( 'financial_heading' );
						$financial_link    = get_sub_field( 'financial_link' );
						?>
						<div class="item">
							<?php if ( $financial_heading ) { ?>
								<h2><a class='link' href="<?= $financial_link ?>" target="_parent"
								       title="<?= $financial_text . " " . $financial_heading ?>"><span><?= $financial_text ?></span><?= $financial_heading ?>
									</a>
								</h2>
							<?php } ?>
						</div>
					<?php endwhile; ?>
					</div><?php endif; ?>
			</div>
        <?php if ( have_rows( 'banner_slider' ) ):
            $c = 0; ?>
        <div id="home_slider_mobile" class="owl-carousel owl-theme" style="display: none;">
            <?php while ( have_rows( 'banner_slider' ) ): the_row();
                // vars
                $bg_image_upload = wp_get_attachment_image( get_sub_field( 'bg_image_upload' ), 'full' );
                $banner_headline = get_sub_field( 'banner_headline' );
                ?>
                <div class="item" data-index="<?= $c ?>">
                    <?php echo $bg_image_upload; ?>
                </div>

            <?php $c++; endwhile; ?>
        </div>
        <?php endif; ?>
        <div class="container mobile-home-slider-nav" style="display: none;">
            <?php if ( have_rows( 'banner_slider' ) ):
                $c = 0;
                ?>
                <div id="home_thumb_slide_mobile" class="owl-carousel owl-theme">
                <?php while ( have_rows( 'banner_slider' ) ): the_row();
                    $financial_text    = get_sub_field( 'financial_text' );
                    $financial_heading = get_sub_field( 'financial_heading' );
                    $financial_link    = get_sub_field( 'financial_link' );
                    ?>
                    <div class="item" data-index="<?= $c ?>">
                        <?php if ( $financial_heading ) { ?>
                            <h2><a class='link' href="<?= $financial_link ?>" target="_parent"
                                   title="<?= $financial_text . " " . $financial_heading ?>"><span><?= $financial_text ?></span><?= $financial_heading ?>
                                </a>
                            </h2>
                        <?php } ?>
                    </div>
                <?php $c++; endwhile; ?>
                </div>
            <?php endif; ?>
        </div>
		<?php } else if ( is_page( '190' ) ) { ?>

			<section class="hero">
				<div class="inner_banner radio_show" style="background-image:url(<?php echo $hero_image; ?>);">
					<div class="container">
						<?php if ( $hero_headline ) { ?>
							<h1 class="align-center"><?php echo $hero_headline; ?>
							</h1>
						<?php } ?>
					</div>
				</div>
			</section>

		<?php } else { ?>
			<div class="<?php if ( is_page( '189' ) || is_page( '188' ) || is_page( '182' ) ) {
				echo "blog_banner";
			} else {
				echo "inner_banner";
			} ?>" style="background-image:url(<?php echo $hero_image; ?>);">
				<?php if ( ! is_page( '188' ) ){ ?>
				<div class="container">
					<?php } ?>
					<?php if ( is_page( '189' ) || is_page( '188' ) || is_page( '182' ) ) {
						?><?php if ( $hero_headline ) { ?>
							<h1 class="image_callout">
								<span><?php echo $hero_subheadline; ?></span><?php echo $hero_headline; ?></h1>
						<?php } ?>
					<?php } else { ?>
						<?php if ( $hero_headline ) { ?>
							<h1 class="align-center"><?php echo $hero_headline; ?></h1>
						<?php } ?>
					<?php } ?>

					<?php if ( ! is_page( '188' ) ){ ?>
				</div>
			<?php } ?>
			</div>

		<?php } ?>
	</section>
	<!--Hero_END-->
<?php endif; ?>