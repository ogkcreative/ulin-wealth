<?php //register posttype Team
add_action( 'init', 'ulin_news_events' ); 
function ulin_news_events() {
  register_post_type( 'news_events',
    array(
    'labels' => array( 
        'name' => __( 'News Events' ),
        'singular_name' => __( 'News Events' ),
        'add_new' => _x('Add News Events', 'News Events'),
        'add_new_item' => __("Add New News Events"),
        'edit_item' => __("Edit This News Events"),
        'new_item' => __("New News Events"),
        'view_item' => __("View News Events"),
        'search_items' => __("Search in News Events"),
        'not_found' =>  __('No News Events'),
        'not_found_in_trash' => __('No News Events found in Trash')

      ),
    'public' => true,
    'has_archive' => true,
       'menu_icon'   => 'dashicons-grid-view',
    'supports' => array('title', 'editor', 'thumbnail', 'page-attributes'),
    'rewrite' => array('slug' => 'news_event'),
    )
  ); 
} 
// news_events_category
$args = array(
  'hierarchical'      => true,
  'labels'            => array(
  'name'              => _x( 'News events Category', 'taxonomy general name', '' ),
  'singular_name'     => _x( 'News events Category', 'taxonomy singular name', '' ),
  'search_items'      => __( 'Search News events Category', '' ),
  'all_items'         => __( 'All News events Category', '' ),
  'parent_item'       => __( 'Parent News events Category', '' ),
  'parent_item_colon' => __( 'Parent News events Category:', '' ),
  'edit_item'         => __( 'Edit News events Category', '' ),
  'update_item'       => __( 'Update News events Category', '' ),
  'add_new_item'      => __( 'Add New News events Category', '' ),
  'new_item_name'     => __( 'New News events Category', '' ),
  'menu_name'         => __( 'News events Category', '' ),
),
  'show_ui'           => true,
  'show_admin_column' => true,
  'query_var'         => true, 
  'rewrite'           => array( 'slug' => 'news_events_cats' ), 
);

register_taxonomy( 'news_events_cat', array( 'news_events' ), $args );

//////////////////Radio Archive////////////////////

add_action( 'init', 'ulin_radio_archive' ); 
function ulin_radio_archive() {
  register_post_type( 'radio_archives',
    array(
    'labels' => array(
        'name' => __( 'Radio Archives' ),
        'singular_name' => __( 'Radio Archives' ),
        'add_new' => _x('Add Radio Archives', 'Radio Archives'),
        'add_new_item' => __("Add New Radio Archives"),
        'edit_item' => __("Edit This Radio Archives"),
        'new_item' => __("New Radio Archives"),
        'view_item' => __("View Radio Archives"),
        'search_items' => __("Search in Radio Archives"),
        'not_found' =>  __('No Radio Archives'),
        'not_found_in_trash' => __('No Radio Archives found in Trash')

      ),
    'public' => true,
    'has_archive' => true,
       'menu_icon'   => 'dashicons-grid-view',
    'supports' => array('title', 'editor', 'thumbnail', 'page-attributes'),
    'rewrite' => array('slug' => 'radio_archives'),
    )
  );
}

add_action( 'init', 'ulin_resources' ); 
function ulin_resources() {
  register_post_type( 'resource',
    array(
    'labels' => array( 
        'name' => __( 'Resources' ),
        'singular_name' => __( 'Resources' ),
        'add_new' => _x('Add Resources', 'Resources'),
        'add_new_item' => __("Add Resources"),
        'edit_item' => __("Edit This Resources"),
        'new_item' => __("New Resources"),
        'view_item' => __("View Resources"),
        'search_items' => __("Search in Resources"),
        'not_found' =>  __('No Resources'),
        'not_found_in_trash' => __('No Resources found in Trash')

      ),
    'public' => true,
    'has_archive' => true,
       'menu_icon'   => 'dashicons-exerpt-view',
    'supports' => array('title', 'editor', 'thumbnail', 'page-attributes'),
    'rewrite' => array('slug' => 'resource'),
    )
  ); 
} 
// news_events_category
$args = array(
  'hierarchical'      => true,
  'labels'            => array(
  'name'              => _x( 'Resources Category', 'taxonomy general name', '' ),
  'singular_name'     => _x( 'Resources Category', 'taxonomy singular name', '' ),
  'search_items'      => __( 'Search Resources Category', '' ),
  'all_items'         => __( 'All Resources Category', '' ),
  'parent_item'       => __( 'Parent Resources Category', '' ),
  'parent_item_colon' => __( 'Parent Resources  Category:', '' ),
  'edit_item'         => __( 'Edit Resources Category', '' ),
  'update_item'       => __( 'Update Resources Category', '' ),
  'add_new_item'      => __( 'Add New Resources Category', '' ),
  'new_item_name'     => __( 'New Resources Category', '' ),
  'menu_name'         => __( 'Resources Category', '' ),
),
  'show_ui'           => true,
  'show_admin_column' => true,
  'query_var'         => true, 
  'rewrite'           => array( 'slug' => 'resource_cat' ), 
);

register_taxonomy( 'resource_cat', array( 'resource' ), $args );