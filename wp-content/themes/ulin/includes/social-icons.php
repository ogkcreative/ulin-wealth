<?php if(have_rows('social_links','options')): ?>
<ul> 
	<?php while(have_rows('social_links','options')): the_row(); ?>
	<li><a href="<?php the_sub_field('url'); ?>" target="_blank">
		<?php if(!get_sub_field('custom_icon')): ?>
		<i class="fa fa-<?= strtolower(the_sub_field('site')) ?> " aria-hidden="true"></i>
		<?php else: ?>
		<?php echo file_get_contents(get_sub_field('custom_icon','options'),'full'); ?>
		<?php endif; ?> 
	</a></li>
	<?php endwhile; ?>  
</ul><!--/social-->
<?php endif; ?>