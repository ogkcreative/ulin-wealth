<?php if(get_field('sections') && have_rows('sections')): ?>
<?php while(have_rows('sections')): the_row(); ?>
	   <section id="<?= sanitize_title(get_sub_field('section_name')) ?>" class="section <? the_sub_field('section_layout_size') ?>">
		    <?php if(get_sub_field('items')): ?>
		        <?php while(have_rows('items')): the_row(); ?>
			               <div class="<?= the_sub_field('item_type') ?> <?= the_sub_field('item_size') ?>">
				                  <?php
				                  $item_type = get_sub_field('item_type');
				                  get_template_part('sections/section', $item_type);
				                  ?>
			               </div>
			    <?php endwhile; ?>
		    <?php endif; // End Items Loop ?>
	   </section>
<?php endwhile; ?>
<?php endif; // End Main Sections Loop ?>

