<?php
/**
 * Template Name: Home
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package _s
 */

get_header(); ?>

	<a class="finra" href="https://brokercheck.finra.org/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/BrokerCheck_logo.png"></a>

	<div id="primary" class="content-area">

		<div class="row">
			<div class="jon-photo">
				<img src="<?php echo get_template_directory_uri(); ?>/img/jon.png">

				<a class="jon-content" href="<?php bloginfo('url'); ?>/client-experience/our-team/">
					<h1><?php _e('Get to Know Our Founder'); ?>,<br><?php _e('Jon W. Ulin, CFP'); ?>&#8482;</h1>
					<p><?php _e('Managing Principal, Private Wealth Advisor'); ?><br><?php _e('CERTIFIED FINANCIAL PLANNER');?>&#8482;</p>
				</a>
			</div>

			<div class="home-right-boxes">
				<a href="<?php bloginfo('url'); ?>/financial-planning/" class="financial-planning">
					<span class="gradient-hover"></span>
					<h1><?php _e('Financial Planning'); ?></h1>
				</a>
				<a href="<?php bloginfo('url'); ?>/wealth-management/" class="wealth-management">
					<span class="gradient-hover"></span>					
					<h1><?php _e('Wealth'); ?><br> <?php _e('Management'); ?></h1>
				</a>
				<div class="newsletter">
					<h1><?php _e('Looking for more information on how');?><br><?php _e('to manage and grow your wealth?'); ?><br>
						<span><?php _e('Get informed. Stay informed.'); ?></span></h1>
					<p><?php _e('Sign up for our monthly newsletter');?> &amp; <?php _e('blog.'); ?></p>
					<?php gravity_form(1, false, false, false, '', true); ?>
				</div>				
			</div>
		</div>

		<div class="row">
			<div class="home-quote">
				<p><?php _e('“When you need independent wealth management and counseling, Ulin');?> &amp; <?php _e('Co. offers exceptional expertise, service, and commitment. For over a decade, we’ve helped affluent families, individuals, and retirees to build, protect, and transfer wealth.”'); ?></p>
			</div>

			<a href="<?php bloginfo('url'); ?>/client-experience/" class="getting-started">
				<span class="gradient-hover"></span>
				<h1><?php _e('Getting Started:'); ?><br><span><?php _e('Saving, Growing, and'); ?><br><?php _e('Protecting Your Money.');?></h1>
			</a>
		</div>		

	</div><!-- #primary -->

<?php get_footer(); ?>