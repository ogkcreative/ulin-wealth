
<?php if(have_rows('slider_items')): ?>
<div id="carousel-<?= sanitize_title(get_field('section_name')) ?>" class="owl-carousel">
	<?php while(have_rows('slider_items')): the_row(); ?>
	      <div class="item">
		      <a href="<?= get_sub_field('link') ?>" ><?php echo wp_get_attachment_image(get_sub_field('image'), 'full'); ?></a>
	      </div>
	<?php endwhile; ?>
</div>
<?php endif; ?>
