<?php

$image_size = get_sub_field('image_size');
$image_type = get_sub_field('image_type');

?>
	<div class="lazyload-wrap <?= $image_type ?>">
        		<?php echo wp_get_attachment_image(get_sub_field('image'), $image_size); ?>
    </div>