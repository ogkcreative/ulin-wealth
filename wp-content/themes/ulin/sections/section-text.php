<?php

$text_type = get_sub_field('text_type');
if($text_type == 'paragraph'): ?>
<p><?= the_sub_field('text_content') ?></p>
<?php else: ?>
<?php if(have_rows('list_items')):  ?>
	        <ul>
		        <?php while(have_rows('list_items')): the_row();
		        $link = get_sub_field('link');
		        $hover_color = get_sub_field('text_hover_color');
		        ?>
		        <li><a href="<?= $link['url'] ?>"
		               title="<?= (isset($link['title']))?$link['title'] :the_sub_field('text') ?>"
			            target="<?= $link['target'] ?>"
				        onMouseOver="this.style.color='<?= $hover_color ?>'"
			            onmouseleave="this.style.color='initial'"><?= the_sub_field('text') ?></a></li>
		        <?php endwhile; ?>
	        </ul>
<?php endif; endif; ?>
