<?php
/**
 * The template for displaying all single posts
 *
 * @package WordPress
 * @since 1.0
 * @version 1.0
 */ 

get_header(); ?>
<?php 

$post = get_queried_object();
 $postType = get_post_type_object(get_post_type($post));

 $post_type_name = esc_html($postType->labels->singular_name);
 
 $current_post_ID=$post->ID;
 $detail_banner_image = wp_get_attachment_image_url(get_field('detail_banner_image'),'full');  
 $detail_banner_heading=get_field('detail_banner_heading');
 $banner_sub_heading=get_field('banner_sub_heading');?>

<section class="hero">
  <?php if(get_field('detail_banner_image')) { ?>
     <div class="blog_banner" style="background-image:url(<?php echo $detail_banner_image;?>);">
      <?php if($banner_sub_heading || $detail_banner_heading ){ ?>
      <h1 class="image_callout"><span><?php echo $banner_sub_heading; ?></span><?php echo $detail_banner_heading; ?></h1>
      <?php } ?>
    </div>
    <?php } ?>
  </section>

<div class="blog_subscribe">
  <?php $newsletter=get_field('inner_page_event_information_form_title','options'); ?>
  <div class="container">
  <?php if($newsletter) { ?> <h2><?php echo $newsletter; ?></h2><?php } ?>
   <?php echo do_shortcode('[gravityform id="3" title="false" description="false" ajax="true"]') ?>
  </div>
</div>

<!--blog_subscribe_END--> 
<div class="container">

  <?php if($post_type_name=='News Events') {
 $post_types_name= 'news_events';
  } else {
  $post_types_name= 'radio_archives';

  } ?> 
    <div class="blog_container">
      <div class="sidebar secondary_nav">
        <div class="related_article">
          <h3>Related Articles</h3>
<?php $related_artical_args=array(
'post_type'=>$post_types_name,
'order' => 'DESC', 
'post__not_in' => array($current_post_ID),
'posts_per_page' => '3',
 ); ?>

    <?php $related_artical_query = new WP_Query( $related_artical_args ); ?>
<?php if ( $related_artical_query->have_posts() ) { ?>
      <ul>
       <?php while ( $related_artical_query->have_posts() ):
          $related_artical_query->the_post();  
          $artical_id=get_the_id();

      ?>
            <li>
              <h3><?php echo get_the_date('F j, Y') ?></h3>
              <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
            </li>
          <?php endwhile;  ?>
          </ul>
          <?php } ?>

          <?php if($post_type_name=='News Events') {
          $artical_link= 'news-events';
 
          } else {
         $artical_link='radio-show';

          } ?>   
          <a href="/<?php echo $artical_link;?>/" class="btn_underline">view all articles</a> </div>
      </div> 
      <!--Secondary_nav_END-->
      <div class="blog_article">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <h2><?php the_title(); ?></h2>
        <?php the_content(); ?>
      <?php endwhile; endif; ?>
        <div class="detail_bottom">
          <div class="left_share">
            <h4>Share this:</h4>
            <div class="social">
              <?php echo do_shortcode('[social_share]'); ?>
            </div>
          </div>

        <?php   $prevPost = get_previous_post();
$nextPost = get_next_post();?>
         <?php if(!empty($prevPost->ID)): ?><a class="btn" href="<?php echo get_the_permalink($prevPost->ID) ?>" class="next">Next ARTICLE</a><?php endif ?>
      </div>
    </div> 
  </div>
  </div>

<?php get_footer();