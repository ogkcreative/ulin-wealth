<?php
/**
 * The template for displaying search forms in _s
 *
 * @package _s
 */
?>
<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<li>
	<label><?php _ex( 'Search &hellip;', 'label', '_s' ); ?></label>
	<input type="search" class="search-field" value="<?php echo esc_attr( get_search_query() ); ?>" name="s" title="<?php _ex( 'Search for:', 'label', '_s' ); ?>">	
	<input type="submit" class="search-submit" value="<?php echo esc_attr_x( 'Search', 'submit button', '_s' ); ?>">
	</li>
</form>
