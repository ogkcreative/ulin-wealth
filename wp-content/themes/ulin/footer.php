<footer class="footer_sec">
    <div class="container">
        <div class="footer-left">
            <div class="foter-col footer01"><a href="#" class="footer_logo">
                    <?php
                    echo file_get_contents(wp_get_attachment_image_url(get_field('footer_logo', 'options'), 'full'));
                    ?>
                </a>
                <p><?php echo get_field('footer_text', 'options'); ?></p>
            </div>
        </div>
        <div class="footer-right">
            <div class="foter-col footer02">
                <h5>Ulin & Co.</h5>
                <div class="footer-menu">
                    <?php
                    wp_nav_menu(array('menu' => 'Ulin-&-Co.', 'container' => false)); ?>
                </div>
            </div>
            <div class="foter-col footer03">
                <h5>Knowledge</h5>
                <div class="footer-menu">
                    <?php
                    wp_nav_menu(array('menu' => 'Knowledge', 'container' => false)); ?>
                </div>
            </div>
            <div class="foter-col footer04">
                <h5>Services</h5>
                <div class="footer-menu">
                    <?php
                    wp_nav_menu(array('menu' => 'Services', 'container' => false)); ?>
                </div>
            </div>
            <!--<div class="foter-col footer04">
                <h5>Resources</h5>
                <div class="footer-menu">
                    <?php
            /*				wp_nav_menu( array( 'menu' => 'Resources', 'container' => false ) ); */ ?>
                </div>
            </div>-->
			
            <div class="foter-col footer05">
                <?php
                $address = get_field('address', 'options');
                $google_map_link = get_field('google_maps__link', 'options');
                $email = get_field('email', 'options');
                $phone = get_field('phone', 'options');
                $telno = str_replace(".", "", $phone);
                $fax = get_field('fax', 'options');
                $faxno = str_replace(".", "", $fax);
                $portal_link = get_field('client_portal_link', 'options');
                ?>
                <h5>Contact Us</h5>
                <p><a target="_blank"
                      href="<?php echo get_field('google_maps__link', 'options'); ?>"><?php echo get_field('address', 'options'); ?></a>
                </p>
                <p><a href="tel:<?php echo $telno; ?>">ph: <?php echo $phone; ?></a></p>
                <p><a href="javascript:void(0)">f: <?php echo $fax; ?></a></p>
                <p><a href="mailto:<?php if ($email) {
                        echo $email;
                    } else {
                        echo "info@ulinwealth.com";
                    } ?>"><?php echo $email; ?></a></p>
            </div>
        </div>
        <div class="foter-col footer01 mobile_view">
            <p><?php echo get_field('footer_text', 'options'); ?></p>
        </div>
    </div>
    <div class="copyright">
        <div class="container">
            <div class="social">
                <?php include('includes/social-icons.php'); ?>
            </div>
            <div class="right_bottom">
                <p><?php echo get_field('copyright_text', 'options'); ?></a></p>
            </div>
        </div>
    </div>
</footer>
</div>
<?= get_field('custom_code', 'options') ?>
<?php wp_footer(); ?>
</body>
</html>