jQuery( document ).ready( function( $ ) {

	$('.slideshow').bxSlider({
		controls: true,
		mode: 'fade'
	});

	// Mobile Menu

	$('.site-header').append('<nav class="mobile-menu"><span class="hamburger">&#9776;</span><ul></ul></nav>');
	$('.site-header').append('<a href="' +site_url+ '" class="mobile-logo">Home</a>');


	var mobileMenu = $('.mobile-menu ul'),
		topMenu = $('#menu-top-menu').html(),
		mainMenu = $('#menu-primary-menu').html(),
		hamburger = $('.mobile-menu .hamburger');

	mobileMenu.append('<li><a href="' +site_url+ '">Home</a></li>');
	mobileMenu.append(topMenu);
	mobileMenu.append(mainMenu);

	$('.mobile-menu .sub-menu').parent('li').append('<span>&#x25BC;</span>');

	$('.mobile-menu li span').toggle(function() {
		$(this).siblings('.sub-menu').slideDown();
		$(this).html('&#x25B2;');
	}, function() {
		$(this).siblings('.sub-menu').slideUp();
		$(this).html('&#x25BC;');
	});

	$(hamburger).click(function() {
		$(this).siblings(mobileMenu).slideToggle();
	});


	// Forms

	$('span.gfield_required').remove();

	$('input, textarea').each(function() {
		var label = $(this).closest('li').find('label').text();
		$(this).attr('placeholder', label);
	});

	jQuery(document).bind('gform_post_render', function(){
		$('input, textarea').each(function() {
			var label = $(this).closest('li').find('label').text();
			$(this).attr('placeholder', label);
		});

		$('[placeholder]').focus(function() {
		  var input = $(this);
		  if (input.val() == input.attr('placeholder')) {
		    input.val('');
		    input.removeClass('placeholder');
		  }
		}).blur(function() {
		  var input = $(this);
		  if (input.val() == '' || input.val() == input.attr('placeholder')) {
		    input.addClass('placeholder');
		    input.val(input.attr('placeholder'));
		  }
		}).blur().parents('form').submit(function() {
		  $(this).find('[placeholder]').each(function() {
		    var input = $(this);
		    if (input.val() == input.attr('placeholder')) {
		      input.val('');
		    }
		  })
		});		
	});

	$('[placeholder]').focus(function() {
	  var input = $(this);
	  if (input.val() == input.attr('placeholder')) {
	    input.val('');
	    input.removeClass('placeholder');
	  }
	}).blur(function() {
	  var input = $(this);
	  if (input.val() == '' || input.val() == input.attr('placeholder')) {
	    input.addClass('placeholder');
	    input.val(input.attr('placeholder'));
	  }
	}).blur().parents('form').submit(function() {
	  $(this).find('[placeholder]').each(function() {
	    var input = $(this);
	    if (input.val() == input.attr('placeholder')) {
	      input.val('');
	    }
	  })
	});	


} );