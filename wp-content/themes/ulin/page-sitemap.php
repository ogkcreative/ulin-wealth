<?php
/**
 * Template Name: Sitemap
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package _s
 */

get_header(); ?>

	<div class="wrap">

		<div id="primary" class="full-width content-area">
			<div id="content" class="site-content" role="main">

				<article class="post-holder">
				<h1><?php the_title(); ?></h1>
				<div id="page-content">
					<ul class="sitemap">
							<?php wp_list_pages('title_li= '); ?>
						</ul>
				</div><!--#pageContent -->
				</article>

			</div><!-- #content -->
		</div><!-- #primary -->

	</div><!-- .wrap -->

<?php get_footer(); ?>