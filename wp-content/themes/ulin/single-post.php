<?php
/**
 * The template for displaying all single posts
 *
 * @package WordPress
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<?php
 $current_post_ID=$post->ID;
$categories = get_the_category( $post->ID );
$cat_id= $categories[0]->term_id;
$cat_name=$categories[0]->name;
 $detail_banner_image = wp_get_attachment_image_url(get_field('detail_banner_image'),'blog-main');
 $detail_banner_heading=get_field('detail_banner_heading');
 $banner_sub_heading=get_field('banner_sub_heading');?>

<section class="hero">
  <?php if(get_field('detail_banner_image')){ ?>
     <div class="blog_banner" style="background-image:url(<?php echo $detail_banner_image;?>);">
      <h1 class="image_callout">
        <?php if($cat_name) { ?>
        <span><?php echo $cat_name; ?></span>
        <?php } else { ?>
        <?php if($banner_sub_heading){ ?>
        <span><?php echo $banner_sub_heading; ?></span>
       <?php } } ?>
        <?php echo $detail_banner_heading; ?></h1>
    </div>
    <?php } ?>
  </section>


<div class="container">
    <div class="blog_container">
      <div class="sidebar secondary_nav">
        <div class="related_article">
          <h3>Related Articles</h3>
<?php $related_artical_args=array(
'post_type'=>'post',
'order' => 'DESC',
'post__not_in' => array($current_post_ID),
'posts_per_page' => '3',
'tax_query' => array(
                        array(
                            'taxonomy' => 'category',
                            'field'    => 'term_id',
                            'terms'    => $cat_id,

                        ),
                    ),
 ); ?>

    <?php $related_artical_query = new WP_Query( $related_artical_args ); ?>
<?php if ( $related_artical_query->have_posts() ) { ?>
      <ul>
       <?php while ( $related_artical_query->have_posts() ):
          $related_artical_query->the_post();
          $artical_id=get_the_id();

      ?>
            <li>
              <h3><?php echo get_the_date('F j, Y') ?></h3>
              <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
            </li>
          <?php endwhile;  ?>
          </ul>
          <?php } ?>
          <a href="/blog/" class="btn_underline">view all articles</a> </div>
      </div>
      <!--Secondary_nav_END-->
      <div class="blog_article">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <h2><?php the_title(); ?></h2>
        <?php the_content(); ?>
      <?php endwhile; endif; ?>
        <div class="detail_bottom">
          <div class="left_share">
            <h4>Share this:</h4>
            <div class="social">
              <?php echo do_shortcode('[social_share]'); ?>
            </div>
          </div>

    <?php $args = array(
    'category' => $cat_id,
    'orderby'  => 'post_date',
    'order'    => 'DESC'
);
$posts = get_posts( $args );
// get IDs of posts retrieved from get_posts
$ids = array();
foreach ( $posts as $thepost ) {
    $ids[] = $thepost->ID;
}
// get and echo previous and next post in the same category
$thisindex = array_search( $current_post_ID, $ids );
$nextid    = isset( $ids[ $thisindex + 1 ] ) ? $ids[ $thisindex + 1 ] : 0;
if ( $nextid ) {
    ?><a class="btn" rel="next" href="<?php echo get_permalink($nextid) ?>">Next article</a><?php
} ?>
</div>
      </div>
    </div>
  </div>
    <div class="blog_subscribe">
        <?php $newsletter=get_field('inner_page_event_information_form_title','options'); ?>
        <div class="container">
            <?php if($newsletter) { ?> <h2><?php echo $newsletter; ?></h2><?php } ?>
            <?php echo do_shortcode('[gravityform id="3" title="false" description="false" ajax="true"]') ?>
        </div>
    </div>

    <!--blog_subscribe_END-->

<?php get_footer();
